module mse
contains

    ! function mapping : T, q, z --> mse value
    function computeMSE(T, q, z) result (mse)

        implicit none
 
        ! q = mixing ratio at t+dt (kg/kg)
        ! T = temperature at t+dt (k)
        ! z = pressure level in centibars?
        real :: T
        real :: q
        integer :: z
        real :: mse

        ! constants
        real :: g = 9.8
        integer(kind=2) :: Cp = 1006
        integer(kind=4) :: Lv = 2500000
        
        ! Moist Static Energy = Cp*T + g*z + Lv*q
        mse = Cp*T + g*z + Lv*q
    end function computeMSE
    
end module mse
