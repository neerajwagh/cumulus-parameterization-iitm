! Simplified Arakawa-Schubert Deep Convection Scheme using data supplied from netCDF files
! Detailed algorithm : http://www.dtcenter.org/GMTB/gfs_phys_doc/group___s_a_s.html#ga5ad7848a22a950d41e5bea89066876fb

! COMMANDS TO RUN THIS CODE
! --------------------------------------------------------------
! interpolate 2.5deg reanalysis grid to 0.25deg resolution using ./regrid_sascnvn_input.ipynb
! $ gfortran -fbacktrace -fbounds-check -ffree-line-length-512 exper_machine.f90 exper_physcons.f90 exper_funcphys.f90 sascnvn_init.f90 sascnvn_static.f90 sascnvn_recalculate.f90 sascnvn_dynamic.f90 sascnvn_feedback.f90 pwl_interp_1d.f90 sascnvn.f90 -o sascnvn -I/usr/local/include -L/usr/local/lib -lnetcdff
! $ ./sascnvn
! --------------------------------------------------------------

! CAUTION
! --------------------------------------------------------------
! 1. Index variables, they should start from 1, not 0
! 2. Datatypes and dimensions of variables
! 3. Implicit ordering when lats, lons 2D pairs are reduced to 1D im, keep it consistent
! 4. Subroutine calculations should not be a part of higher level if or do loop
! 5. Reading _FillValue from netcdf file
! 6. if(totflg) return should stop calling of subsequent subroutines
! 7. If levels are reversed, the data in data arrays remains in old level order. Need to fix that.
! 8. External modules used may cause problems due to datatype size and precision assumed.

program isolated_sascnvn
    
    ! import modules
    use netcdf
    use physcons, grav => con_g
    use machine, only:kind_phys
    ! use omp_lib

    use sascnvn_init
    use sascnvn_static
    use sascnvn_recalculate
    use sascnvn_dynamic
    use sascnvn_feedback

    implicit none

    ! files and handles
    character (len = *), parameter :: AIRTEMP_FILE_NAME     = "../data/Reanalysis_MumbaiFloods_AugEnd2017/regridded_air.4xdaily.mean.nc"
    character (len = *), parameter :: SHUM_FILE_NAME        = "../data/Reanalysis_MumbaiFloods_AugEnd2017/regridded_shum.4xdaily.mean.nc"
    character (len = *), parameter :: SFCPRES_FILE_NAME     = "../data/Reanalysis_MumbaiFloods_AugEnd2017/regridded_pres.sfc.4xdaily.mean.nc"
    character (len = *), parameter :: GHEIGHT_FILE_NAME     = "../data/Reanalysis_MumbaiFloods_AugEnd2017/regridded_hgt.4xdaily.mean.nc"
    character (len = *), parameter :: UWIND_FILE_NAME       = "../data/Reanalysis_MumbaiFloods_AugEnd2017/regridded_uwnd.4xdaily.mean.nc"
    character (len = *), parameter :: VWIND_FILE_NAME       = "../data/Reanalysis_MumbaiFloods_AugEnd2017/regridded_vwnd.4xdaily.mean.nc"
    character (len = *), parameter :: VVELOCITY_FILE_NAME   = "../data/Reanalysis_MumbaiFloods_AugEnd2017/regridded_omega.4xdaily.mean.nc"
    integer :: airtemp_ncid, shum_ncid, sfcpres_ncid, hgt_ncid, uwnd_ncid, vwnd_ncid, omega_ncid
    
    character (len = *), parameter :: OUTPUT_FILE_NAME = "../data/sascnvn_output/regridded_test.nc"
    integer :: out_ncid
    
    ! name given to variables in files
    character (len = *), parameter :: AIRTEMP_NAME =    "air"
    character (len = *), parameter :: SHUM_NAME =       "shum"
    character (len = *), parameter :: SFCPRES_NAME =    "pres"
    character (len = *), parameter :: HGT_NAME =        "hgt"
    character (len = *), parameter :: UWND_NAME =       "uwnd"
    character (len = *), parameter :: VWND_NAME =       "vwnd"
    character (len = *), parameter :: OMEGA_NAME =      "omega"

    character (len = *), parameter :: LVL_NAME =        "level"
    character (len = *), parameter :: LAT_NAME =        "lat"
    character (len = *), parameter :: LON_NAME =        "lon"
    character (len = *), parameter :: REC_NAME =        "time"

    ! FILE PROPERTIES, USE NCDUMP 
    ! ---------------------------------------------------------------------------------------------------
    ! Declarations for file writing

    character (len = *), parameter :: LAT_UNITS =       "degrees_north"
    character (len = *), parameter :: LON_UNITS =       "degrees_east"
    character (len = *), parameter :: LVL_UNITS =       "millibar"
    character (len = *), parameter :: REC_UNITS =       "hours since 1800-01-01 00:00:0.0"
        
    character (len = *), parameter :: RAINFALL_UNITS =  "mm/day"
    character (len = *), parameter :: CLDWRK_UNITS =    "m^2/s^2"
    
    character (len = *), parameter :: RAINFALL_NAME =   "rn"
    character (len = *), parameter :: CLDWRK_NAME =     "cldwrk"
    integer :: rainfall_varid, cldwrk_varid, lat_varid, lon_varid, rec_varid
    
    integer, parameter :: NDIMS_SFCPRES     = 3
    integer, parameter :: NDIMS_COMMON      = 4
    
    integer, parameter :: NLVLS_OMEGA       = 12
    integer, parameter :: NLVLS_SHUM        = 8
    integer, parameter :: NLVLS_COMMON      = 17
    integer :: lvl_omega_dimid, lvl_shum_dimid, lvl_common_dimid
    
    ! total number of pressure levels after interpolation at 25mb, from 1000mb to 300mb
    integer, parameter :: NLVLS_MODEL = 29
    
    real (kind=kind_phys) :: LEVELS_MODEL(NLVLS_MODEL)
    data LEVELS_MODEL /1000., 975., 950., 925., 900., 875., 850., 825., 800., 775., 750., 725., 700., 675., 650., 625., 600., 575., 550., 525., 500., 475., 450., 425., 400., 375., 350., 325., 300./

    ! , 275., 250., 225., 200., 175., 150., 125., 100., 75., 50., 25., 0./
    ! integer, parameter :: NLATS_COMMON = 73, NLONS_COMMON = 144, NRECS_COMMON = 31
    
    integer, parameter :: NLATS_COMMON = 240, NLONS_COMMON = 720, NRECS_COMMON = 45
    ! ---------------------------------------------------------------------------------------------------

    integer :: start(NDIMS_COMMON), count(NDIMS_COMMON), count_omega(NDIMS_COMMON), count_shum(NDIMS_COMMON)
    integer :: start_sfcpres(NDIMS_SFCPRES), count_sfcpres(NDIMS_SFCPRES)
    
    ! USER-DEFINED ADDITIONAL VARIABLES
    ! ---------------------------------------------------------------------------------------------------    
    character (len = *), parameter :: UNITS = "units"    
    real (kind=kind_phys) :: mean_layer_pressure        (NLVLS_MODEL)
    real (kind=kind_phys) :: interpol_airtemp_output    (NLONS_COMMON, NLATS_COMMON, NLVLS_MODEL)
    real (kind=kind_phys) :: interpol_shum_output       (NLONS_COMMON, NLATS_COMMON, NLVLS_MODEL)
    real (kind=kind_phys) :: interpol_hgt_output        (NLONS_COMMON, NLATS_COMMON, NLVLS_MODEL)
    real (kind=kind_phys) :: interpol_uwnd_output       (NLONS_COMMON, NLATS_COMMON, NLVLS_MODEL)
    real (kind=kind_phys) :: interpol_vwnd_output       (NLONS_COMMON, NLATS_COMMON, NLVLS_MODEL)
    real (kind=kind_phys) :: interpol_omega_output      (NLONS_COMMON, NLATS_COMMON, NLVLS_MODEL)
    
    ! loop iterators
    integer :: islimsk_i
    integer :: prsl_i, prsl_k, psp_i
    integer :: airtemp_i, shum_i, sfcpres_i, hgt_i, uwnd_i, vwnd_i, omega_i
    integer :: rn_i, cldwrk_i

    ! write these to file
    real (kind=kind_phys) :: rainfall(NLONS_COMMON, NLATS_COMMON, NRECS_COMMON)
    real (kind=kind_phys) :: cloudwork(NLONS_COMMON, NLATS_COMMON, NRECS_COMMON)
    ! ---------------------------------------------------------------------------------------------------

    ! variables which will hold the actual data
    real (kind=kind_phys) :: lats_common    (NLATS_COMMON) 
    real (kind=kind_phys) :: lons_common    (NLONS_COMMON)
    real (kind=kind_phys) :: levels_common  (NLVLS_COMMON)
    real (kind=kind_phys) :: recs_common  (NRECS_COMMON)      
    real (kind=kind_phys) :: levels_shum    (NLVLS_SHUM) 
    real (kind=kind_phys) :: levels_omega   (NLVLS_OMEGA)
    
    integer :: lon_common_varid, lat_common_varid, rec_common_varid, lvl_common_varid, lvl_shum_varid, lvl_omega_varid

    real (kind=kind_phys) :: airtemp_in  (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: shum_in     (NLONS_COMMON, NLATS_COMMON, NLVLS_SHUM)
    real (kind=kind_phys) :: sfcpres_in  (NLONS_COMMON, NLATS_COMMON)
    real (kind=kind_phys) :: hgt_in      (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: uwnd_in     (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: vwnd_in     (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: omega_in    (NLONS_COMMON, NLATS_COMMON, NLVLS_OMEGA)
    integer :: airtemp_varid, shum_varid, sfcpres_varid, hgt_varid, uwnd_varid, vwnd_varid, omega_varid
    
    integer :: dimids(3)
    integer :: lon_dimid, lat_dimid, lvl_dimid, rec_dimid    
    integer :: dimids_sfcpres(NDIMS_SFCPRES)
    integer :: dimids_common(NDIMS_COMMON)

    
    ! Loop indices
    integer :: lon, lat, lvl, rec

    ! Open the files 
    call check( nf90_open(AIRTEMP_FILE_NAME     , nf90_nowrite, airtemp_ncid) )
    call check( nf90_open(SHUM_FILE_NAME        , nf90_nowrite, shum_ncid) )
    call check( nf90_open(SFCPRES_FILE_NAME     , nf90_nowrite, sfcpres_ncid) )
    call check( nf90_open(GHEIGHT_FILE_NAME     , nf90_nowrite, hgt_ncid) )
    call check( nf90_open(UWIND_FILE_NAME       , nf90_nowrite, uwnd_ncid) )
    call check( nf90_open(VWIND_FILE_NAME       , nf90_nowrite, vwnd_ncid) )
    call check( nf90_open(VVELOCITY_FILE_NAME   , nf90_nowrite, omega_ncid) )
    
    ! Get the varids of variables.
    call check( nf90_inq_varid(airtemp_ncid, LAT_NAME, lat_common_varid) )
    call check( nf90_inq_varid(airtemp_ncid, LON_NAME, lon_common_varid) )
    call check( nf90_inq_varid(airtemp_ncid, REC_NAME, rec_common_varid) )

    call check( nf90_inq_varid(airtemp_ncid, LVL_NAME, lvl_common_varid) )
    call check( nf90_inq_varid(shum_ncid, LVL_NAME, lvl_shum_varid) )
    call check( nf90_inq_varid(omega_ncid, LVL_NAME, lvl_omega_varid) )

    call check( nf90_inq_varid(airtemp_ncid, AIRTEMP_NAME, airtemp_varid) )
    call check( nf90_inq_varid(shum_ncid, SHUM_NAME, shum_varid) )
    call check( nf90_inq_varid(sfcpres_ncid, SFCPRES_NAME, sfcpres_varid) )
    call check( nf90_inq_varid(hgt_ncid, HGT_NAME, hgt_varid) )
    call check( nf90_inq_varid(uwnd_ncid, UWND_NAME, uwnd_varid) )
    call check( nf90_inq_varid(vwnd_ncid, VWND_NAME, vwnd_varid) )
    call check( nf90_inq_varid(omega_ncid, OMEGA_NAME, omega_varid) )

    ! Read the dimensions
    call check( nf90_get_var(airtemp_ncid, lat_common_varid, lats_common) )
    call check( nf90_get_var(airtemp_ncid, lon_common_varid, lons_common) )
    call check( nf90_get_var(airtemp_ncid, rec_common_varid, recs_common) )
    
    call check( nf90_get_var(airtemp_ncid, lvl_common_varid, levels_common) )
    call check( nf90_get_var(shum_ncid, lvl_shum_varid, levels_shum) )
    call check( nf90_get_var(omega_ncid, lvl_omega_varid, levels_omega) )
    
    ! reverse levels for model code
    ! levels_common = levels_common(NLVLS_COMMON : 1 : -1)
    ! levels_shum = levels_shum(NLVLS_SHUM : 1 : -1)
    ! levels_omega = levels_omega(NLVLS_OMEGA : 1 : -1)

    ! -------------------------------------------------------------------
    ! Create the output file. 
    call check( nf90_create(OUTPUT_FILE_NAME, nf90_clobber, out_ncid) )
    
    ! Define the dimensions. The record dimension is defined to have unlimited length - it can grow as needed.
    call check( nf90_def_dim(out_ncid, LON_NAME, NLONS_COMMON, lon_dimid) )
    call check( nf90_def_dim(out_ncid, LAT_NAME, NLATS_COMMON, lat_dimid) )
    call check( nf90_def_dim(out_ncid, REC_NAME, NF90_UNLIMITED, rec_dimid) )
    ! call check( nf90_def_dim(out_ncid, LVL_NAME, NLVLS_MODEL, lvl_dimid) )
    
    ! Define the coordinate variables and assign attributes
    call check( nf90_def_var(out_ncid, LON_NAME, NF90_REAL, lon_dimid, lon_varid) )
    call check( nf90_put_att(out_ncid, lon_varid, UNITS, LON_UNITS) )
    
    call check( nf90_def_var(out_ncid, LAT_NAME, NF90_REAL, lat_dimid, lat_varid) )
    call check( nf90_put_att(out_ncid, lat_varid, UNITS, LAT_UNITS) )
    
    call check( nf90_def_var(out_ncid, REC_NAME, NF90_REAL, rec_dimid, rec_varid) )
    call check( nf90_put_att(out_ncid, rec_varid, UNITS, REC_UNITS) )
   
    ! call check( nf90_def_var(out_ncid, LVL_NAME, NF90_REAL, lvl_dimid, lvl_varid) )
    ! call check( nf90_put_att(out_ncid, lvl_varid, UNITS, LVL_UNITS) )
    
    ! dimids = (/ lon_dimid, lat_dimid, lvl_dimid /)
    dimids = (/ lon_dimid, lat_dimid, rec_dimid /)
    
    ! Define the custom variables and assign attributes
    call check( nf90_def_var(out_ncid, RAINFALL_NAME, NF90_REAL, dimids, rainfall_varid) )
    call check( nf90_put_att(out_ncid, rainfall_varid, UNITS, RAINFALL_UNITS) )
        
    call check( nf90_def_var(out_ncid, CLDWRK_NAME, NF90_REAL, dimids, cldwrk_varid) )
    call check( nf90_put_att(out_ncid, cldwrk_varid, UNITS, CLDWRK_UNITS) )

    ! End define mode.
    call check( nf90_enddef(out_ncid) )
    ! -------------------------------------------------------------------
    

    ! MAP NETCDF INPUT TO VARIABLES THE MODULE CODE EXPECTS
    ! ------------------------------------------------------------
    ! LEVELS_MODEL = LEVELS_MODEL(NLVLS_MODEL : 1 : -1)
    im = NLATS_COMMON * NLONS_COMMON
    ix = NLATS_COMMON * NLONS_COMMON
    km = NLVLS_MODEL
    ! delt = 3600
    delt = 600
    ncloud = 1
    jcap = 10

    ! ALLOCATE ALL THE ARRAYS IN MODULE DECLARATION: if (.not.allocated(Jam)) allocate ( Jam(4*M) )
    ! ------------------------------------------------------------
    ! ALLOCATE 1D
    ! -------------------------------------
    allocate(kbot(im))
    allocate(ktop(im))
    allocate(kcnv(im))
    allocate(kb(im))
    allocate(kbcon(im))
    allocate(kbcon1(im))
    allocate(ktcon(im))
    allocate(ktcon1(im))
    allocate(jmin(im))
    allocate(lmin(im))
    allocate(kbmax(im))
    allocate(kbm(im)) 
    allocate(kmax(im))
    allocate(islimsk(im))
    allocate(psp(im))
    allocate(cldwrk(im))
    allocate(rn(im))
    allocate(ps(im))

    allocate(qcond(im))
    allocate(qevap(im))
    allocate(rntot(im))
    allocate(vshear(im))
    allocate(xaa0(im))
    allocate(xk(im))
    allocate(xlamd(im))
    allocate(xmb(im))
    allocate(xmbmax(im))
    allocate(xpwav(im))
    allocate(xpwev(im))
    allocate(delubar(im))
    allocate(delvbar(im))
    allocate(aa1(im))
    allocate(acrt(im))
    allocate(acrtfct(im))
    allocate(delhbar(im))
    allocate(delq(im))
    allocate(delq2(im))
    allocate(delqbar(im))
    allocate(delqev(im))
    allocate(deltbar(im))
    allocate(deltv(im))
    allocate(dtconv(im))
    allocate(edt(im))
    allocate(edto(im))
    allocate(edtx(im))
    allocate(fld(im))
    allocate(hmax(im))
    allocate(hmin(im))
    allocate(aa2(im))
    allocate(pbcdif(im))
    allocate(pdot(im))
    allocate(pwavo(im))
    allocate(pwevo(im))
    allocate(xlamud(im))
    allocate(cnvflg(im))
    allocate(flg(im))
    allocate(qlko_ktcon(im))
    allocate(tx1(im))
    allocate(sumx(im))

    ! ALLOCATE 2D
    ! -------------------------------------
    allocate(delp(ix,km))
    allocate(prslp(ix,km))
    allocate(del(ix,km))    
    allocate(prsl(ix,km))
    allocate(q1(ix,km))
    allocate(t1(ix,km))
    allocate(u1(ix,km))
    allocate(v1(ix,km))
    allocate(dot(ix,km))
    allocate(phil(ix,km))
    allocate(cnvw(ix,km))
    allocate(cnvc(ix,km))

    allocate(ud_mf(im,km))
    allocate(dd_mf(im,km))
    allocate(dt_mf(im,km))
    allocate(hcdo(im,km))
    allocate(po(im,km))
    allocate(ucdo(im,km))
    allocate(vcdo(im,km))
    allocate(qcdo(im,km))
    allocate(pfld(im,km))
    allocate(to(im,km))
    allocate(qo(im,km))
    allocate(uo(im,km))
    allocate(vo(im,km))
    allocate(qeso(im,km))
    allocate(dellal(im,km))
    allocate(tvo(im,km))
    allocate(dbyo(im,km))
    allocate(zo(im,km))
    allocate(xlamue(im,km))
    allocate(fent1(im,km))
    allocate(fent2(im,km))
    allocate(frh(im,km))
    allocate(heo(im,km))
    allocate(heso(im,km))
    allocate(qrcd(im,km))
    allocate(dellah(im,km))
    allocate(dellaq(im,km))
    allocate(dellau(im,km))
    allocate(dellav(im,km))
    allocate(hcko(im,km))
    allocate(ucko(im,km))
    allocate(vcko(im,km))
    allocate(qcko(im,km))
    allocate(eta(im,km))
    allocate(etad(im,km))
    allocate(zi(im,km))
    allocate(qrcko(im,km))
    allocate(qrcdo(im,km))
    allocate(pwo(im,km))
    allocate(pwdo(im,km))
    allocate(cnvwt(im,km))


    ! islimsk = 1 for now
    do islimsk_i = 1, im
        islimsk(islimsk_i) = 1
    end do
    
    ! generate delp array as expected by model, 25mb interpolation, IN PASCALS
    do prsl_i = 1, im
        do prsl_k = 1, km
            delp(prsl_i, prsl_k) = 2500.
        end do
    end do
    
    ! 1000mb (1000,00Pa) to 300mb (300,00Pa), with 25mb (2500Pa) interpolation, apply arithmetic series
    do prsl_i = 1, NLVLS_MODEL
        mean_layer_pressure(prsl_i) = 98750. - (prsl_i - 1)*(2500.)
    end do
    ! mean_layer_pressure = mean_layer_pressure(NLVLS_MODEL : 1 : -1)

    ! generate psrlp array as expected by model, IN PASCALS
    do prsl_i = 1, im
        do prsl_k = 1, km
            prslp(prsl_i, prsl_k) = mean_layer_pressure(prsl_k)
        end do
    end do

    ! start and count should be reverse of the netcdf var dimensions
    start = (/ 1, 1, 1, 1 /)
    count = (/ NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON, 1 /)
    count_shum = (/ NLONS_COMMON, NLATS_COMMON, NLVLS_SHUM, 1 /)
    count_omega = (/ NLONS_COMMON, NLATS_COMMON, NLVLS_OMEGA, 1 /)

    start_sfcpres = (/ 1, 1, 1 /)
    count_sfcpres = (/ NLONS_COMMON, NLATS_COMMON, 1 /)

    ! call sascnvn routine after every timestep of netcdf file
    !$OMP PARALLEL DO
    do rec = 1, NRECS_COMMON
        start(4) = rec
        start_sfcpres(3) = rec
        ! print *, "for time step - ", rec

        ! read the airtemp data
        call check( nf90_get_var(airtemp_ncid, airtemp_varid, airtemp_in, start = start, count = count) )
        airtemp_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                ! interpolate observation data to model levels
                call pwl_value_1d(NLVLS_COMMON, levels_common, airtemp_in(lon, lat, :), NLVLS_MODEL, LEVELS_MODEL, interpol_airtemp_output(lon, lat, :) )

                ! convert t1 to dimension = (ix, km)
                do lvl = 1, NLVLS_MODEL
                    t1(airtemp_i, lvl) = interpol_airtemp_output(lon, lat, lvl)
                end do
                airtemp_i = airtemp_i + 1
            end do
        end do


        ! read the shum data        
        call check( nf90_get_var(shum_ncid, shum_varid, shum_in, start = start, count = count_shum) )
        shum_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON

                ! interpolate observation data to model levels
                call pwl_value_1d(NLVLS_SHUM, levels_shum, shum_in(lon, lat, :), NLVLS_MODEL, LEVELS_MODEL, interpol_shum_output(lon, lat, :) )

                ! convert q1 to dimension = (ix,km)
                do lvl = 1, NLVLS_MODEL
                    q1(shum_i, lvl) = interpol_shum_output(lon, lat, lvl)
                end do
                shum_i = shum_i + 1
            end do
        end do


        ! read the sfcpres data
        call check( nf90_get_var(sfcpres_ncid, sfcpres_varid, sfcpres_in, start = start_sfcpres, count = count_sfcpres) )
        ! convert psp to dimension = (im) 
        psp_i = 1        
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                psp(psp_i) = sfcpres_in(lon, lat)
                psp_i = psp_i + 1
            end do
        end do


        ! read the hgt data        
        call check( nf90_get_var(hgt_ncid, hgt_varid, hgt_in, start = start, count = count) )
        ! convert geopot height into geopotential
        hgt_in = hgt_in * grav 
        ! convert phil to dimension = (ix,km)
        hgt_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON

                ! interpolate observation data to model levels
                call pwl_value_1d(NLVLS_COMMON, levels_common, hgt_in(lon, lat, :), NLVLS_MODEL, LEVELS_MODEL, interpol_hgt_output(lon, lat, :) )

                do lvl = 1, NLVLS_MODEL
                    phil(hgt_i, lvl) = interpol_hgt_output(lon, lat, lvl)
                end do
                hgt_i = hgt_i + 1
            end do
        end do


        ! read the uwnd data        
        call check( nf90_get_var(uwnd_ncid, uwnd_varid, uwnd_in, start = start, count = count) )
        uwnd_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON

                ! interpolate observation data to model levels
                call pwl_value_1d(NLVLS_COMMON, levels_common, uwnd_in(lon, lat, :), NLVLS_MODEL, LEVELS_MODEL, interpol_uwnd_output(lon, lat, :) )

                ! convert uwnd to dimension = (ix,km)
                do lvl = 1, NLVLS_MODEL
                    u1(uwnd_i, lvl) = interpol_uwnd_output(lon, lat, lvl)
                end do
                uwnd_i = uwnd_i + 1
            end do
        end do


        ! read the vwnd data        
        call check( nf90_get_var(vwnd_ncid, vwnd_varid, vwnd_in, start = start, count = count) )
        ! convert vwnd to dimension = (ix,km)
        vwnd_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON

                ! interpolate observation data to model levels
                call pwl_value_1d(NLVLS_COMMON, levels_common, vwnd_in(lon, lat, :), NLVLS_MODEL, LEVELS_MODEL, interpol_vwnd_output(lon, lat, :) )

                do lvl = 1, NLVLS_MODEL
                    v1(vwnd_i, lvl) = interpol_vwnd_output(lon, lat, lvl)
                end do
                vwnd_i = vwnd_i + 1
            end do
        end do

        ! read the omega data        
        call check( nf90_get_var(omega_ncid, omega_varid, omega_in, start = start, count = count_omega) )
        ! convert dot to dimension = (ix,km)
        omega_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON

                ! interpolate observation data to model levels
                call pwl_value_1d(NLVLS_OMEGA, levels_omega, omega_in(lon, lat, :), NLVLS_MODEL, LEVELS_MODEL, interpol_omega_output(lon, lat, :) )

                do lvl = 1, NLVLS_MODEL
                    dot(omega_i, lvl) = interpol_omega_output(lon, lat, lvl)
                end do
                omega_i = omega_i + 1
            end do
        end do


        ! CALL SASCNVN MODULES NOW

        ! SASCNVN: INITIALIZATION SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call convert_input_pa_to_cb()
        call init_arrays_and_tunable_params()
        call define_top_layer()
        call init_hydrostatic_height_and_entr_rate()
        call convert_from_cb_to_mb()
        call init_column_variables()
        call computeMSE()
        ! -----------------------------------------------------------

        ! SASCNVN: STATIC CONTROL SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call determine_max_MSE_level()
        call find_level_of_free_convection()
        if(totflg) goto 101

        call find_critical_convective_inhibition()
        if(totflg) goto 101
        
        call assume_entrainment_detrainment_rate()
        call functions_mimic_cloud_ensemble()
        call final_entrainment_rate()
        call find_updraft_mass_flux()
        call mass_flux_above_cloud_base()
        call updraft_cloud_properties()
        call consider_convection_inhibition()
        if(totflg) goto 101

        call first_guess_cloud_top()
        if(totflg) goto 101

        call search_downdraft_originating_layer()
        call verify_jmin_within_cloud ()
        call upper_limit_mass_flux()
        call find_cloud_moisture_and_precipitation()
        call find_cloud_work_function()
        if(totflg) goto 101

        call estimate_convective_overshooting()
        call find_properties()
        call exchange_ktcon_ktcon1()
        call find_liquid_vapor_separation()
        call compute_precipitation_efficiency()
        call determine_detrainment_rate()
        call downdraft_mass_flux()
        call downdraft_moisture_properties()
        call final_downdraft_strength ()
        call downdraft_cloudwork_functions()
        if(totflg) goto 101

        call change_due_to_cloud_unit_mass()
        call change_due_to_subsidence_entrainment()
        call cloud_top_and_water()
        call final_variable_per_unit_mass_flux()
        call environmental_conditions()
        call recalculate_moist_static_energy()
        ! -----------------------------------------------------------

        ! SASCNVN: RECALCULATION SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call recalculate_moisture()
        call recalculate_downdraft_moisture_properties()
        call recalculate_downdraft_cloudwork_functions()
        ! -----------------------------------------------------------

        ! SASCNVN: DYNAMIC CONTROL SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call critical_cloudwork_function()
        call large_scale_forcing()
        if(totflg) goto 101

        call return_without_modifying()
        ! -----------------------------------------------------------

        ! SASCNVN: FEEDBACK CONTROL SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call feedback()
        call convective_cloud_water()
        call convective_cloud_cover()
        call cloud_water()
        call final_feedback()
        ! -----------------------------------------------------------
        
        101 continue
        ! print *, "totflag", totflg
        ! print *, "q1 : ", q1
        ! print *, "t1 : ", t1
        ! print *, "u1 : ", u1
        ! print *, "v1 : ", v1
        ! print *, "cloud work function : ", cldwrk
        ! print *, "convective rain : ", rn
        ! print *, "kbot : ", kbot
        ! print *, "ktop : ", ktop
        ! print *, "deep conv flag :", kcnv
        ! print *, "ud_mf :", ud_mf
        ! print *, "dd_mf : ", dd_mf
        ! print *, "dt_mf : ", dt_mf
        ! print *, "cnvw : ", cnvw
        ! print *, "cnvc : ", cnvc
        
        ! pause

        ! units = mm/day
        rn = rn * 86400.0

        ! unflatten im dimension into (lon,lat) for file writing
        rn_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                rainfall(lon, lat, rec) = rn(rn_i)
                rn_i = rn_i + 1
            end do
        end do
        
        cldwrk_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                cloudwork(lon, lat, rec) = cldwrk(cldwrk_i)
                cldwrk_i = cldwrk_i + 1
            end do
        end do
        
        print *, "--------- time step ends -------------"
        ! next time step
    end do        
    !$OMP END PARALLEL DO

    ! -----------------------------------------------------
    ! Write the variable data.
    call check( nf90_put_var(out_ncid, lat_varid, lats_common) )
    call check( nf90_put_var(out_ncid, lon_varid, lons_common) )
    call check( nf90_put_var(out_ncid, rec_varid, recs_common) )    
    call check( nf90_put_var(out_ncid, rainfall_varid, rainfall) )
    call check( nf90_put_var(out_ncid, cldwrk_varid, cloudwork) )
    ! -----------------------------------------------------

    ! ARRAY DEALLOCATION
    ! -----------------------------------------------------
    
    ! DEALLOCATE 1D
    ! -------------------------------------
    deallocate(kbot)
    deallocate(ktop)
    deallocate(kcnv)
    deallocate(kb)
    deallocate(kbcon)
    deallocate(kbcon1)
    deallocate(ktcon)
    deallocate(ktcon1)
    deallocate(jmin)
    deallocate(lmin)
    deallocate(kbmax)
    deallocate(kbm) 
    deallocate(kmax)
    deallocate(islimsk)
    deallocate(psp)
    deallocate(cldwrk)
    deallocate(rn)
    deallocate(ps)
    deallocate(qcond)
    deallocate(qevap)
    deallocate(rntot)
    deallocate(vshear)
    deallocate(xaa0)
    deallocate(xk)
    deallocate(xlamd)
    deallocate(xmb)
    deallocate(xmbmax)
    deallocate(xpwav)
    deallocate(xpwev)
    deallocate(delubar)
    deallocate(delvbar)
    deallocate(aa1)
    deallocate(acrt)
    deallocate(acrtfct)
    deallocate(delhbar)
    deallocate(delq)
    deallocate(delq2)
    deallocate(delqbar)
    deallocate(delqev)
    deallocate(deltbar)
    deallocate(deltv)
    deallocate(dtconv)
    deallocate(edt)
    deallocate(edto)
    deallocate(edtx)
    deallocate(fld)
    deallocate(hmax)
    deallocate(hmin)
    deallocate(aa2)
    deallocate(pbcdif)
    deallocate(pdot)
    deallocate(pwavo)
    deallocate(pwevo)
    deallocate(xlamud)
    deallocate(cnvflg)
    deallocate(flg)
    deallocate(qlko_ktcon)
    deallocate(tx1)
    deallocate(sumx)
    
    ! DEALLOCATE 2D
    ! -------------------------------------
    deallocate(delp)
    deallocate(prslp)
    deallocate(del)    
    deallocate(prsl)
    deallocate(q1)
    deallocate(t1)
    deallocate(u1)
    deallocate(v1)
    deallocate(dot)
    deallocate(phil)
    deallocate(cnvw)
    deallocate(cnvc)

    deallocate(ud_mf)
    deallocate(dd_mf)
    deallocate(dt_mf)
    deallocate(hcdo)
    deallocate(po)
    deallocate(ucdo)
    deallocate(vcdo)
    deallocate(qcdo)
    deallocate(pfld)
    deallocate(to)
    deallocate(qo)
    deallocate(uo)
    deallocate(vo)
    deallocate(qeso)
    deallocate(dellal)
    deallocate(tvo)
    deallocate(dbyo)
    deallocate(zo)
    deallocate(xlamue)
    deallocate(fent1)
    deallocate(fent2)
    deallocate(frh)
    deallocate(heo)
    deallocate(heso)
    deallocate(qrcd)
    deallocate(dellah)
    deallocate(dellaq)
    deallocate(dellau)
    deallocate(dellav)
    deallocate(hcko)
    deallocate(ucko)
    deallocate(vcko)
    deallocate(qcko)
    deallocate(eta)
    deallocate(etad)
    deallocate(zi)
    deallocate(qrcko)
    deallocate(qrcdo)
    deallocate(pwo)
    deallocate(pwdo)
    deallocate(cnvwt)


    
    ! Close the files
    call check( nf90_close(airtemp_ncid) )
    call check( nf90_close(shum_ncid) )
    call check( nf90_close(sfcpres_ncid) )
    call check( nf90_close(hgt_ncid) )
    call check( nf90_close(uwnd_ncid) )
    call check( nf90_close(vwnd_ncid) )
    call check( nf90_close(omega_ncid) )
    call check( nf90_close(out_ncid) )
    
    print *, "*** END OF PROGRAM! ***"

contains
    subroutine check(status)
        use netcdf
        implicit none
        integer, intent ( in) :: status
        if(status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped"
        end if
    end subroutine check

end program isolated_sascnvn

!  \section original Original Documentation
!  Penetrative convection is simulated following Pan and Wu (1994), which is based on Arakawa and Schubert(1974) as simplified by Grell (1993) and with a saturated downdraft. Convection occurs when the cloud work function (CWF) exceeds a certain threshold. Mass flux of the cloud is determined using a quasi-equilibrium assumption based on this threshold CWF. The CWF is a function of temperature and moisture in each air column of the model gridpoint. The temperature and moisture profiles are adjusted towards the equilibrium CWF within a specified time scale using the deduced mass flux. A major simplification of the original Arakawa-Shubert scheme is to consider only the deepest cloud and not the spectrum of clouds. The cloud model incorporates a downdraft mechanism as well as the evaporation of precipitation. Entrainment of the updraft and detrainment of the downdraft in the sub-cloud layers are included. Downdraft strength is based on the vertical wind shear through the cloud. The critical CWF is a function of the cloud base vertical motion. As the large-scale rising motion becomes strong, the CWF [similar to convective available potential energy (CAPE)] is allowed to approach zero (therefore approaching neutral stability).
!
!  Mass fluxes induced in the updraft and the downdraft are allowed to transport momentum. The momentum exchange is calculated through the mass flux formulation in a manner similar to that for heat and moisture. The effect of the convection-induced pressure gradient force on cumulus momentum transport is parameterized in terms of mass flux and vertical wind shear (Han and Pan, 2006). As a result, the cumulus momentum exchange is reduced by about 55 % compared to the full exchange.
!
!  The entrainment rate in cloud layers is dependent upon environmental humidity (Han and Pan, 2010). A drier environment increases the entrainment, suppressing the convection. The entrainment rate in sub-cloud layers is given as inversely proportional to height. The detrainment rate is assumed to be a constant in all layers and equal to the entrainment rate value at cloud base, which is O(10-4). The liquid water in the updraft layer is assumed to be detrained from the layers above the level of the minimum moist static energy into the grid-scale cloud water with conversion parameter of 0.002 m-1, which is same as the rain conversion parameter.
!
!  Following Han and Pan (2010), the trigger condition is that a parcel lifted from the convection starting level without entrainment must reach its level of free convection within 120-180 hPa of ascent, proportional to the large-scale vertical velocity. This is intended to produce more convection in large-scale convergent regions but less convection in large-scale subsidence regions. Another important trigger mechanism is to include the effect of environmental humidity in the sub-cloud layer, taking into account convection inhibition due to existence of dry layers below cloud base. On the other hand, the cloud parcel might overshoot beyond the level of neutral buoyancy due to its inertia, eventually stopping its overshoot at cloud top. The CWF is used to model the overshoot. The overshoot of the cloud top is stopped at the height where a parcel lifted from the neutral buoyancy level with energy equal to 10% of the CWF would first have zero energy.
!
!  Deep convection parameterization (SAS) modifications include:
!  - Detraining cloud water from every updraft layer
!  - Starting convection from the level of maximum moist static energy within PBL
!  - Random cloud top is eliminated and only deepest cloud is considered
!  - Cloud water is detrained from every cloud layer
!  - Finite entrainment and detrainment rates for heat, moisture, and momentum are specified
!  - Similar to shallow convection scheme,
!  - entrainment rate is given to be inversely proportional to height in sub-cloud layers
!  - detrainment rate is set to be a constant as entrainment rate at the cloud base.
!  - Above cloud base, an organized entrainment is added, which is a function of environmental relative humidity
