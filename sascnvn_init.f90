module sascnvn_init

! imports

    use machine , only : kind_phys
    use funcphys , only : fpvsx, fpvs
    use physcons, grav => con_g, cp => con_cp, hvap => con_hvap, rv => con_rv, fv => con_fvirt, t0c => con_t0c, cvap => con_cvap, cliq => con_cliq, eps => con_eps, epsm1 => con_epsm1

    implicit none

    ! NORMAL VARIABLE DECLARATIONS
    ! -------------------------------------------------------------------------------------- 
    integer im, ix, km, ncloud, jcap
    real(kind=kind_phys) delt
    integer i, indx, jmn, k, kk, km1
    real(kind=kind_phys) clam, cxlamu, xlamde, xlamdd
    
    logical totflg    
    real(kind=kind_phys) tf, tcr, tcrf
    
    real(kind=kind_phys) adw,aup,aafac, beta,    betal,   betas, c0, dellat,  delta, desdt,   dg, dh, dhh,dp, dq, dqsdp,   dqsdt,   dt, dt2,dtmax,   dtmin,   dv1h, dv1q,    dv2h,    dv2q,    dv1u, dv1v,    dv2u,    dv2v,    dv3q, dv3h,    dv3u,    dv3v, dz, dz1,e1, edtmax, edtmaxl, edtmaxs, el2orc,  elocp, es, etah,    cthk,    dthk, evef
    real(kind=kind_phys)    evfact,  evfactl, fact1, fact2,   factor, fjcap, fkm, g,  gamma,   pprime, qlk,qrch,    qs, c1, rain,    rfact,   shear,   tem1, val,val1, val2,    w1, w1l,w1s, w2, w2l,w2s,w3, w3l,w3s,w4, w4l, w4s,xdby,    xpw,xpwd, xqrch,   mbdt,    tem, ptem,    ptem1,   pgcon
    real(kind=kind_phys) cincr, cincrmax, cincrmin

    ! physical parameters
    parameter(g=grav)
    parameter(elocp=hvap/cp, el2orc=hvap*hvap/(rv*cp))
    parameter(c0=.002,c1=.002,delta=fv)
    parameter(fact1=(cvap-cliq)/rv,fact2=hvap/rv-fact1*t0c)
    parameter(cthk=150.,cincrmax=180.,cincrmin=120.,dthk=25.)

    real(kind=kind_phys) pcrit(15), acritt(15), acrit(15)
    data pcrit/850.,800.,750.,700.,650.,600.,550.,500.,450.,400.,350.,300.,250.,200.,150./
    data acritt/.0633,.0445,.0553,.0664,.075,.1082,.1521,.2216,.3151,.3677,.41,.5255,.7663,1.1686,1.6851/
    parameter(tf=233.16, tcr=263.16, tcrf=1.0/(tcr-tf))
    ! -------------------------------------------------------------------------------------- 

    ! ALLOCATABLE ARRAYS - 1D - (im)
    ! -------------------------------------------------------------------------------------- 
    integer, DIMENSION(:), allocatable ::               kbot, ktop, kcnv, kb, kbcon, kbcon1, ktcon, ktcon1, jmin, lmin, kbmax, kbm, kmax, islimsk
    real(kind=kind_phys), DIMENSION(:), allocatable ::  psp, cldwrk, rn, ps
    real(kind=kind_phys), DIMENSION(:), allocatable ::  qcond,  qevap, rntot,   vshear, xaa0, xk, xlamd, xmb,xmbmax, xpwav, xpwev,   delubar,delvbar
    real(kind=kind_phys), DIMENSION(:), allocatable ::  aa1,     acrt,   acrtfct, delhbar, delq,   delq2, delqbar, delqev, deltbar, deltv,   dtconv, edt, edto, edtx,   fld, hmax,   hmin, aa2, pbcdif,  pdot, pwavo,   pwevo,  xlamud
    logical, DIMENSION(:), allocatable ::               cnvflg, flg
    real(kind=kind_phys), DIMENSION(:), allocatable ::  qlko_ktcon, tx1, sumx
    ! -------------------------------------------------------------------------------------- 
    
    ! ALLOCATABLE ARRAYS - 2D - (ix, km)
    ! -------------------------------------------------------------------------------------- 
    real(kind=kind_phys), DIMENSION(:,:), allocatable :: delp, prslp, del,  prsl, q1, t1, u1,  v1, dot, phil, cnvw, cnvc
    ! -------------------------------------------------------------------------------------- 
   
    ! ALLOCATABLE ARRAYS - 2D - (im, km)
    ! -------------------------------------------------------------------------------------- 
    real(kind=kind_phys), DIMENSION(:,:), allocatable ::    ud_mf, dd_mf, dt_mf                                         
    real(kind=kind_phys), DIMENSION(:,:), allocatable ::    hcdo, po, ucdo, vcdo, qcdo                                  
    real(kind=kind_phys), DIMENSION(:,:), allocatable ::    pfld,to, qo, uo,  vo, qeso                                  
    real(kind=kind_phys), DIMENSION(:,:), allocatable ::    dellal, tvo, dbyo, zo,  xlamue, fent1                      
    real(kind=kind_phys), DIMENSION(:,:), allocatable ::    fent2,  frh, heo, heso, qrcd                                
    real(kind=kind_phys), DIMENSION(:,:), allocatable ::    dellah, dellaq, dellau,  dellav, hcko, ucko,    vcko        
    real(kind=kind_phys), DIMENSION(:,:), allocatable ::    qcko, eta, etad,   zi, qrcko,   qrcdo, pwo, pwdo, cnvwt     
    ! -------------------------------------------------------------------------------------- 
    

    ! ALLOCATABLE ARRAYS - 3D - (ix, km, 2)
    ! -------------------------------------------------------------------------------------- 
    ! real(kind=kind_phys), DIMENSION(:,:,:), allocatable :: ql
    ! -------------------------------------------------------------------------------------- 

contains




    subroutine convert_input_pa_to_cb()
        ps   = psp   * 0.001
        prsl = prslp * 0.001
        del  = delp  * 0.001
    end subroutine convert_input_pa_to_cb




    subroutine init_arrays_and_tunable_params()
        km1 = km - 1
        ! initialize arrays
        do i=1,im
            cnvflg(i) = .true.
            rn(i)=0.
            kbot(i)=km+1
            ktop(i)=0
            kbcon(i)=km
            ktcon(i)=1
            dtconv(i) = 3600.
            cldwrk(i) = 0.
            pdot(i) = 0.
            pbcdif(i)= 0.
            lmin(i) = 1
            jmin(i) = 1
            qlko_ktcon(i) = 0.
            edt(i)  = 0.
            edto(i) = 0.
            edtx(i) = 0.
            acrt(i) = 0.
            
            acrtfct(i) = 1.
            aa1(i)  = 0.
            aa2(i)  = 0.
            xaa0(i) = 0.
            pwavo(i)= 0.
            pwevo(i)= 0.
            xpwav(i)= 0.
            xpwev(i)= 0.
            vshear(i) = 0.
        enddo
        do k = 1, km
            do i = 1, im
            cnvw(i,k) = 0.
            cnvc(i,k) = 0.
            enddo
        enddo
    ! hchuang code change
        do k = 1, km
            do i = 1, im
            ud_mf(i,k) = 0.
            dd_mf(i,k) = 0.
            dt_mf(i,k) = 0.
            enddo
        enddo
    !
        do k = 1, 15
            acrit(k) = acritt(k) * (975. - pcrit(k))
        enddo
        dt2 = delt
        val   =         1200.
        dtmin = max(dt2, val )
        val   =         3600.
        dtmax = max(dt2, val )
    !  model tunable parameters are all here
        mbdt    = 10.
        edtmaxl = .3
        edtmaxs = .3
        clam    = .1
        aafac   = .1
    !     betal   = .15
    !     betas   = .15
        betal   = .05
        betas   = .05
    !     evef    = 0.07
        evfact  = 0.3
        evfactl = 0.3
    !
        cxlamu  = 1.0e-4
        xlamde  = 1.0e-4
        xlamdd  = 1.0e-4
    !
    !     pgcon   = 0.7     ! gregory et al. (1997, qjrms)
        pgcon   = 0.55    ! zhang & wu (2003,jas)
        fjcap   = (float(jcap) / 126.) ** 2
        val     =           1.
        fjcap   = max(fjcap,val)
        fkm     = (float(km) / 28.) ** 2
        fkm     = max(fkm,val)
        w1l     = -8.e-3
        w2l     = -4.e-2
        w3l     = -5.e-3
        w4l     = -5.e-4
        w1s     = -2.e-4
        w2s     = -2.e-3
        w3s     = -1.e-3
        w4s     = -2.e-5
    end subroutine init_arrays_and_tunable_params







    subroutine define_top_layer()
        !
        !  define top layer for search of the downdraft originating layer
        !  and the maximum thetae for updraft
        !
            do i=1,im
                kbmax(i) = km
                kbm(i)   = km
                kmax(i)  = km
                tx1(i)   = 1.0 / ps(i)
                ! print *, "ps = ", ps(i), "tx1 = ", tx1(i)
            enddo
        !

            ! print *,"prsl min = ", MINVAL(prsl)
            ! print *,"prsl max = ", MAXVAL(prsl)
            ! pause
            do k = 1, km
                do i=1,im
                    if (prsl(i,k)*tx1(i) .gt. 0.04) kmax(i)  = k + 1
                    if (prsl(i,k)*tx1(i) .gt. 0.45) kbmax(i) = k + 1
                    if (prsl(i,k)*tx1(i) .gt. 0.70) kbm(i)   = k + 1
                    ! print *, prsl(i,k)
                    ! print *, tx1(i)
                    ! print *, "ps = ", ps(i)
                    ! print *, "prsl = ", prsl(i,k)
                enddo
                    ! pause
            enddo
            do i=1,im
                kmax(i)  = min(km,kmax(i))
                kbmax(i) = min(kbmax(i),kmax(i))
                kbm(i)   = min(kbm(i),kmax(i))
            enddo
    end subroutine define_top_layer





    subroutine init_hydrostatic_height_and_entr_rate()
        !
        !  hydrostatic height assume zero terr and initially assume
        !    updraft entrainment rate as an inverse subroutine of height
        !
            do k = 1, km
                do i=1,im
                    zo(i,k) = phil(i,k) / g
                enddo
            enddo
            do k = 1, km1
                do i=1,im
                    zi(i,k) = 0.5*(zo(i,k)+zo(i,k+1))
                    xlamue(i,k) = clam / zi(i,k)
                enddo
            enddo
    end subroutine init_hydrostatic_height_and_entr_rate





    subroutine convert_from_cb_to_mb()
       do k = 1, km
            do i = 1, im
                if (k .le. kmax(i)) then
                    pfld(i,k) = prsl(i,k) * 10.0
                    eta(i,k)  = 1.
                    fent1(i,k)= 1.
                    fent2(i,k)= 1.
                    frh(i,k)  = 0.
                    hcko(i,k) = 0.
                    qcko(i,k) = 0.
                    qrcko(i,k)= 0.
                    ucko(i,k) = 0.
                    vcko(i,k) = 0.
                    etad(i,k) = 1.
                    hcdo(i,k) = 0.
                    qcdo(i,k) = 0.
                    ucdo(i,k) = 0.
                    vcdo(i,k) = 0.
                    qrcd(i,k) = 0.
                    qrcdo(i,k)= 0.
                    dbyo(i,k) = 0.
                    pwo(i,k)  = 0.
                    pwdo(i,k) = 0.
                    dellal(i,k) = 0.
                    to(i,k)   = t1(i,k)
                    qo(i,k)   = q1(i,k)
                    uo(i,k)   = u1(i,k)
                    vo(i,k)   = v1(i,k)
        !           uo(i,k)   = u1(i,k) * rcs(i)
        !           vo(i,k)   = v1(i,k) * rcs(i)
                    cnvwt(i,k)= 0.
                endif
            enddo
       enddo
    end subroutine convert_from_cb_to_mb





    subroutine init_column_variables()
        !
        !  column variables
        !  p is pressure of the layer (mb)
        !  t is temperature at t-dt (k)..tn
        !  q is mixing ratio at t-dt (kg/kg)..qn
        !  to is temperature at t+dt (k)... this is after advection and turbulan
        !  qo is mixing ratio at t+dt (kg/kg)..q1
        !
        do k = 1, km
            do i=1,im
                if (k .le. kmax(i)) then
                    ! print *, "before fpvsx", to(i,k)
                    qeso(i,k) = 0.01 * fpvsx(to(i,k))      ! fpvsx is in pa
                    ! print *, "after fpvsx", fpvsx(to(i,k))
                    ! pause
                    qeso(i,k) = eps * qeso(i,k) / (pfld(i,k) + epsm1*qeso(i,k))
                    val1      =             1.e-8
                    qeso(i,k) = max(qeso(i,k), val1)
                    val2      =           1.e-10
                    qo(i,k)   = max(qo(i,k), val2 )
        !           qo(i,k)   = min(qo(i,k),qeso(i,k))
        !           tvo(i,k)  = to(i,k) + delta * to(i,k) * qo(i,k)
                endif
            enddo
        enddo
    end subroutine init_column_variables




    subroutine computeMSE()
        !
        !  compute moist static energy
        !
        ! km = vertical layer dimension
        ! im = number of used points
        ! phil = g*z, layer geopotential
        ! to = temperature at t+dt (k)
        ! hvap = constant
        ! cp = constant
        ! qo = mixing ratio at t+dt (kg/kg)
        ! qeso = 
        ! heo = MSE
        ! heso = saturation MSE

        do k = 1, km
            do i=1,im
                if (k .le. kmax(i)) then
        !           tem       = g * zo(i,k) + cp * to(i,k)
                    tem       = phil(i,k) + cp * to(i,k)
                    heo(i,k)  = tem  + hvap * qo(i,k)
                    heso(i,k) = tem  + hvap * qeso(i,k)
        !           heo(i,k)  = min(heo(i,k),heso(i,k))
                endif
            enddo
        enddo
    end subroutine computeMSE



end module sascnvn_init
