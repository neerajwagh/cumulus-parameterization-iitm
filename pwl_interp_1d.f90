subroutine pwl_value_1d ( nd, xd, yd, ni, xi, yi )

!*****************************************************************************80
!
!! PWL_VALUE_1D evaluates the piecewise linear interpolant.
!
!  Discussion:
!
!    The piecewise linear interpolant L(ND,XD,YD)(X) is the piecewise
!    linear function which interpolates the data (XD(I),YD(I)) for I = 1
!    to ND.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    22 September 2012
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, integer ( kind = 4 ) ND, the number of data points.
!    ND must be at least 1.
!
!    Input, real ( kind = 8 ) XD(ND), the data points.
!
!    Input, real ( kind = 8 ) YD(ND), the data values.
!
!    Input, integer ( kind = 4 ) NI, the number of interpolation points.
!
!    Input, real ( kind = 8 ) XI(NI), the interpolation points.
!
!    Output, real ( kind = 8 ) YI(NI), the interpolated values.
!
  use machine , only : kind_phys
  implicit none

  integer ( kind = 4 ) nd             !17
  integer ( kind = 4 ) ni             !29

  integer ( kind = 4 ) i
  integer ( kind = 4 ) k
  
  real ( kind = kind_phys ) current_level
  real ( kind = kind_phys ) x

  real ( kind = kind_phys ) slope
  real ( kind = kind_phys ) intercept
  
  real ( kind = kind_phys ) x2
  real ( kind = kind_phys ) y2
  
  real ( kind = kind_phys ) x1
  real ( kind = kind_phys ) y1
  
  logical case1_flag
  logical case2_flag
  logical case3_flag
  
  real ( kind = kind_phys ) xd(nd)    !1000mb...30mb, 17 values, from netcdf file
  real ( kind = kind_phys ) yd(nd)    !17 values of netcdf variable, from netcdf file
  
  real ( kind = kind_phys ) xi(ni)    !1000mb...30mb, 29 interpol values with 25mb gap
  real ( kind = kind_phys ) yi(ni)    !find these 29 values and return

  yi(1:ni) = 0.0D+00

  ! -----------------------------------------------------
  ! CUSTOM INTERPOLATION STARTS
  do i = 1, ni

    case1_flag = .false.
    case2_flag = .false.
    case3_flag = .false.
    current_level = xi(i)
    
    ! CASE 1
    ! if current level is same as original, copy value of netcdf variable as it is
    do k = 1, nd
      if (current_level == xd(k)) then
        yi(i) = yd(k)
        case1_flag = .true.
        exit
      end if
    end do


    if (case1_flag .eqv. .true.) cycle
    ! CASE 2
    ! current level falls in between 2 orig levels, interpolate
    do k = 1, nd-1
      ! CAREFUL - levels are decreasing!
      if (xd(k) > current_level .and. current_level > xd(k+1)) then
        
        x1 = xd(k)
        y1 = yd(k)

        y2 = yd(k+1)
        x2 = xd(k+1)

        slope = (y2 - y1) / (x2 - x1)
        intercept = y2 - (slope * x2) ! or intercept = y1 - slope*x1

        x = current_level
        yi(i) = (slope * x) + intercept

        case2_flag = .true.
        exit
      end if
    end do


    if (case2_flag .eqv. .true.) cycle
    ! CASE 3
    ! current level is lower than least orig level, extrapolate using last 2 original levels
    ! CAREFUL - levels are decreasing!
    if (current_level < xd(nd)) then
      
      x1 = xd(nd-1)
      y1 = yd(nd-1)

      y2 = yd(nd)
      x2 = xd(nd)

      slope = (y2 - y1) / (x2 - x1)
      intercept = y2 - (slope * x2) ! or intercept = y1 - slope*x1

      x = current_level
      yi(i) = (slope * x) + intercept

      case3_flag = .true.
    end if


    if (case3_flag .eqv. .true.) cycle
    ! CASE 4
    ! current level is higher than highest orig level, extrapolate using first 2 orig levels
    ! CAREFUL - levels are decreasing!

    if (current_level > xd(1)) then
      
      x1 = xd(1)
      y1 = yd(1)

      y2 = yd(2)
      x2 = xd(2)

      slope = (y2 - y1) / (x2 - x1)
      intercept = y2 - (slope * x2) ! or intercept = y1 - slope*x1

      x = current_level
      yi(i) = (slope * x) + intercept
    end if

  ! next level
  end do
  ! all levels calculated, return to calling program
  return
end
  ! CUSTOM INTERPOLATION ENDS
  ! -----------------------------------------------------
  
  
  ! ORIGINAL CODE FOR pwl_value_1d:
  ! if ( nd == 1 ) then
  !   yi(1:ni) = yd(1)
  !   return
  ! end if

  ! do i = 1, ni

  !   if ( xi(i) <= xd(1) ) then

  !     t = ( xi(i) - xd(1) ) / ( xd(2) - xd(1) )
  !     yi(i) = ( 1.0D+00 - t ) * yd(1) + t * yd(2)

  !   else if ( xd(nd) <= xi(i) ) then

  !     t = ( xi(i) - xd(nd-1) ) / ( xd(nd) - xd(nd-1) )
  !     yi(i) = ( 1.0D+00 - t ) * yd(nd-1) + t * yd(nd)

  !   else

  !     do k = 2, nd

  !       if ( xd(k-1) <= xi(i) .and. xi(i) <= xd(k) ) then

  !         t = ( xi(i) - xd(k-1) ) / ( xd(k) - xd(k-1) )
  !         yi(i) = ( 1.0D+00 - t ) * yd(k-1) + t * yd(k)
  !         exit

  !       end if

  !     end do

  !   end if

  ! end do
  ! return
  ! end
