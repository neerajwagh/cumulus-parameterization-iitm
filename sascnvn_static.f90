module sascnvn_static
    use sascnvn_init
    implicit none
contains

    subroutine determine_max_MSE_level()
        !
        !  determine level with largest moist static energy
        !  this is the level where updraft starts
        !
        do i = 1, im
            hmax(i) = heo(i,1)
            kb(i)   = 1
        enddo
        do k = 2, km
            do i=1,im
                if (k .le. kbm(i)) then
                    if(heo(i,k) .gt. hmax(i)) then
                        kb(i)   = k
                        hmax(i) = heo(i,k)
                    endif
                endif
            enddo
        enddo

        ! print *, "hmax :", hmax

        do k = 1, km1
            do i=1,im
                if (k .le. kmax(i)-1) then
                    dz      = .5 * (zo(i,k+1) - zo(i,k))
                    dp      = .5 * (pfld(i,k+1) - pfld(i,k))
                    es      = 0.01 * fpvsx(to(i,k+1))      ! fpvsx is in pa
                    pprime  = pfld(i,k+1) + epsm1 * es
                    qs      = eps * es / pprime
                    dqsdp   = - qs / pprime
                    desdt   = es * (fact1 / to(i,k+1) + fact2 / (to(i,k+1)**2))
                    dqsdt   = qs * pfld(i,k+1) * desdt / (es * pprime)
                    gamma   = el2orc * qeso(i,k+1) / (to(i,k+1)**2)
                    dt      = (g * dz + hvap * dqsdp * dp) / (cp * (1. + gamma))
                    dq      = dqsdt * dt + dqsdp * dp
                    to(i,k) = to(i,k+1) + dt
                    qo(i,k) = qo(i,k+1) + dq
                    po(i,k) = .5 * (pfld(i,k) + pfld(i,k+1))
                endif
            enddo
        enddo

        do k = 1, km1
            do i=1,im
                if (k .le. kmax(i)-1) then
                    qeso(i,k) = 0.01 * fpvsx(to(i,k))      ! fpvsx is in pa
                    qeso(i,k) = eps * qeso(i,k) / (po(i,k) + epsm1*qeso(i,k))
                    val1      =             1.e-8
                    qeso(i,k) = max(qeso(i,k), val1)
                    val2      =           1.e-10
                    qo(i,k)   = max(qo(i,k), val2 )
        !           qo(i,k)   = min(qo(i,k),qeso(i,k))
                    frh(i,k)  = 1. - min(qo(i,k)/qeso(i,k), 1.)
                    heo(i,k)  = .5 * g * (zo(i,k) + zo(i,k+1)) + cp * to(i,k) + hvap * qo(i,k)
                    heso(i,k) = .5 * g * (zo(i,k) + zo(i,k+1)) + cp * to(i,k) + hvap * qeso(i,k)
                    uo(i,k)   = .5 * (uo(i,k) + uo(i,k+1))
                    vo(i,k)   = .5 * (vo(i,k) + vo(i,k+1))
                endif
            enddo
        enddo

    end subroutine determine_max_MSE_level

    subroutine find_level_of_free_convection()
    !
    !  look for the level of free convection as cloud base
    !
        do i=1,im
            flg(i)   = .true.
            kbcon(i) = kmax(i)
        enddo
        do k = 1, km1
            do i=1,im
                if (flg(i).and.k.le.kbmax(i)) then
                    if(k.gt.kb(i).and.heo(i,kb(i)).gt.heso(i,k)) then
                        kbcon(i) = k
                        flg(i)   = .false.
                    endif
                endif
            enddo
        enddo

        ! print *, "kbcon", kbcon
        ! pause
        do i=1,im
            if(kbcon(i).eq.kmax(i)) cnvflg(i) = .false.
        enddo

        totflg = .true.
        do i=1,im
            totflg = totflg .and. (.not. cnvflg(i))
        enddo
        ! if(totflg) return
    end subroutine find_level_of_free_convection






    subroutine find_critical_convective_inhibition()
    !
    !  determine critical convective inhibition
    !  as a function of vertical velocity at cloud base.
    !
        do i=1,im
            if(cnvflg(i)) then
    !         pdot(i)  = 10.* dot(i,kbcon(i))
            pdot(i)  = 0.01 * dot(i,kbcon(i)) ! now dot is in pa/s
            endif
        enddo
        do i=1,im
            if(cnvflg(i)) then
                if(islimsk(i) == 1) then
                    w1 = w1l
                    w2 = w2l
                    w3 = w3l
                    w4 = w4l
                else
                    w1 = w1s
                    w2 = w2s
                    w3 = w3s
                    w4 = w4s
                endif
                if(pdot(i).le.w4) then
                    tem = (pdot(i) - w4) / (w3 - w4)
                elseif(pdot(i).ge.-w4) then
                    tem = - (pdot(i) + w4) / (w4 - w3)
                else
                    tem = 0.
                endif
                val1    =             -1.
                tem = max(tem,val1)
                val2    =             1.
                tem = min(tem,val2)
                tem = 1. - tem
                tem1= .5*(cincrmax-cincrmin)
                cincr = cincrmax - tem * tem1
                pbcdif(i) = pfld(i,kb(i)) - pfld(i,kbcon(i))
                if(pbcdif(i).gt.cincr) then
                    cnvflg(i) = .false.
                endif
            endif
        enddo

        totflg = .true.
        do i=1,im
            totflg = totflg .and. (.not. cnvflg(i))
        enddo
        ! if(totflg) return

    end subroutine find_critical_convective_inhibition









    subroutine assume_entrainment_detrainment_rate()
    !
    !  assume that updraft entrainment rate above cloud base is
    !    same as that at cloud base
    !
        do k = 2, km1
            do i=1,im
                if(cnvflg(i) .and. (k.gt.kbcon(i).and.k.lt.kmax(i))) then
                    xlamue(i,k) = xlamue(i,kbcon(i))
                endif
            enddo
        enddo
    !
    !  assume the detrainment rate for the updrafts to be same as
    !  the entrainment rate at cloud base
    !
        do i = 1, im
            if(cnvflg(i)) then
                xlamud(i) = xlamue(i,kbcon(i))
            endif
        enddo
    end subroutine assume_entrainment_detrainment_rate








    subroutine functions_mimic_cloud_ensemble()
    !
    !  functions rapidly decreasing with height, mimicking a cloud ensemble
    !    (bechtold et al., 2008)
    !
        do k = 2, km1
            do i=1,im
                if(cnvflg(i) .and. (k.gt.kbcon(i).and.k.lt.kmax(i))) then
                    tem = qeso(i,k)/qeso(i,kbcon(i))
                    fent1(i,k) = tem**2
                    fent2(i,k) = tem**3
                endif
            enddo
        enddo
    end subroutine functions_mimic_cloud_ensemble









    subroutine final_entrainment_rate()
    !
    !  final entrainment rate as the sum of turbulent part and organized entrainment
    !    depending on the environmental relative humidity
    !    (bechtold et al., 2008)
    !
        do k = 2, km1
            do i=1,im
                if(cnvflg(i) .and. (k.ge.kbcon(i).and.k.lt.kmax(i))) then
                    tem = cxlamu * frh(i,k) * fent2(i,k)
                    xlamue(i,k) = xlamue(i,k)*fent1(i,k) + tem
                endif
            enddo
        enddo

    end subroutine final_entrainment_rate









    subroutine find_updraft_mass_flux()
    !
    !  determine updraft mass flux for the subcloud layers
    !
        do k = km1, 1, -1
            do i = 1, im
                if (cnvflg(i)) then
                    if(k.lt.kbcon(i).and.k.ge.kb(i)) then
                    dz       = zi(i,k+1) - zi(i,k)
                    ptem     = 0.5*(xlamue(i,k)+xlamue(i,k+1))-xlamud(i)
                    eta(i,k) = eta(i,k+1) / (1. + ptem * dz)
                    endif
                endif
            enddo
        enddo
    end subroutine find_updraft_mass_flux







    subroutine mass_flux_above_cloud_base()
    !
    !  compute mass flux above cloud base
    !
        do k = 2, km1
            do i = 1, im
                if(cnvflg(i))then
                    if(k.gt.kbcon(i).and.k.lt.kmax(i)) then
                        dz       = zi(i,k) - zi(i,k-1)
                        ptem     = 0.5*(xlamue(i,k)+xlamue(i,k-1))-xlamud(i)
                        eta(i,k) = eta(i,k-1) * (1 + ptem * dz)
                    endif
                endif
            enddo
        enddo
    end subroutine mass_flux_above_cloud_base




    subroutine updraft_cloud_properties()
    !
    !  compute updraft cloud properties
    !
        do i = 1, im
            if(cnvflg(i)) then
                indx         = kb(i)
                hcko(i,indx) = heo(i,indx)
                ucko(i,indx) = uo(i,indx)
                vcko(i,indx) = vo(i,indx)
                pwavo(i)     = 0.
            endif
        enddo
    !
    !  cloud property is modified by the entrainment process
    !
        do k = 2, km1
            do i = 1, im
                if (cnvflg(i)) then
                    if(k.gt.kb(i).and.k.lt.kmax(i)) then
                        dz   = zi(i,k) - zi(i,k-1)
                        tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
                        tem1 = 0.5 * xlamud(i) * dz
                        factor = 1. + tem - tem1
                        ptem = 0.5 * tem + pgcon
                        ptem1= 0.5 * tem - pgcon
                        hcko(i,k) = ((1.-tem1)*hcko(i,k-1)+tem*0.5*(heo(i,k)+heo(i,k-1)))/factor
                        ucko(i,k) = ((1.-tem1)*ucko(i,k-1)+ptem*uo(i,k)+ptem1*uo(i,k-1))/factor
                        vcko(i,k) = ((1.-tem1)*vcko(i,k-1)+ptem*vo(i,k)+ptem1*vo(i,k-1))/factor
                        dbyo(i,k) = hcko(i,k) - heso(i,k)
                    endif
                endif
            enddo
        enddo
    end subroutine updraft_cloud_properties



    subroutine consider_convection_inhibition()
    !
    !   taking account into convection inhibition due to existence of
    !    dry layers below cloud base
    !
        do i=1,im
            flg(i) = cnvflg(i)
            kbcon1(i) = kmax(i)
        enddo
        do k = 2, km1
        do i=1,im
            if (flg(i).and.k.lt.kmax(i)) then
            if(k.ge.kbcon(i).and.dbyo(i,k).gt.0.) then
                kbcon1(i) = k
                flg(i)    = .false.
            endif
            endif
        enddo
        enddo
        do i=1,im
            if(cnvflg(i)) then
            if(kbcon1(i).eq.kmax(i)) cnvflg(i) = .false.
            endif
        enddo
        do i=1,im
            if(cnvflg(i)) then
            tem = pfld(i,kbcon(i)) - pfld(i,kbcon1(i))
            if(tem.gt.dthk) then
                cnvflg(i) = .false.
            endif
            endif
        enddo
    !!
        totflg = .true.
        do i = 1, im
            totflg = totflg .and. (.not. cnvflg(i))
        enddo
        ! if(totflg) return
    !!
    end subroutine consider_convection_inhibition



    subroutine first_guess_cloud_top()
    !
    !  determine first guess cloud top as the level of zero buoyancy
    !
        do i = 1, im
            flg(i) = cnvflg(i)
            ktcon(i) = 1
        enddo
        do k = 2, km1
        do i = 1, im
            if (flg(i).and.k .lt. kmax(i)) then
            if(k.gt.kbcon1(i).and.dbyo(i,k).lt.0.) then
                ktcon(i) = k
                flg(i)   = .false.
            endif
            endif
        enddo
        enddo
    !
        do i = 1, im
            if(cnvflg(i)) then
            tem = pfld(i,kbcon(i))-pfld(i,ktcon(i))
            if(tem.lt.cthk) cnvflg(i) = .false.
            endif
        enddo
    !!
        totflg = .true.
        do i = 1, im
            totflg = totflg .and. (.not. cnvflg(i))
        enddo
        ! if(totflg) return
    !!
    end subroutine first_guess_cloud_top



    subroutine search_downdraft_originating_layer()
    !
    !  search for downdraft originating level above theta-e minimum
    !
        do i = 1, im
            if(cnvflg(i)) then
            hmin(i) = heo(i,kbcon1(i))
            lmin(i) = kbmax(i)
            jmin(i) = kbmax(i)
            endif
        enddo
        do k = 2, km1
            do i = 1, im
            if (cnvflg(i) .and. k .le. kbmax(i)) then
                if(k.gt.kbcon1(i).and.heo(i,k).lt.hmin(i)) then
                lmin(i) = k + 1
                hmin(i) = heo(i,k)
                endif
            endif
            enddo
        enddo
    end subroutine search_downdraft_originating_layer



    subroutine verify_jmin_within_cloud ()
    !
    !  make sure that jmin(i) is within the cloud
    !
        do i = 1, im
            if(cnvflg(i)) then
            jmin(i) = min(lmin(i),ktcon(i)-1)
            jmin(i) = max(jmin(i),kbcon1(i)+1)
            if(jmin(i).ge.ktcon(i)) cnvflg(i) = .false.
            endif
        enddo
    end subroutine verify_jmin_within_cloud 



    subroutine upper_limit_mass_flux()
    !
    !  specify upper limit of mass flux at cloud base
    !
        do i = 1, im
            if(cnvflg(i)) then
    !         xmbmax(i) = .1
    !
            k = kbcon(i)
            dp = 1000. * del(i,k)
            xmbmax(i) = dp / (g * dt2)
    !
    !         tem = dp / (g * dt2)
    !         xmbmax(i) = min(tem, xmbmax(i))
            endif
        enddo
    end subroutine upper_limit_mass_flux



    subroutine find_cloud_moisture_and_precipitation()
 !
 !  compute cloud moisture property and precipitation
 !
       do i = 1, im
         if (cnvflg(i)) then
           aa1(i) = 0.
           qcko(i,kb(i)) = qo(i,kb(i))
           qrcko(i,kb(i)) = qo(i,kb(i))
 !         rhbar(i) = 0.
         endif
       enddo
       do k = 2, km1
         do i = 1, im
           if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.lt.ktcon(i)) then
               dz    = zi(i,k) - zi(i,k-1)
               gamma = el2orc * qeso(i,k) / (to(i,k)**2)
               qrch = qeso(i,k)+ gamma * dbyo(i,k) / (hvap * (1. + gamma))
 
               tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
               tem1 = 0.5 * xlamud(i) * dz
               factor = 1. + tem - tem1
               qcko(i,k) = ((1.-tem1)*qcko(i,k-1)+tem*0.5*(qo(i,k)+qo(i,k-1)))/factor
               qrcko(i,k) = qcko(i,k)
 
               dq = eta(i,k) * (qcko(i,k) - qrch)
 !
 !             rhbar(i) = rhbar(i) + qo(i,k) / qeso(i,k)
 !
 !  check if there is excess moisture to release latent heat
 !
               if(k.ge.kbcon(i).and.dq.gt.0.) then
                 etah = .5 * (eta(i,k) + eta(i,k-1))
                 if(ncloud.gt.0..and.k.gt.jmin(i)) then
                   dp = 1000. * del(i,k)
                   qlk = dq / (eta(i,k) + etah * (c0 + c1) * dz)
                   dellal(i,k) = etah * c1 * dz * qlk * g / dp
                 else
                   qlk = dq / (eta(i,k) + etah * c0 * dz)
                 endif
                 aa1(i) = aa1(i) - dz * g * qlk
                 qcko(i,k) = qlk + qrch
                 pwo(i,k) = etah * c0 * dz * qlk
                 pwavo(i) = pwavo(i) + pwo(i,k)
 !               cnvwt(i,k) = (etah*qlk + pwo(i,k)) * g / dp
                 cnvwt(i,k) = etah * qlk * g / dp
               endif
             endif
           endif
         enddo
       enddo
 !     do i = 1, im
 !       if(cnvflg(i)) then
 !         indx = ktcon(i) - kb(i) - 1
 !         rhbar(i) = rhbar(i) / float(indx)
 !       endif
 !     enddo
    end subroutine find_cloud_moisture_and_precipitation



    subroutine find_cloud_work_function()
 !
 !  calculate cloud work function
 !
       do k = 2, km1
         do i = 1, im
           if (cnvflg(i)) then
             if(k.ge.kbcon(i).and.k.lt.ktcon(i)) then
               dz1 = zo(i,k+1) - zo(i,k)
               gamma = el2orc * qeso(i,k) / (to(i,k)**2)
               rfact =  1. + delta * cp * gamma* to(i,k) / hvap
               aa1(i) = aa1(i) +dz1 * (g / (cp * to(i,k)))* dbyo(i,k) / (1. + gamma)* rfact
               val = 0.
               aa1(i)=aa1(i)+dz1 * g * delta *max(val,(qeso(i,k) - qo(i,k)))
             endif
           endif
         enddo
       enddo
       do i = 1, im
         if(cnvflg(i).and.aa1(i).le.0.) cnvflg(i) = .false.
       enddo
 !!
       totflg = .true.
       do i=1,im
         totflg = totflg .and. (.not. cnvflg(i))
       enddo
    !    if(totflg) return
    end subroutine find_cloud_work_function



    subroutine estimate_convective_overshooting()
 !
 !  estimate the onvective overshooting as the level
 !    where the [aafac * cloud work function] becomes zero,
 !    which is the final cloud top
 !
       do i = 1, im
         if (cnvflg(i)) then
           aa2(i) = aafac * aa1(i)
         endif
       enddo
 !
       do i = 1, im
         flg(i) = cnvflg(i)
         ktcon1(i) = kmax(i) - 1
       enddo
       do k = 2, km1
         do i = 1, im
           if (flg(i)) then
             if(k.ge.ktcon(i).and.k.lt.kmax(i)) then
               dz1 = zo(i,k+1) - zo(i,k)
               gamma = el2orc * qeso(i,k) / (to(i,k)**2)
               rfact =  1. + delta * cp * gamma* to(i,k) / hvap
               aa2(i) = aa2(i) +dz1 * (g / (cp * to(i,k)))* dbyo(i,k) / (1. + gamma)* rfact
               if(aa2(i).lt.0.) then
                 ktcon1(i) = k
                 flg(i) = .false.
               endif
             endif
           endif
         enddo
       enddo
    end subroutine estimate_convective_overshooting



    subroutine find_properties()
 !
 !  compute cloud moisture property, detraining cloud water
 !    and precipitation in overshooting layers
 !
       do k = 2, km1
         do i = 1, im
           if (cnvflg(i)) then
             if(k.ge.ktcon(i).and.k.lt.ktcon1(i)) then
               dz    = zi(i,k) - zi(i,k-1)
               gamma = el2orc * qeso(i,k) / (to(i,k)**2)
               qrch = qeso(i,k) + gamma * dbyo(i,k) / (hvap * (1. + gamma))
               tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
               tem1 = 0.5 * xlamud(i) * dz
               factor = 1. + tem - tem1
               qcko(i,k) = ((1.-tem1)*qcko(i,k-1)+tem*0.5*(qo(i,k)+qo(i,k-1)))/factor
               qrcko(i,k) = qcko(i,k)
               dq = eta(i,k) * (qcko(i,k) - qrch)
 !
 !  check if there is excess moisture to release latent heat
 !
               if(dq.gt.0.) then
                 etah = .5 * (eta(i,k) + eta(i,k-1))
                 if(ncloud.gt.0.) then
                   dp = 1000. * del(i,k)
                   qlk = dq / (eta(i,k) + etah * (c0 + c1) * dz)
                   dellal(i,k) = etah * c1 * dz * qlk * g / dp
                 else
                   qlk = dq / (eta(i,k) + etah * c0 * dz)
                 endif
                 qcko(i,k) = qlk + qrch
                 pwo(i,k) = etah * c0 * dz * qlk
                 pwavo(i) = pwavo(i) + pwo(i,k)
 !               cnvwt(i,k) = (etah*qlk + pwo(i,k)) * g / dp
                 cnvwt(i,k) = etah * qlk * g / dp
               endif
             endif
           endif
         enddo
       enddo
    end subroutine find_properties



    subroutine exchange_ktcon_ktcon1()
 !
 ! exchange ktcon with ktcon1
 !
       do i = 1, im
         if(cnvflg(i)) then
           kk = ktcon(i)
           ktcon(i) = ktcon1(i)
           ktcon1(i) = kk
         endif
       enddo
    end subroutine exchange_ktcon_ktcon1



    subroutine find_liquid_vapor_separation()
    !
    !  this section is ready for cloud water
    !
        if(ncloud.gt.0) then
        !
        !  compute liquid and vapor separation at cloud top
        !
            do i = 1, im
                if(cnvflg(i)) then
                    k = ktcon(i) - 1
                    gamma = el2orc * qeso(i,k) / (to(i,k)**2)
                    qrch = qeso(i,k) + gamma * dbyo(i,k) / (hvap * (1. + gamma))
                    dq = qcko(i,k) - qrch
            !
            !  check if there is excess moisture to release latent heat
            !
                    if(dq.gt.0.) then
                        qlko_ktcon(i) = dq
                        qcko(i,k) = qrch
                    endif
                endif
            enddo
        endif
    end subroutine find_liquid_vapor_separation


 !
 !------- downdraft calculations
 !

    subroutine compute_precipitation_efficiency()
 !--- compute precipitation efficiency in terms of windshear
 !
       do i = 1, im
         if(cnvflg(i)) then
           vshear(i) = 0.
         endif
       enddo
       do k = 2, km
         do i = 1, im
           if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.le.ktcon(i)) then
               shear= sqrt((uo(i,k)-uo(i,k-1)) ** 2 + (vo(i,k)-vo(i,k-1)) ** 2)
               vshear(i) = vshear(i) + shear
             endif
           endif
         enddo
       enddo
       do i = 1, im
         if(cnvflg(i)) then
           vshear(i) = 1.e3 * vshear(i) / (zi(i,ktcon(i))-zi(i,kb(i)))
           e1=1.591-.639*vshear(i)+.0953*(vshear(i)**2)-.00496*(vshear(i)**3)
           edt(i)=1.-e1
           val =         .9
           edt(i) = min(edt(i),val)
           val =         .0
           edt(i) = max(edt(i),val)
           edto(i)=edt(i)
           edtx(i)=edt(i)
         endif
       enddo
    end subroutine compute_precipitation_efficiency



    subroutine determine_detrainment_rate()
 !
 !  determine detrainment rate between 1 and kbcon
 !
       do i = 1, im
         if(cnvflg(i)) then
           sumx(i) = 0.
         endif
       enddo
       do k = 1, km1
       do i = 1, im
         if(cnvflg(i).and.k.ge.1.and.k.lt.kbcon(i)) then
           dz = zi(i,k+1) - zi(i,k)
           sumx(i) = sumx(i) + dz
         endif
       enddo
       enddo
       do i = 1, im
         beta = betas
         if(islimsk(i) == 1) beta = betal
         if(cnvflg(i)) then
           dz  = (sumx(i)+zi(i,1))/float(kbcon(i))
           tem = 1./float(kbcon(i))
           xlamd(i) = (1.-beta**tem)/dz
         endif
       enddo
    end subroutine determine_detrainment_rate



    subroutine downdraft_mass_flux()
 !
 !  determine downdraft mass flux
 !
       do k = km1, 1, -1
         do i = 1, im
           if (cnvflg(i) .and. k .le. kmax(i)-1) then
            if(k.lt.jmin(i).and.k.ge.kbcon(i)) then
               dz        = zi(i,k+1) - zi(i,k)
               ptem      = xlamdd - xlamde
               etad(i,k) = etad(i,k+1) * (1. - ptem * dz)
            else if(k.lt.kbcon(i)) then
               dz        = zi(i,k+1) - zi(i,k)
               ptem      = xlamd(i) + xlamdd - xlamde
               etad(i,k) = etad(i,k+1) * (1. - ptem * dz)
            endif
           endif
         enddo
       enddo
    end subroutine downdraft_mass_flux



    subroutine downdraft_moisture_properties()
!
 !--- downdraft moisture properties
 !
       do i = 1, im
         if(cnvflg(i)) then
           jmn = jmin(i)
           hcdo(i,jmn) = heo(i,jmn)
           qcdo(i,jmn) = qo(i,jmn)
           qrcdo(i,jmn)= qo(i,jmn)
           ucdo(i,jmn) = uo(i,jmn)
           vcdo(i,jmn) = vo(i,jmn)
           pwevo(i) = 0.
         endif
       enddo
       do k = km1, 1, -1
         do i = 1, im
           if (cnvflg(i) .and. k.lt.jmin(i)) then
               dz = zi(i,k+1) - zi(i,k)
               if(k.ge.kbcon(i)) then
                  tem  = xlamde * dz
                  tem1 = 0.5 * xlamdd * dz
               else
                  tem  = xlamde * dz
                  tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
               endif
               factor = 1. + tem - tem1
               ptem = 0.5 * tem - pgcon
               ptem1= 0.5 * tem + pgcon
               hcdo(i,k) = ((1.-tem1)*hcdo(i,k+1)+tem*0.5*(heo(i,k)+heo(i,k+1)))/factor
               ucdo(i,k) = ((1.-tem1)*ucdo(i,k+1)+ptem*uo(i,k+1)+ptem1*uo(i,k))/factor
               vcdo(i,k) = ((1.-tem1)*vcdo(i,k+1)+ptem*vo(i,k+1)+ptem1*vo(i,k))/factor
               dbyo(i,k) = hcdo(i,k) - heso(i,k)
           endif
         enddo
       enddo
 !
       do k = km1, 1, -1
         do i = 1, im
           if (cnvflg(i).and.k.lt.jmin(i)) then
               gamma      = el2orc * qeso(i,k) / (to(i,k)**2)
               qrcdo(i,k) = qeso(i,k)+(1./hvap)*(gamma/(1.+gamma))*dbyo(i,k)
 !             detad      = etad(i,k+1) - etad(i,k)
               dz = zi(i,k+1) - zi(i,k)
               if(k.ge.kbcon(i)) then
                  tem  = xlamde * dz
                  tem1 = 0.5 * xlamdd * dz
               else
                  tem  = xlamde * dz
                  tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
               endif
               factor = 1. + tem - tem1
               qcdo(i,k) = ((1.-tem1)*qrcdo(i,k+1)+tem*0.5*(qo(i,k)+qo(i,k+1)))/factor
 !             pwdo(i,k)  = etad(i,k+1) * qcdo(i,k+1) -
 !    &                     etad(i,k) * qrcdo(i,k)
 !             pwdo(i,k)  = pwdo(i,k) - detad *
 !    &                    .5 * (qrcdo(i,k) + qrcdo(i,k+1))
               pwdo(i,k)  = etad(i,k) * (qcdo(i,k) - qrcdo(i,k))
               pwevo(i)   = pwevo(i) + pwdo(i,k)
           endif
         enddo
       enddo
    end subroutine downdraft_moisture_properties



    subroutine final_downdraft_strength ()
 !
 !--- final downdraft strength dependent on precip
 !--- efficiency (edt), normalized condensate (pwav), and
 !--- evaporate (pwev)
 !
       do i = 1, im
         edtmax = edtmaxl
         if(islimsk(i) == 0) edtmax = edtmaxs
         if(cnvflg(i)) then
           if(pwevo(i).lt.0.) then
             edto(i) = -edto(i) * pwavo(i) / pwevo(i)
             edto(i) = min(edto(i),edtmax)
           else
             edto(i) = 0.
           endif
         endif
       enddo
 !
    end subroutine final_downdraft_strength 


    subroutine downdraft_cloudwork_functions()
 !
 !--- downdraft cloudwork functions
 !
       do k = km1, 1, -1
         do i = 1, im
           if (cnvflg(i) .and. k .lt. jmin(i)) then
               gamma = el2orc * qeso(i,k) / to(i,k)**2
               dhh=hcdo(i,k)
               dt=to(i,k)
               dg=gamma
               dh=heso(i,k)
               dz=-1.*(zo(i,k+1)-zo(i,k))
               aa1(i)=aa1(i)+edto(i)*dz*(g/(cp*dt))*((dhh-dh)/(1.+dg))*(1.+delta*cp*dg*dt/hvap)
               val=0.
               aa1(i)=aa1(i)+edto(i)*dz*g*delta*max(val,(qeso(i,k)-qo(i,k)))
           endif
         enddo
       enddo
       do i = 1, im
         if(cnvflg(i).and.aa1(i).le.0.) then
            cnvflg(i) = .false.
         endif
       enddo
 !!
       totflg = .true.
       do i=1,im
         totflg = totflg .and. (.not. cnvflg(i))
       enddo
    !    if(totflg) return
 !!
    end subroutine downdraft_cloudwork_functions


    subroutine change_due_to_cloud_unit_mass()
 !
 !--- what would the change be, that a cloud with unit mass
 !--- will do to the environment?
 !
       do k = 1, km
         do i = 1, im
           if(cnvflg(i) .and. k .le. kmax(i)) then
             dellah(i,k) = 0.
             dellaq(i,k) = 0.
             dellau(i,k) = 0.
             dellav(i,k) = 0.
           endif
         enddo
       enddo
       do i = 1, im
         if(cnvflg(i)) then
           dp = 1000. * del(i,1)
           dellah(i,1) = edto(i) * etad(i,1) * (hcdo(i,1) - heo(i,1)) * g / dp
           dellaq(i,1) = edto(i) * etad(i,1) * (qrcdo(i,1) - qo(i,1)) * g / dp
           dellau(i,1) = edto(i) * etad(i,1) * (ucdo(i,1) - uo(i,1)) * g / dp
           dellav(i,1) = edto(i) * etad(i,1) * (vcdo(i,1) - vo(i,1)) * g / dp
         endif
       enddo
    end subroutine change_due_to_cloud_unit_mass


    subroutine change_due_to_subsidence_entrainment()
 !
 !--- changed due to subsidence and entrainment
 !
       do k = 2, km1
         do i = 1, im
           if (cnvflg(i).and.k.lt.ktcon(i)) then
               aup = 1.
               if(k.le.kb(i)) aup = 0.
               adw = 1.
               if(k.gt.jmin(i)) adw = 0.
               dp = 1000. * del(i,k)
               dz = zi(i,k) - zi(i,k-1)
 !
               dv1h = heo(i,k)
               dv2h = .5 * (heo(i,k) + heo(i,k-1))
               dv3h = heo(i,k-1)
               dv1q = qo(i,k)
               dv2q = .5 * (qo(i,k) + qo(i,k-1))
               dv3q = qo(i,k-1)
               dv1u = uo(i,k)
               dv2u = .5 * (uo(i,k) + uo(i,k-1))
               dv3u = uo(i,k-1)
               dv1v = vo(i,k)
               dv2v = .5 * (vo(i,k) + vo(i,k-1))
               dv3v = vo(i,k-1)
 !
               tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1))
               tem1 = xlamud(i)
 !
               if(k.le.kbcon(i)) then
                 ptem  = xlamde
                 ptem1 = xlamd(i)+xlamdd
               else
                 ptem  = xlamde
                 ptem1 = xlamdd
               endif
               dellah(i,k) = dellah(i,k) + ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1h- (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3h- (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2h*dz+  aup*tem1*eta(i,k-1)*.5*(hcko(i,k)+hcko(i,k-1))*dz+  adw*edto(i)*ptem1*etad(i,k)*.5*(hcdo(i,k)+hcdo(i,k-1))*dz     ) *g/dp
               
               dellaq(i,k) = dellaq(i,k) + ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1q- (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3q- (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2q*dz+  aup*tem1*eta(i,k-1)*.5*(qrcko(i,k)+qcko(i,k-1))*dz+  adw*edto(i)*ptem1*etad(i,k)*.5*(qrcdo(i,k)+qcdo(i,k-1))*dz     ) *g/dp
               
               dellau(i,k) = dellau(i,k) + ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1u- (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3u- (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2u*dz+  aup*tem1*eta(i,k-1)*.5*(ucko(i,k)+ucko(i,k-1))*dz+  adw*edto(i)*ptem1*etad(i,k)*.5*(ucdo(i,k)+ucdo(i,k-1))*dz-  pgcon*(aup*eta(i,k-1)-adw*edto(i)*etad(i,k))*(dv1u-dv3u)     ) *g/dp

               dellav(i,k) = dellav(i,k) + ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1v- (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3v- (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2v*dz+  aup*tem1*eta(i,k-1)*.5*(vcko(i,k)+vcko(i,k-1))*dz+  adw*edto(i)*ptem1*etad(i,k)*.5*(vcdo(i,k)+vcdo(i,k-1))*dz-  pgcon*(aup*eta(i,k-1)-adw*edto(i)*etad(i,k))*(dv1v-dv3v)     ) *g/dp
           
           endif
         enddo
       enddo
    end subroutine change_due_to_subsidence_entrainment


    subroutine cloud_top_and_water()
 !
 !------- cloud top
 !
       do i = 1, im
         if(cnvflg(i)) then
           indx = ktcon(i)
           dp = 1000. * del(i,indx)
           dv1h = heo(i,indx-1)
           dellah(i,indx) = eta(i,indx-1) * (hcko(i,indx-1) - dv1h) * g / dp
           dv1q = qo(i,indx-1)
           dellaq(i,indx) = eta(i,indx-1) * (qcko(i,indx-1) - dv1q) * g / dp
           dv1u = uo(i,indx-1)
           dellau(i,indx) = eta(i,indx-1) * (ucko(i,indx-1) - dv1u) * g / dp
           dv1v = vo(i,indx-1)
           dellav(i,indx) = eta(i,indx-1) * (vcko(i,indx-1) - dv1v) * g / dp
 !
 !  cloud water
 !
           dellal(i,indx) = eta(i,indx-1) * qlko_ktcon(i) * g / dp
         endif
       enddo
    end subroutine cloud_top_and_water


    subroutine final_variable_per_unit_mass_flux()
 !
 !------- final changed variable per unit mass flux
 !
       do k = 1, km
         do i = 1, im
           if (cnvflg(i).and.k .le. kmax(i)) then
             if(k.gt.ktcon(i)) then
               qo(i,k) = q1(i,k)
               to(i,k) = t1(i,k)
             endif
             if(k.le.ktcon(i)) then
               qo(i,k) = dellaq(i,k) * mbdt + q1(i,k)
               dellat = (dellah(i,k) - hvap * dellaq(i,k)) / cp
               to(i,k) = dellat * mbdt + t1(i,k)
               val   =           1.e-10
               qo(i,k) = max(qo(i,k), val  )
             endif
           endif
         enddo
       enddo
    end subroutine final_variable_per_unit_mass_flux

 !--- the above changed environment is now used to calulate the
 !--- effect the arbitrary cloud (with unit mass flux)
 !--- would have on the stability,
 !--- which then is used to calculate the real mass flux,
 !--- necessary to keep this change in balance with the large-scale
 !--- destabilization.




    subroutine environmental_conditions()
 !
 !--- environmental conditions again, first heights
 !
       do k = 1, km
         do i = 1, im
           if(cnvflg(i) .and. k .le. kmax(i)) then
             qeso(i,k) = 0.01 * fpvsx(to(i,k))      ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (pfld(i,k)+epsm1*qeso(i,k))
             val       =             1.e-8
             qeso(i,k) = max(qeso(i,k), val )
 !           tvo(i,k)  = to(i,k) + delta * to(i,k) * qo(i,k)
           endif
         enddo
       enddo
    end subroutine environmental_conditions





    subroutine recalculate_moist_static_energy()
 !
 !--- moist static energy
 !
 !! - Recalculate moist static energy and saturation moist static energy.
       do k = 1, km1
         do i = 1, im
           if(cnvflg(i) .and. k .le. kmax(i)-1) then
             dz = .5 * (zo(i,k+1) - zo(i,k))
             dp = .5 * (pfld(i,k+1) - pfld(i,k))
             es = 0.01 * fpvsx(to(i,k+1))      ! fpvsx is in pa
             pprime = pfld(i,k+1) + epsm1 * es
             qs = eps * es / pprime
             dqsdp = - qs / pprime
             desdt = es * (fact1 / to(i,k+1) + fact2 / (to(i,k+1)**2))
             dqsdt = qs * pfld(i,k+1) * desdt / (es * pprime)
             gamma = el2orc * qeso(i,k+1) / (to(i,k+1)**2)
             dt = (g * dz + hvap * dqsdp * dp) / (cp * (1. + gamma))
             dq = dqsdt * dt + dqsdp * dp
             to(i,k) = to(i,k+1) + dt
             qo(i,k) = qo(i,k+1) + dq
             po(i,k) = .5 * (pfld(i,k) + pfld(i,k+1))
           endif
         enddo
       enddo
       do k = 1, km1
         do i = 1, im
           if(cnvflg(i) .and. k .le. kmax(i)-1) then
             qeso(i,k) = 0.01 * fpvsx(to(i,k))      ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (po(i,k) + epsm1 * qeso(i,k))
             val1      =             1.e-8
             qeso(i,k) = max(qeso(i,k), val1)
             val2      =           1.e-10
             qo(i,k)   = max(qo(i,k), val2 )
 !           qo(i,k)   = min(qo(i,k),qeso(i,k))
             heo(i,k)   = .5 * g * (zo(i,k) + zo(i,k+1)) + cp * to(i,k) + hvap * qo(i,k)
             heso(i,k) = .5 * g * (zo(i,k) + zo(i,k+1)) + cp * to(i,k) + hvap * qeso(i,k)
           endif
         enddo
       enddo
       do i = 1, im
         if(cnvflg(i)) then
           k = kmax(i)
           heo(i,k) = g * zo(i,k) + cp * to(i,k) + hvap * qo(i,k)
           heso(i,k) = g * zo(i,k) + cp * to(i,k) + hvap * qeso(i,k)
 !         heo(i,k) = min(heo(i,k),heso(i,k))
         endif
       enddo
    end subroutine recalculate_moist_static_energy


end module sascnvn_static
