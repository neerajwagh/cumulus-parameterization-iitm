module sascnvn_recalculate
    use sascnvn_init
    implicit none
contains

    subroutine recalculate_moisture()
 !
 !------- moisture and cloud work functions
 !
       do i = 1, im
         if(cnvflg(i)) then
           xaa0(i) = 0.
           xpwav(i) = 0.
         endif
       enddo
 !
       do i = 1, im
         if(cnvflg(i)) then
           indx = kb(i)
           hcko(i,indx) = heo(i,indx)
           qcko(i,indx) = qo(i,indx)
         endif
       enddo
       do k = 2, km1
         do i = 1, im
           if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.le.ktcon(i)) then
               dz = zi(i,k) - zi(i,k-1)
               tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
               tem1 = 0.5 * xlamud(i) * dz
               factor = 1. + tem - tem1
               hcko(i,k) = ((1.-tem1)*hcko(i,k-1)+tem*0.5*(heo(i,k)+heo(i,k-1)))/factor
             endif
           endif
         enddo
       enddo
       do k = 2, km1
         do i = 1, im
           if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.lt.ktcon(i)) then
               dz = zi(i,k) - zi(i,k-1)
               gamma = el2orc * qeso(i,k) / (to(i,k)**2)
               xdby = hcko(i,k) - heso(i,k)
               xqrch = qeso(i,k) + gamma * xdby / (hvap * (1. + gamma))
               tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
               tem1 = 0.5 * xlamud(i) * dz
               factor = 1. + tem - tem1
               qcko(i,k) = ((1.-tem1)*qcko(i,k-1)+tem*0.5*(qo(i,k)+qo(i,k-1)))/factor
               dq = eta(i,k) * (qcko(i,k) - xqrch)
 !
               if(k.ge.kbcon(i).and.dq.gt.0.) then
                 etah = .5 * (eta(i,k) + eta(i,k-1))
                 if(ncloud.gt.0..and.k.gt.jmin(i)) then
                   qlk = dq / (eta(i,k) + etah * (c0 + c1) * dz)
                 else
                   qlk = dq / (eta(i,k) + etah * c0 * dz)
                 endif
                 if(k.lt.ktcon1(i)) then
                   xaa0(i) = xaa0(i) - dz * g * qlk
                 endif
                 qcko(i,k) = qlk + xqrch
                 xpw = etah * c0 * dz * qlk
                 xpwav(i) = xpwav(i) + xpw
               endif
             endif
             if(k.ge.kbcon(i).and.k.lt.ktcon1(i)) then
               dz1 = zo(i,k+1) - zo(i,k)
               gamma = el2orc * qeso(i,k) / (to(i,k)**2)
               rfact =  1. + delta * cp * gamma * to(i,k) / hvap
               xaa0(i) = xaa0(i)+ dz1 * (g / (cp * to(i,k)))* xdby / (1. + gamma)* rfact
               val=0.
               xaa0(i)=xaa0(i)+dz1 * g * delta * max(val,(qeso(i,k) - qo(i,k)))
             endif
           endif
         enddo
       enddo
    end subroutine recalculate_moisture


 !
 !------- downdraft calculations
 !

    subroutine recalculate_downdraft_moisture_properties()
 !--- downdraft moisture properties
 !
       do i = 1, im
         if(cnvflg(i)) then
           jmn = jmin(i)
           hcdo(i,jmn) = heo(i,jmn)
           qcdo(i,jmn) = qo(i,jmn)
           qrcd(i,jmn) = qo(i,jmn)
           xpwev(i) = 0.
         endif
       enddo
       do k = km1, 1, -1
         do i = 1, im
           if (cnvflg(i) .and. k.lt.jmin(i)) then
               dz = zi(i,k+1) - zi(i,k)
               if(k.ge.kbcon(i)) then
                  tem  = xlamde * dz
                  tem1 = 0.5 * xlamdd * dz
               else
                  tem  = xlamde * dz
                  tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
               endif
               factor = 1. + tem - tem1
               hcdo(i,k) = ((1.-tem1)*hcdo(i,k+1)+tem*0.5*(heo(i,k)+heo(i,k+1)))/factor
           endif
         enddo
       enddo
       do k = km1, 1, -1
         do i = 1, im
           if (cnvflg(i) .and. k .lt. jmin(i)) then
               dq = qeso(i,k)
               dt = to(i,k)
               gamma    = el2orc * dq / dt**2
               dh       = hcdo(i,k) - heso(i,k)
               qrcd(i,k)=dq+(1./hvap)*(gamma/(1.+gamma))*dh
 !             detad    = etad(i,k+1) - etad(i,k)
               dz = zi(i,k+1) - zi(i,k)
               if(k.ge.kbcon(i)) then
                  tem  = xlamde * dz
                  tem1 = 0.5 * xlamdd * dz
               else
                  tem  = xlamde * dz
                  tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
               endif
               factor = 1. + tem - tem1
               qcdo(i,k) = ((1.-tem1)*qrcd(i,k+1)+tem*0.5*(qo(i,k)+qo(i,k+1)))/factor
 !             xpwd     = etad(i,k+1) * qcdo(i,k+1) -
 !    &                   etad(i,k) * qrcd(i,k)
 !             xpwd     = xpwd - detad *
 !    &                 .5 * (qrcd(i,k) + qrcd(i,k+1))
               xpwd     = etad(i,k) * (qcdo(i,k) - qrcd(i,k))
               xpwev(i) = xpwev(i) + xpwd
           endif
         enddo
       enddo
 !
       do i = 1, im
         edtmax = edtmaxl
         if(islimsk(i) == 0) edtmax = edtmaxs
         if(cnvflg(i)) then
           if(xpwev(i).ge.0.) then
             edtx(i) = 0.
           else
             edtx(i) = -edtx(i) * xpwav(i) / xpwev(i)
             edtx(i) = min(edtx(i),edtmax)
           endif
         endif
       enddo

    end subroutine recalculate_downdraft_moisture_properties



    subroutine recalculate_downdraft_cloudwork_functions()
 !
 !
 !--- downdraft cloudwork functions
 !
 !
       do k = km1, 1, -1
         do i = 1, im
           if (cnvflg(i) .and. k.lt.jmin(i)) then
               gamma = el2orc * qeso(i,k) / to(i,k)**2
               dhh=hcdo(i,k)
               dt= to(i,k)
               dg= gamma
               dh= heso(i,k)
               dz=-1.*(zo(i,k+1)-zo(i,k))
               xaa0(i)=xaa0(i)+edtx(i)*dz*(g/(cp*dt))*((dhh-dh)/(1.+dg))*(1.+delta*cp*dg*dt/hvap)
               val=0.
               xaa0(i)=xaa0(i)+edtx(i)*dz*g*delta*max(val,(qeso(i,k)-qo(i,k)))
           endif
         enddo
       enddo
    end subroutine recalculate_downdraft_cloudwork_functions


end module sascnvn_recalculate
