import itertools
import json

import numpy as np
from netCDF4 import Dataset

def computeMSE(T,z):
        
    # CONSTANTS
    # z = millibar, T = degree celcius, g = 9.8 m/s2, Cp unit = J/K.kg, Lv = , q = 
    g = 9.8
    Cp = 1006 
    Lv = 2.5*10e6
    # q has to be taken from NCEP data as any other variable at each pressure level.
    # q = ?
    q = 1

    # Moist Static Energy = Cp*T + g*z + Lv*q
    return Cp*T + g*z + Lv*q


if __name__ == "__main__":

    # output format
    # {
    #     (lon,lat) : [
    #                 {
    #                     day : 
    #                     maxima_level :
    #                     minima_level :
    #                 }
    #         ]
    # }
    output = { }

    # open netCDF file for reading.
    ncfile = Dataset('../data/air.mon.mean.nc','r') 

    # read netcdf variables into numpy arrays
    lats = np.array(ncfile.variables['lat'])[:2]
    lons = np.array(ncfile.variables['lon'])[:2]
    levels = np.array(ncfile.variables['level'])
    times = np.array(ncfile.variables['time'])
    airtemps = np.array(ncfile.variables['air'])

    # In [74]: test
    # Out[74]: [(0, 9), (1, 8), (2, 7), (3, 6)]

    # In [75]: test2
    # Out[75]: [(0, 'a'), (1, 'b'), (2, 'c'), (3, 'd'), (4, 'e'), (5, 'f')]

    # In [76]: list(itertools.product(test,test2))
    # Out[76]: 
    # [((0, 9), (0, 'a')),
    #  ((0, 9), (1, 'b')),
    #  ((0, 9), (2, 'c')),
    #  ((0, 9), (3, 'd')),
    #  ((0, 9), (4, 'e')),
    #  ((0, 9), (5, 'f')),

    # find pairs of (longitude,latitude) while preserving their indices
    lat_lon_iter = list(itertools.product(enumerate(lons), enumerate(lats)))
    for lat_lon_pair in lat_lon_iter:

        lon_index = lat_lon_pair[0][0]
        lon = lat_lon_pair[0][1]
        
        lat_index = lat_lon_pair[1][0]
        lat = lat_lon_pair[1][1]
        
        print "---------------for lon lat pair", lon, lat

        # prep for output
        key = "(" + str(lon) + "," + str(lat) + ")"
        output[key] = [ ]

        for time_index, time in enumerate(times):
  
            # list containing all mse values along vertical axis of (lat,lon)
            mse_values = []
  
            # loop through all levels for the grid point (lat,lon) for one day
            level_iter = list(enumerate(levels))
            for level_index, level in level_iter:
            
                # get air temperature for (lat,lon,level) at a particular time
                temperature = airtemps[time_index][level_index][lat_index][lon_index]
                # COMPARE PROPERLY!
                if temperature == -9.96921e+36:
                    print "got missing temp!"

                # compute MSE (lat,lon,level)
                mse = computeMSE(temperature, level)
                # print "got mse : ", mse

                # store temporarily
                mse_values.append(mse)    

            # find maxima
            max_index, max_val = max(enumerate(mse_values), key=lambda x: x[1])
            
            # find minima
            min_index, min_val = min(enumerate(mse_values), key=lambda x: x[1])

            # add to output
            entry = { }
            entry["day"] = str(time)
            entry["maxima_level"] = str(levels[max_index])
            entry["minima_level"] = str(levels[min_index])
            output[key].append(entry)
            print "new entry", entry

    # export output to json file
    with open('output.json', 'w') as fp:
        json.dump(output, fp)

    # cleanup
    ncfile.close()
