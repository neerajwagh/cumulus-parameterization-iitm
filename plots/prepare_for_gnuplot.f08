module gnuplot
    implicit none
    contains
    subroutine plot2d(x, y)
        
        real integer(in), dimentions(:) :: x, y
        integer :: size_x, size_y, i
        
        size_x = size(x, dim=1)
        size_y = size(y, dim=1)

        if (size_x /= size_y) then
            print *, "Array size mismatch!"
        else
            open(unit=1, file='data.dat', action="read")
            do i = 1, size_x
                write(1, *) x(i), "  ", y(i)
            end do
        end if

    end subroutine plot2d
end module gnuplot