module sascnvn_feedback
    use sascnvn_init
    implicit none
contains

subroutine feedback()
 !--- feedback: simply the changes from the cloud with unit mass flux
 !---           multiplied by  the mass flux necessary to keep the
 !---           equilibrium with the larger-scale.
 
       do i = 1, im
         delhbar(i) = 0.
         delqbar(i) = 0.
         deltbar(i) = 0.
         delubar(i) = 0.
         delvbar(i) = 0.
         qcond(i) = 0.
       enddo
       do k = 1, km
         do i = 1, im
           if (cnvflg(i) .and. k .le. kmax(i)) then
             if(k.le.ktcon(i)) then
               dellat = (dellah(i,k) - hvap * dellaq(i,k)) / cp
               t1(i,k) = t1(i,k) + dellat * xmb(i) * dt2
               q1(i,k) = q1(i,k) + dellaq(i,k) * xmb(i) * dt2
 !             tem = 1./rcs(i)
 !             u1(i,k) = u1(i,k) + dellau(i,k) * xmb(i) * dt2 * tem
 !             v1(i,k) = v1(i,k) + dellav(i,k) * xmb(i) * dt2 * tem
               u1(i,k) = u1(i,k) + dellau(i,k) * xmb(i) * dt2
               v1(i,k) = v1(i,k) + dellav(i,k) * xmb(i) * dt2
               dp = 1000. * del(i,k)
               delhbar(i) = delhbar(i) + dellah(i,k)*xmb(i)*dp/g
               delqbar(i) = delqbar(i) + dellaq(i,k)*xmb(i)*dp/g
               deltbar(i) = deltbar(i) + dellat*xmb(i)*dp/g
               delubar(i) = delubar(i) + dellau(i,k)*xmb(i)*dp/g
               delvbar(i) = delvbar(i) + dellav(i,k)*xmb(i)*dp/g
             endif
           endif
         enddo
       enddo
       do k = 1, km
         do i = 1, im
           if (cnvflg(i) .and. k .le. kmax(i)) then
             if(k.le.ktcon(i)) then
               qeso(i,k) = 0.01 * fpvsx(t1(i,k))      ! fpvsx is in pa
               qeso(i,k) = eps * qeso(i,k)/(pfld(i,k) + epsm1*qeso(i,k))
               val     =             1.e-8
               qeso(i,k) = max(qeso(i,k), val )
             endif
           endif
         enddo
       enddo
 
       do i = 1, im
         rntot(i) = 0.
         delqev(i) = 0.
         delq2(i) = 0.
         flg(i) = cnvflg(i)
       enddo
       do k = km, 1, -1
         do i = 1, im
           if (cnvflg(i) .and. k .le. kmax(i)) then
             if(k.lt.ktcon(i)) then
               aup = 1.
               if(k.le.kb(i)) aup = 0.
               adw = 1.
               if(k.ge.jmin(i)) adw = 0.
               rain =  aup * pwo(i,k) + adw * edto(i) * pwdo(i,k)
               rntot(i) = rntot(i) + rain * xmb(i) * .001 * dt2
             endif
           endif
         enddo
       enddo
       do k = km, 1, -1
         do i = 1, im
           if (k .le. kmax(i)) then
             deltv(i) = 0.
             delq(i) = 0.
             qevap(i) = 0.
             if(cnvflg(i).and.k.lt.ktcon(i)) then
               aup = 1.
               if(k.le.kb(i)) aup = 0.
               adw = 1.
               if(k.ge.jmin(i)) adw = 0.
               rain =  aup * pwo(i,k) + adw * edto(i) * pwdo(i,k)
               rn(i) = rn(i) + rain * xmb(i) * .001 * dt2
             endif
             if(flg(i).and.k.lt.ktcon(i)) then
               evef = edt(i) * evfact
               if(islimsk(i) == 1) evef=edt(i) * evfactl
 !             if(islimsk(i) == 1) evef=.07
 !             if(islimsk(i) == 1) evef = 0.
               qcond(i) = evef * (q1(i,k) - qeso(i,k)) / (1. + el2orc * qeso(i,k) / t1(i,k)**2)
               dp = 1000. * del(i,k)
               if(rn(i).gt.0..and.qcond(i).lt.0.) then
                 qevap(i) = -qcond(i) * (1.-exp(-.32*sqrt(dt2*rn(i))))
                 qevap(i) = min(qevap(i), rn(i)*1000.*g/dp)
                 delq2(i) = delqev(i) + .001 * qevap(i) * dp / g
               endif
               if(rn(i).gt.0..and.qcond(i).lt.0..and.delq2(i).gt.rntot(i)) then
                 qevap(i) = 1000.* g * (rntot(i) - delqev(i)) / dp
                 flg(i) = .false.
               endif
               if(rn(i).gt.0..and.qevap(i).gt.0.) then
                 q1(i,k) = q1(i,k) + qevap(i)
                 t1(i,k) = t1(i,k) - elocp * qevap(i)
                 rn(i) = rn(i) - .001 * qevap(i) * dp / g
                 deltv(i) = - elocp*qevap(i)/dt2
                 delq(i) =  + qevap(i)/dt2
                 delqev(i) = delqev(i) + .001*dp*qevap(i)/g
               endif
               dellaq(i,k) = dellaq(i,k) + delq(i) / xmb(i)
               delqbar(i) = delqbar(i) + delq(i)*dp/g
               deltbar(i) = deltbar(i) + deltv(i)*dp/g
             endif
           endif
         enddo
       enddo
 !     do i = 1, im
 !     if(me.eq.31.and.cnvflg(i)) then
 !     if(cnvflg(i)) then
 !       print *, ' deep delhbar, delqbar, deltbar = ',
 !    &             delhbar(i),hvap*delqbar(i),cp*deltbar(i)
 !       print *, ' deep delubar, delvbar = ',delubar(i),delvbar(i)
 !       print *, ' precip =', hvap*rn(i)*1000./dt2
 !       print*,'pdif= ',pfld(i,kbcon(i))-pfld(i,ktcon(i))
 !     endif
 !     enddo
 
 !  precipitation rate converted to actual precip
 !  in unit of m instead of kg
 !
       do i = 1, im
         if(cnvflg(i)) then
 !
 !  in the event of upper level rain evaporation and lower level downdraft
 !    moistening, rn can become negative, in this case, we back out of the
 !    heating and the moistening
 !
 
           if(rn(i).lt.0..and..not.flg(i)) rn(i) = 0.
           if(rn(i).le.0.) then
             rn(i) = 0.
           else
             ktop(i) = ktcon(i)
             kbot(i) = kbcon(i)
             kcnv(i) = 1
             cldwrk(i) = aa1(i)
            ! print *, "cldwrk :", cldwrk             
            ! print *, "rn :", rn
           endif
         endif
       enddo
end subroutine feedback



subroutine convective_cloud_water()
 !
 !  convective cloud water
 !
      ! print *, "dt2 :", dt2
       do k = 1, km
         do i = 1, im
           if (cnvflg(i) .and. rn(i).gt.0.) then
             if (k.ge.kbcon(i).and.k.lt.ktcon(i)) then
               cnvw(i,k) = cnvwt(i,k) * xmb(i) * dt2
              !  print *, "cnvwt :", cnvwt
              !  print *, "xmb :", xmb
             endif
           endif
         enddo
       enddo
end subroutine convective_cloud_water



subroutine convective_cloud_cover()
 !
 !  convective cloud cover
 !
       do k = 1, km
         do i = 1, im
           if (cnvflg(i) .and. rn(i).gt.0.) then
             if (k.ge.kbcon(i).and.k.lt.ktcon(i)) then
               cnvc(i,k) = 0.04 * log(1. + 675. * eta(i,k) * xmb(i))
               cnvc(i,k) = min(cnvc(i,k), 0.6)
               cnvc(i,k) = max(cnvc(i,k), 0.0)
             endif
           endif
         enddo
       enddo
end subroutine convective_cloud_cover

subroutine cloud_water()
 !
 !  cloud water
 !
       if (ncloud.gt.0) then
!
       do k = 1, km
         do i = 1, im
           if (cnvflg(i) .and. rn(i).gt.0.) then
             if (k.gt.kb(i).and.k.le.ktcon(i)) then
               tem  = dellal(i,k) * xmb(i) * dt2
               tem1 = max(0.0, min(1.0, (tcr-t1(i,k))*tcrf))
              !  if (ql(i,k,2) .gt. -999.0) then
              !    ql(i,k,1) = ql(i,k,1) + tem * tem1            ! ice
              !    ql(i,k,2) = ql(i,k,2) + tem *(1.0-tem1)       ! water
              !  else
              !    ql(i,k,1) = ql(i,k,1) + tem
              !  endif
             endif
           endif
         enddo
       enddo
 !
       endif
end subroutine cloud_water

subroutine final_feedback()
       do k = 1, km
         do i = 1, im
           if(cnvflg(i).and.rn(i).le.0.) then
             if (k .le. kmax(i)) then
               t1(i,k) = to(i,k)
               q1(i,k) = qo(i,k)
               u1(i,k) = uo(i,k)
               v1(i,k) = vo(i,k)
             endif
           endif
         enddo
       enddo
 !
 ! hchuang code change
 !
       do k = 1, km
         do i = 1, im
           if(cnvflg(i).and.rn(i).gt.0.) then
             if(k.ge.kb(i) .and. k.lt.ktop(i)) then
               ud_mf(i,k) = eta(i,k) * xmb(i) * dt2
             endif
           endif
         enddo
       enddo
       do i = 1, im
         if(cnvflg(i).and.rn(i).gt.0.) then
            k = ktop(i)-1
            dt_mf(i,k) = ud_mf(i,k)
         endif
       enddo
       do k = 1, km
         do i = 1, im
           if(cnvflg(i).and.rn(i).gt.0.) then
             if(k.ge.1 .and. k.le.jmin(i)) then
               dd_mf(i,k) = edto(i) * etad(i,k) * xmb(i) * dt2
             endif
           endif
         enddo
       enddo
 !!
 end subroutine final_feedback

end module sascnvn_feedback
 
