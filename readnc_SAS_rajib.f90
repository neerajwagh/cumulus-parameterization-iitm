
!!following modules are to be loaded before execution
!!netcdf libraries used is netcdf-4.4.1

!source /gpfs1/opt/intel/parallel_studio_xe_2016/compilers_and_libraries/linux/bin/compilervars.sh intel64
!source /gpfs1/opt/intel/parallel_studio_xe_2016/impi/5.1.1.109/intel64/bin/mpivars.sh
!module load netcdf/4.4.1

! ifort -traceback -I/gpfs1/home/Libs/INTEL/NETCDF4/netcdf-4.4.1/include -L/gpfs1/home/Libs/INTEL/NETCDF4/netcdf-4.4.1/lib -lnetcdff -lnetcdf  machine.f90 physcons.f90 fpvs.f90 readnc_SAS_rajib.f90

!Note 11Aug2017:fpvs not working fpvsx is used here

program main
use machine, only:kind_phys
use netcdf

IMPLICIT NONE
! integer, parameter :: kind_phys = selected_real_kind(13,60) 
    
logical :: stat,system,EXIST,ex, INQUIRE,MASK
integer::i,j,k,ii,jj,ic,icc,ixc,nt,year
integer,parameter::nt1=1,nt2=124,nday=nt2-nt1+1
integer,parameter::yr_st=2017,yr_en=2017,nyr=yr_en-yr_st+1
integer,parameter::nlev=20,nlon=360,nlat=181,ntime=124,ndims=4,dim3d=3,dim2d=2
integer :: ncid, varid,ncstat,count(ndims),start(ndims),start3d(dim3d),count3d(dim3d),start2d(dim2d),count2d(dim2d)

real::u(nlon,nlat,nlev),v(nlon,nlat,nlev),q(nlon,nlat,nlev),w(nlon,nlat,nlev),temp(nlon,nlat,nlev)
real::phi(nlon,nlat,nlev),msl(nlon,nlat),lsmask(nlon,nlat),dummy(nlon,nlat,nlev,1),dummy3d(nlon,nlat,1)
real::levels(nlev),sf,ao
character::dir_in*300,fu*300,fv*300,fw*300,ft*300,fq*300,fps*300,fph*300,cyr*4,fmask*300
data levels /1000.0, 950.0, 900.0, 850.0,800.0, 750,700.0, 650.0,600.0, 550.0, 500.0, 450.0,400.0, 350.0, 300.0, 250.0, 200.0, 150.0 ,100.0 , 50.0/

!SAS Subroutine required variables
integer,parameter::im=nlon*nlat,ix=im,km=nlev,ncloud=1,jcap=10
real(kind=kind_phys),parameter::delt=600,delp(1:ix,1:km)=100
real(kind=kind_phys)::xprslp(km),prslp(ix,km),psp(ix),phil(ix,km),ql(ix,km,2),q1(ix,km),t1(ix,km),u1(ix,km),v1(ix,km),w1(ix,km)
real(kind=kind_phys)::cldwrk(im),rn(im),kbot(im),ktop(im),kcnv(im),islimsk(im)
real(kind=kind_phys)::dot(ix,km),ud_mf(ix,km),dd_mf(ix,km),dt_mf(ix,km),cnvw(ix,km),cnvc(ix,km)

!SAS output variables for writing in bin/ctl files
real::rn_out(nlon,nlat),cnvw_out(nlon,nlat,nlev),cldwrk_out(nlon,nlat),cnvc_out(nlon,nlat,nlev)

 DO year =yr_st,yr_en

!!!!!!!!!!!!OUTPUT FILES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
write(cyr,'(i4.4)')year

open(41,file='out_data/aug_extrm_2017/cn_rn_aug_'//cyr//'.dat',form='unformatted',status='unknown',access='direct',recl=nlon*nlat  )
open(42,file='out_data/aug_extrm_2017/cn_cnvw_aug_'//cyr//'.dat',form='unformatted',status='unknown',access='direct',recl=nlon*nlat*nlev  )
open(43,file='out_data/aug_extrm_2017/cn_cldwrk_aug_'//cyr//'.dat',form='unformatted',status='unknown',access='direct',recl=nlon*nlat  )
open(44,file='out_data/aug_extrm_2017/cn_cnvc_aug_'//cyr//'.dat',form='unformatted',status='unknown',access='direct',recl=nlon*nlat*nlev  )


!!!!!!!!!!!!START MAIN CODE: INPUT READ DATA!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

write(cyr,'(i4.4)')year
dir_in='/iitm2/erpas/rajib/SAS_cn/in_data/yr'//cyr

!!!!!!!!!!!!!!!!!!!!4D data!!!!!!!!!!!!!!!!!!!!!!
fu=TRIM(dir_in)//'/uwind_aug_'//cyr//'.nc'
fv=TRIM(dir_in)//'/vwind_aug_'//cyr//'.nc'
fw=TRIM(dir_in)//'/omega_aug_'//cyr//'.nc'
fph= TRIM(dir_in)//'/gphgt_aug_'//cyr//'.nc'
ft=TRIM(dir_in)//'/atemp_aug_'//cyr//'.nc'
fq=TRIM(dir_in)//'/sphum_aug_'//cyr//'.nc'
!!!!!!!!!!!!!!!!!!!3D data!!!!!!!!!!!!!!!!!!!!!!!!
fps=TRIM(dir_in)//'/mslp_aug_'//cyr//'.nc'
dir_in='/iitm2/erpas/imdoper/OBSERVATION_data/ECMWF/ERA_Interim'
fmask=TRIM(dir_in)//'/mask/landsea_mask.nc'
print*,fu

open(17,file=TRIM(fps),form='unformatted',status='old',access='direct',recl=nlon*nlat  )
open(18,file=TRIM(fmask),form='unformatted',status='old',access='direct',recl=nlon*nlat  )
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!Renaming the variables as per as subroutine except those defined as parameter!!
do i=1,km
 xprslp(i)=levels(i)*100  !  convert  from hPa to Pa
end do

!!!!!!!!!!!!!!!!!!!!!!!!!READ 2D (Nlon,Nlat) data!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    start2d = [1, 1]
    count2d = (/ nlon, nlat /)
    call check( nf90_open(fmask,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "lsm", varid ) ) 
    call check( nf90_get_var(ncid, varid, lsmask,start=start2d,count=count2d) )
    sf=1.52594875864068e-05  !scale factor
    ao=0.499992370256207     !add offset
    lsmask=lsmask*sf+ao 
    call check( nf90_close(ncid) )
    
ixc=0
do nt = nt1,nt2
    ixc=ixc+1
    
!!!!!!!!!!!!!!!!!!!!!!!!!READ 3D (Nlon,Nlat,time) data!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    start3d = [1, 1, nt]
    count3d = (/ nlon, nlat,1 /)
    call check( nf90_open(fps,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "msl", varid ) ) 
    call check( nf90_get_var(ncid, varid, dummy3d,start=start3d,count=count3d) )
    sf=1.0   !scale factor
    ao=0.0   !add offset
    msl(1:nlon,1:nlat)=dummy3d(1:nlon,1:nlat,1)*sf+ao
    call check( nf90_close(ncid) )

!!!!!!!!!!!!!!!!!!!!!!!!!READ 4D (Nlon,Nlat,Nlev, time) data!!!!!!!!!!!!!!!!!!!!!!!!!!
    start = [1, 1, 1, nt]
    count = (/ nlon, nlat, nlev,1 /)
    call check( nf90_open(fu,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "u", varid ) ) 
    call check( nf90_get_var(ncid, varid, dummy,start=start,count=count) )
    u(1:nlon,1:nlat,1:nlev)=dummy(1:nlon,1:nlat,1:nlev,1)
    call check( nf90_close(ncid) )
    
    call check( nf90_open(fv,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "v", varid ) ) 
    call check( nf90_get_var(ncid, varid, dummy,start=start,count=count) )
    v(1:nlon,1:nlat,1:nlev)=dummy(1:nlon,1:nlat,1:nlev,1)
    call check( nf90_close(ncid) )
    
    call check( nf90_open(fw,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "w", varid ) ) 
    call check( nf90_get_var(ncid, varid, dummy,start=start,count=count) )
    w(1:nlon,1:nlat,1:nlev)=dummy(1:nlon,1:nlat,1:nlev,1)
    call check( nf90_close(ncid) )
    
    call check( nf90_open(fph,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "z", varid ) ) 
    call check( nf90_get_var(ncid, varid, dummy,start=start,count=count) )
    phi(1:nlon,1:nlat,1:nlev)=dummy(1:nlon,1:nlat,1:nlev,1)
    call check( nf90_close(ncid) )
    
    call check( nf90_open(ft,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "t", varid ) ) 
    call check( nf90_get_var(ncid, varid, dummy,start=start,count=count) )
    temp(1:nlon,1:nlat,1:nlev)=dummy(1:nlon,1:nlat,1:nlev,1)
    call check( nf90_close(ncid) )
    
    call check( nf90_open(fq,NF90_NOWRITE, ncid ) )
    call check( nf90_inq_varid(ncid, "q", varid ) ) 
    call check( nf90_get_var(ncid, varid, dummy,start=start,count=count) )
    q(1:nlon,1:nlat,1:nlev)=dummy(1:nlon,1:nlat,1:nlev,1)
    call check( nf90_close(ncid) )
    
    
!!!!!!!!!!!!!!!!!!!!!!END DATA READ!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  

!!!Check Input data is yrev and/or zrev
!!!!!!!!!!!!!!!!!!!!!!!!Checking Zrev is is very important!!!!!!!!!!!!!!!!!!!!!!!!

ic=0
do ii=1,nlon
 do jj=1,nlat
 ic=ic+1
     do k=1,nlev
            u1(ic,k)=u(ii,jj,nlev-k+1)
                v1(ic,k)=v(ii,jj,nlev-k+1) 
                        w1(ic,k)=w(ii,jj,nlev-k+1)
                        phil(ic,k)=phi(ii,jj,nlev-k+1)  
                        t1(ic,k)=temp(ii,jj,nlev-k+1)
                     q1(ic,k)=q(ii,jj,nlev-k+1)
                 prslp(ic,k)=xprslp(k)
        end do
     psp(ic)=msl(ii,jj)        !input is in Pa from ERA INTERIM always check for all data
     islimsk(ic)=lsmask(ii,jj)
 end do
end do
dot=w1

call sascnvn(im,ix,km,jcap,delt,delp,prslp,psp,phil,ql,       &
                     q1,t1,u1,v1,cldwrk,rn,kbot,ktop,kcnv,islimsk,      &
                     dot,ncloud,ud_mf,dd_mf,dt_mf,cnvw,cnvc)


!write(*,*)rn*86400
ic=0
do ii=1,nlon
 do jj=1,nlat
    ic=ic+1
     rn_out(ii,jj)=rn(ic)*86400.0
     cldwrk_out(ii,jj)=cldwrk(ic)
     do k=1,nlev
        cnvw_out(ii,jj,k)=cnvw(ic,k)
        cnvc_out(ii,jj,k)=cnvc(ic,k)
     end do
 end do
end do
write(41,rec=ixc)rn_out 
write(42,rec=ixc)cnvw_out 
write(43,rec=ixc)cldwrk_out 
write(44,rec=ixc)cnvc_out 


write(*,*)"FINISH TIME-STEP",nt
end do!time
 END DO!year
end program
!:======================================================================
SUBROUTINE check(istatus)
USE netcdf
IMPLICIT NONE
INTEGER, INTENT (IN) :: istatus
IF (istatus /= nf90_noerr) THEN
write(*,*) TRIM(ADJUSTL(nf90_strerror(istatus)))
END IF
END SUBROUTINE check 


 
             subroutine sascnvn(im,ix,km,jcap,delt,delp,prslp,psp,phil,ql,	 &
            &     q1,t1,u1,v1,cldwrk,rn,kbot,ktop,kcnv,islimsk,		 &
            &     dot,ncloud,ud_mf,dd_mf,dt_mf,cnvw,cnvc)
    !
    !   use machine , only : kind_phys
             use funcphys , only : fpvsx
             use physcons, grav => con_g, cp => con_cp, hvap => con_hvap	 &
            &,	     rv => con_rv, fv => con_fvirt, t0c => con_t0c	 &
            &,	     cvap => con_cvap, cliq => con_cliq 		 &
            &,	     eps => con_eps, epsm1 => con_epsm1
             implicit none
 !
!       integer, parameter :: kind_phys = selected_real_kind(13,60)		
             integer  	  im, ix,  km, jcap, ncloud,			 &
            & 		  kbot(im), ktop(im), kcnv(im)  
 !    &,		  me
             real(kind=kind_phys) delt
             real(kind=kind_phys) psp(im),	delp(ix,km), prslp(ix,km)
             real(kind=kind_phys) ps(im),	del(ix,km),  prsl(ix,km),&
            & 		    ql(ix,km,2),q1(ix,km),   t1(ix,km),  &
            & 		    u1(ix,km),  v1(ix,km),		 & !rcs(im),
            & 		    cldwrk(im), rn(im), 		 &
            & 		    dot(ix,km), phil(ix,km),		 &
            & 		    cnvw(ix,km), cnvc(ix,km),		 &
            & 		    ud_mf(im,km),dd_mf(im,km),dt_mf(im,km) ! hchuang code change mass flux output
 !
             integer  	    i, indx, jmn, k, kk, km1
             integer, dimension(im), intent(in) :: islimsk
 !     integer  	    latd,lond
 !
             real(kind=kind_phys) clam, cxlamu, xlamde, xlamdd
 !
 !     real(kind=kind_phys) detad
             real(kind=kind_phys) adw,     aup,     aafac,                 &
            & 		    beta,    betal,   betas,                 &
            & 		    c0,      dellat,  delta,                 &                 
            & 		    desdt,   dg,                             &                 
            & 		    dh,      dhh,     dp,                              &
            & 		    dq,      dqsdp,   dqsdt,   dt,                     &
            & 		    dt2,     dtmax,   dtmin,   dv1h,                   &                                      
            & 		    dv1q,    dv2h,    dv2q,    dv1u,                   &
            & 		    dv1v,    dv2u,    dv2v,    dv3q,                   &
            & 		    dv3h,    dv3u,    dv3v,                            &
            & 		    dz,      dz1,     e1,      edtmax,                 &
            & 		    edtmaxl, edtmaxs, el2orc,  elocp,                  &
            & 		    es,      etah,    cthk,    dthk,                   &
            & 		    evef,    evfact,  evfactl, fact1,                  &
            & 		    fact2,   factor,  fjcap,   fkm,                    &
            & 		    g,       gamma,   pprime,                          &
            & 		    qlk,     qrch,    qs,      c1,                     &
            & 		    rain,    rfact,   shear,   tem1,                   &
            & 		    val,     val1,                                     &
            & 		    val2,    w1,      w1l,     w1s,                    &
            & 		    w2,      w2l,     w2s,     w3,                     &
            & 		    w3l,     w3s,     w4,      w4l,                    &
            & 		    w4s,     xdby,    xpw,     xpwd,                   &
            & 		    xqrch,   mbdt,    tem,                             &
            & 		    ptem,    ptem1,   pgcon                            
 !
             integer  	    kb(im), kbcon(im), kbcon1(im),                     &
            & 		    ktcon(im), ktcon1(im),                             &
            & 		    jmin(im), lmin(im), kbmax(im),                     &
            & 		    kbm(im), kmax(im)                                  
 !
             real(kind=kind_phys) aa1(im),	 acrt(im),   acrtfct(im),              &
            & 		    delhbar(im), delq(im),   delq2(im),                &
            & 		    delqbar(im), delqev(im), deltbar(im),              &
            & 		    deltv(im),   dtconv(im), edt(im),                  &
            & 		    edto(im),	 edtx(im),   fld(im),                  &
            & 		    hcdo(im,km), hmax(im),   hmin(im),                 &
            & 		    ucdo(im,km), vcdo(im,km),aa2(im),                  &
            & 		    pbcdif(im),  pdot(im),   po(im,km),                &
            & 		    pwavo(im),   pwevo(im),  xlamud(im),               &
            & 		    qcdo(im,km), qcond(im),  qevap(im),                &
            & 		    rntot(im),   vshear(im), xaa0(im),                 &
            & 		    xk(im),	 xlamd(im),                            &
            & 		    xmb(im),	 xmbmax(im), xpwav(im),                &
            & 		    xpwev(im),   delubar(im),delvbar(im)                 
!j
             real(kind=kind_phys) cincr, cincrmax, cincrmin
!j
!  physical parameters
             parameter(g=grav)
             parameter(elocp=hvap/cp,                                                &
            & 	 el2orc=hvap*hvap/(rv*cp))
             parameter(c0=.002,c1=.002,delta=fv)
             parameter(fact1=(cvap-cliq)/rv,fact2=hvap/rv-fact1*t0c)
             parameter(cthk=150.,cincrmax=180.,cincrmin=120.,dthk=25.)
!  local variables and arrays
             real(kind=kind_phys) pfld(im,km),    to(im,km),     qo(im,km),            &
            & 		    uo(im,km),      vo(im,km),     qeso(im,km)           
!  cloud water
 !     real(kind=kind_phys) tvo(im,km)
             real(kind=kind_phys) qlko_ktcon(im), dellal(im,km), tvo(im,km),           &
            & 		    dbyo(im,km),    zo(im,km),     xlamue(im,km),        &
            & 		    fent1(im,km),   fent2(im,km),  frh(im,km),           &
            & 		    heo(im,km),     heso(im,km),                         &
            & 		    qrcd(im,km),    dellah(im,km), dellaq(im,km),        &
            & 		    dellau(im,km),  dellav(im,km), hcko(im,km),          &
            & 		    ucko(im,km),    vcko(im,km),   qcko(im,km),          &
            & 		    eta(im,km),     etad(im,km),   zi(im,km),            &
            & 		    qrcko(im,km),   qrcdo(im,km),                        &
            & 		    pwo(im,km),     pwdo(im,km),                         &
            & 		    tx1(im),	    sumx(im),	   cnvwt(im,km)           
 !    &,		    rhbar(im)
 !
             logical totflg, cnvflg(im), flg(im)
 !
             real(kind=kind_phys) pcrit(15), acritt(15), acrit(15)
 !     save pcrit, acritt
             data pcrit/850.,800.,750.,700.,650.,600.,550.,500.,450.,400.,             &
            & 	  350.,300.,250.,200.,150./
             data acritt/.0633,.0445,.0553,.0664,.075,.1082,.1521,.2216,               &
            & 	  .3151,.3677,.41,.5255,.7663,1.1686,1.6851/
!  gdas derived acrit
!     data acritt/.203,.515,.521,.566,.625,.665,.659,.688,
!    & 	   .743,.813,.886,.947,1.138,1.377,1.896/
             real(kind=kind_phys) tf, tcr, tcrf
             parameter(tf=233.16, tcr=263.16, tcrf=1.0/(tcr-tf))
 !
!-----------------------------------------------------------------------
 !************************************************************************
 !     convert input pa terms to cb terms  -- moorthi
             ps   = psp   * 0.001
             prsl = prslp * 0.001
             del  = delp  * 0.001
!       write(*,*)ps(100),prsl(100,1),prsl(100,1)/ps(100)
!       stop
 !************************************************************************
 !
 !
             km1 = km - 1
!
!  initialize arrays
!
             do i=1,im
     cnvflg(i) = .true.
     rn(i)=0.
     kbot(i)=km+1
     ktop(i)=0
     kbcon(i)=km
     ktcon(i)=1
     dtconv(i) = 3600.
     cldwrk(i) = 0.
     pdot(i) = 0.
     pbcdif(i)= 0.
     lmin(i) = 1
     jmin(i) = 1
     qlko_ktcon(i) = 0.
     edt(i)  = 0.
     edto(i) = 0.
     edtx(i) = 0.
     acrt(i) = 0.
     acrtfct(i) = 1.
     aa1(i)  = 0.
     aa2(i)  = 0.
     xaa0(i) = 0.
     pwavo(i)= 0.
     pwevo(i)= 0.
     xpwav(i)= 0.
     xpwev(i)= 0.
     vshear(i) = 0.
             enddo
             do k = 1, km
     do i = 1, im
         cnvw(i,k) = 0.
         cnvc(i,k) = 0.
     enddo
             enddo
             
             
 ! hchuang code change
             do k = 1, km
     do i = 1, im
         ud_mf(i,k) = 0.
         dd_mf(i,k) = 0.
         dt_mf(i,k) = 0.
     enddo
             enddo
!
             do k = 1, 15
     acrit(k) = acritt(k) * (975. - pcrit(k))
             enddo
             dt2 = delt
             val   =         1200.
             dtmin = max(dt2, val )
             val   =         3600.
             dtmax = max(dt2, val )
!  model tunable parameters are all here
             mbdt    = 10.
             edtmaxl = .3
             edtmaxs = .3
             clam    = .1
             aafac   = .1
 !     betal   = .15
 !     betas   = .15
             betal   = .05
             betas   = .05
!     evef    = 0.07
             evfact  = 0.3
             evfactl = 0.3
 !
             cxlamu  = 1.0e-4
             xlamde  = 1.0e-4
             xlamdd  = 1.0e-4
 !
 !     pgcon   = 0.7	 ! gregory et al. (1997, qjrms)
             pgcon   = 0.55	 ! zhang & wu (2003,jas)
             fjcap   = (float(jcap) / 126.) ** 2
             val     =	   1.
             fjcap   = max(fjcap,val)
             fkm     = (float(km) / 28.) ** 2
             fkm     = max(fkm,val)
             w1l     = -8.e-3
             w2l     = -4.e-2
             w3l     = -5.e-3
             w4l     = -5.e-4
             w1s     = -2.e-4
             w2s     = -2.e-3
             w3s     = -1.e-3
             w4s     = -2.e-5
!
!  define top layer for search of the downdraft originating layer
!  and the maximum thetae for updraft
!
             do i=1,im
     kbmax(i) = km
     kbm(i)   = km
     kmax(i)  = km
     tx1(i)   = 1.0 / ps(i)
             enddo
     !
             do k = 1, km
     do i=1,im
         if (prsl(i,k)*tx1(i) .gt. 0.04) kmax(i)  = k + 1
         if (prsl(i,k)*tx1(i) .gt. 0.45) kbmax(i) = k + 1
         if (prsl(i,k)*tx1(i) .gt. 0.70) kbm(i)   = k + 1
     enddo
             enddo
             
             do i=1,im
     kmax(i)  = min(km,kmax(i))                  !kmax = cloud top
     kbmax(i) = min(kbmax(i),kmax(i))            !kbmax = LFC
     kbm(i)   = min(kbm(i),kmax(i))              !kbm= parcel starting point
!	 write(*,*)kmax(i),kbmax(i),kbm(i)
             enddo
 !
!  hydrostatic height assume zero terr and initially assume
!c    updraft entrainment rate as an inverse function of height
!
             do k = 1, km
     do i=1,im
         zo(i,k) = phil(i,k) / g
     enddo
             enddo
             
             do k = 1, km1
     do i=1,im
         zi(i,k) = 0.5*(zo(i,k)+zo(i,k+1))
         xlamue(i,k) = clam / zi(i,k)
     enddo
             enddo
 !
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !   convert surface pressure to mb from cb
 !
             do k = 1, km
     do i = 1, im
         if (k .le. kmax(i)) then
             pfld(i,k) = prsl(i,k) * 10.0
             eta(i,k)  = 1.
             fent1(i,k)= 1.
             fent2(i,k)= 1.
             frh(i,k)  = 0.
             hcko(i,k) = 0.
             qcko(i,k) = 0.
             qrcko(i,k)= 0.
             ucko(i,k) = 0.
             vcko(i,k) = 0.
             etad(i,k) = 1.
             hcdo(i,k) = 0.
             qcdo(i,k) = 0.
             ucdo(i,k) = 0.
             vcdo(i,k) = 0.
             qrcd(i,k) = 0.
             qrcdo(i,k)= 0.
             dbyo(i,k) = 0.
             pwo(i,k)  = 0.
             pwdo(i,k) = 0.
             dellal(i,k) = 0.
             to(i,k)   = t1(i,k)
             qo(i,k)   = q1(i,k)
             uo(i,k)   = u1(i,k)
             vo(i,k)   = v1(i,k)
 !	     uo(i,k)   = u1(i,k) * rcs(i)
 !	     vo(i,k)   = v1(i,k) * rcs(i)
             cnvwt(i,k)= 0.
         endif
     enddo
             enddo
    
 !
 !  column variables
 !  p is pressure of the layer(mb)
 !  t is temperature at t-dt(k)..tn
 !  q is mixing ratio at t-dt(kg/kg)..qn
 !  to is temperature at t+dt(k)... this is after advection and turbulan
 !  qo is mixing ratio at t+dt(kg/kg)..q1
 !
             do k = 1, km
     do i=1,im
         if (k .le. kmax(i)) then
             qeso(i,k) = 0.01 * fpvsx(to(i,k))	   ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (pfld(i,k) + epsm1*qeso(i,k))
             val1      =	     1.e-8
             qeso(i,k) = max(qeso(i,k), val1)
             val2      =	   1.e-10
             qo(i,k)   = max(qo(i,k), val2 )
 !	     qo(i,k)   = min(qo(i,k),qeso(i,k))
 !	     tvo(i,k)  = to(i,k) + delta * to(i,k) * qo(i,k)
         endif
     enddo
             enddo
 !
 !  compute moist static energy
 !
             do k = 1, km
     do i=1,im
         if (k .le. kmax(i)) then
 !	     tem       = g * zo(i,k) + cp * to(i,k)
             tem       = phil(i,k) + cp * to(i,k)
             heo(i,k)  = tem  + hvap * qo(i,k)
             heso(i,k) = tem  + hvap * qeso(i,k)
 !	     heo(i,k)  = min(heo(i,k),heso(i,k))
         endif
     enddo
             enddo
 
 !
 !  determine level with largest moist static energy
 !  this is the level where updraft starts
 !
             do i=1,im
     hmax(i) = heo(i,1)
     kb(i)   = 1
             enddo
             do k = 2, km
     do i=1,im
         if (k .le. kbm(i)) then
             if(heo(i,k).gt.hmax(i)) then
                 kb(i)   = k
                 hmax(i) = heo(i,k)
             endif
         endif
     enddo
             enddo

 !
             do k = 1, km1
     do i=1,im
         if (k .le. kmax(i)-1) then
             dz      = .5 * (zo(i,k+1) - zo(i,k))
             dp      = .5 * (pfld(i,k+1) - pfld(i,k))
             es      = 0.01 * fpvsx(to(i,k+1))	   ! fpvsx is in pa
             pprime  = pfld(i,k+1) + epsm1 * es
             qs      = eps * es / pprime
             dqsdp   = - qs / pprime
             desdt   = es * (fact1 / to(i,k+1) + fact2 / (to(i,k+1)**2))
             dqsdt   = qs * pfld(i,k+1) * desdt / (es * pprime)
             gamma   = el2orc * qeso(i,k+1) / (to(i,k+1)**2)
             dt      = (g * dz + hvap * dqsdp * dp) / (cp * (1. + gamma))
             dq      = dqsdt * dt + dqsdp * dp
             to(i,k) = to(i,k+1) + dt
             qo(i,k) = qo(i,k+1) + dq
             po(i,k) = .5 * (pfld(i,k) + pfld(i,k+1))
         endif
     enddo
             enddo
    !
             do k = 1, km1
     do i=1,im
         if (k .le. kmax(i)-1) then
             qeso(i,k) = 0.01 * fpvsx(to(i,k))	   ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (po(i,k) + epsm1*qeso(i,k))
             val1      =	     1.e-8
             qeso(i,k) = max(qeso(i,k), val1)
             val2      =	   1.e-10
             qo(i,k)   = max(qo(i,k), val2 )
 !	     qo(i,k)   = min(qo(i,k),qeso(i,k))
             frh(i,k)  = 1. - min(qo(i,k)/qeso(i,k), 1.)
             heo(i,k)  = .5 * g * (zo(i,k) + zo(i,k+1)) +       &
            & 		 cp * to(i,k) + hvap * qo(i,k)
             heso(i,k) = .5 * g * (zo(i,k) + zo(i,k+1)) +         &
            & 		 cp * to(i,k) + hvap * qeso(i,k)
             uo(i,k)   = .5 * (uo(i,k) + uo(i,k+1))
             vo(i,k)   = .5 * (vo(i,k) + vo(i,k+1))
         endif
     enddo
             enddo
!       	     write(*,*)qeso

 !
 !  look for the level of free convection as cloud base
 !
             do i=1,im
     flg(i)   = .true.
     kbcon(i) = kmax(i)
                enddo
             
             do k = 1, km1
     do i=1,im
         if (flg(i).and.k.le.kbmax(i)) then
             if(k.gt.kb(i).and.heo(i,kb(i)).gt.heso(i,k)) then
                 kbcon(i) = k
                 flg(i)	= .false.
             endif
         endif
     enddo
             enddo
             
 !
             do i=1,im
     if(kbcon(i).eq.kmax(i)) cnvflg(i) = .false.
             enddo
 !!
             totflg = .true.
             do i=1,im
     totflg = totflg .and. (.not. cnvflg(i))
             enddo
             if(totflg) return
 !!
 !
 !  determine critical convective inhibition
 !  as a function of vertical velocity at cloud base.
 !
             do i=1,im
     if(cnvflg(i)) then
 !	   pdot(i)  = 10.* dot(i,kbcon(i))
         pdot(i)  = 0.01 * dot(i,kbcon(i)) ! now dot is in pa/s
     endif
             enddo
             do i=1,im
     if(cnvflg(i)) then
         if(islimsk(i) == 1) then
             w1 = w1l
             w2 = w2l
             w3 = w3l
             w4 = w4l
         else
             w1 = w1s
             w2 = w2s
             w3 = w3s
             w4 = w4s
         endif
         if(pdot(i).le.w4) then
             tem = (pdot(i) - w4) / (w3 - w4)
         elseif(pdot(i).ge.-w4) then
             tem = - (pdot(i) + w4) / (w4 - w3)
         else
             tem = 0.
         endif
         val1    =		 -1.
         tem = max(tem,val1)
         val2    =		 1.
         tem = min(tem,val2)
         tem = 1. - tem
         tem1= .5*(cincrmax-cincrmin)
         cincr = cincrmax - tem * tem1
         pbcdif(i) = pfld(i,kb(i)) - pfld(i,kbcon(i))
         if(pbcdif(i).gt.cincr) then
                cnvflg(i) = .false.
         endif
     endif
             enddo
 !!
             totflg = .true.
             do i=1,im
     totflg = totflg .and. (.not. cnvflg(i))
             enddo
             if(totflg) return
 !!
 !
 !  assume that updraft entrainment rate above cloud base is
 !    same as that at cloud base
 !
             do k = 2, km1
     do i=1,im
         if(cnvflg(i).and.              &
            &      (k.gt.kbcon(i).and.k.lt.kmax(i))) then
                 xlamue(i,k) = xlamue(i,kbcon(i))
         endif
     enddo
             enddo
 !
 !  assume the detrainment rate for the updrafts to be same as
 !  the entrainment rate at cloud base
 !
             do i = 1, im
     if(cnvflg(i)) then
         xlamud(i) = xlamue(i,kbcon(i))
     endif
             enddo
 !
 !  functions rapidly decreasing with height, mimicking a cloud ensemble
 !    (bechtold et al., 2008)
 !
             do k = 2, km1
     do i=1,im
         if(cnvflg(i).and.                 &
            &      (k.gt.kbcon(i).and.k.lt.kmax(i))) then
                 tem = qeso(i,k)/qeso(i,kbcon(i))
                 fent1(i,k) = tem**2
                 fent2(i,k) = tem**3
         endif
     enddo
             enddo
 !
 !  final entrainment rate as the sum of turbulent part and organized entrainment
 !    depending on the environmental relative humidity
 !    (bechtold et al., 2008)
 !
             do k = 2, km1
     do i=1,im
         if(cnvflg(i).and.                &
            &      (k.ge.kbcon(i).and.k.lt.kmax(i))) then
                 tem = cxlamu * frh(i,k) * fent2(i,k)
                 xlamue(i,k) = xlamue(i,k)*fent1(i,k) + tem
         endif
     enddo
             enddo
 !
 !  determine updraft mass flux for the subcloud layers
 !
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i)) then
             if(k.lt.kbcon(i).and.k.ge.kb(i)) then
                 dz	= zi(i,k+1) - zi(i,k)
                 ptem	= 0.5*(xlamue(i,k)+xlamue(i,k+1))-xlamud(i)
                 eta(i,k) = eta(i,k+1) / (1. + ptem * dz)
             endif
         endif
     enddo
             enddo
 !
 !  compute mass flux above cloud base
 !
             do k = 2, km1
     do i = 1, im
        if(cnvflg(i))then
            if(k.gt.kbcon(i).and.k.lt.kmax(i)) then
                 dz	= zi(i,k) - zi(i,k-1)
                 ptem	= 0.5*(xlamue(i,k)+xlamue(i,k-1))-xlamud(i)
                 eta(i,k) = eta(i,k-1) * (1 + ptem * dz)
            endif
        endif
     enddo
             enddo
 !
 !  compute updraft cloud properties
 !
             do i = 1, im
     if(cnvflg(i)) then
         indx 	= kb(i)
         hcko(i,indx) = heo(i,indx)
         ucko(i,indx) = uo(i,indx)
         vcko(i,indx) = vo(i,indx)
         pwavo(i)	= 0.
     endif
             enddo
 !
 !  cloud property is modified by the entrainment process
 !
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.lt.kmax(i)) then
                 dz   = zi(i,k) - zi(i,k-1)
                 tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
                 tem1 = 0.5 * xlamud(i) * dz
                 factor = 1. + tem - tem1
                 ptem = 0.5 * tem + pgcon
                 ptem1= 0.5 * tem - pgcon
                 hcko(i,k) = ((1.-tem1)*hcko(i,k-1)+tem*0.5*                    &
            & 		    (heo(i,k)+heo(i,k-1)))/factor
                 ucko(i,k) = ((1.-tem1)*ucko(i,k-1)+ptem*uo(i,k)                &
            & 		    +ptem1*uo(i,k-1))/factor
                 vcko(i,k) = ((1.-tem1)*vcko(i,k-1)+ptem*vo(i,k)                &
            & 		    +ptem1*vo(i,k-1))/factor
                 dbyo(i,k) = hcko(i,k) - heso(i,k)
             endif
         endif
     enddo
             enddo
 !
 !   taking account into convection inhibition due to existence of
 !    dry layers below cloud base
 !
             do i=1,im
     flg(i) = cnvflg(i)
     kbcon1(i) = kmax(i)
             enddo
             do k = 2, km1
             do i=1,im
     if (flg(i).and.k.lt.kmax(i)) then
         if(k.ge.kbcon(i).and.dbyo(i,k).gt.0.) then
             kbcon1(i) = k
             flg(i)    = .false.
         endif
     endif
             enddo
             enddo
             do i=1,im
     if(cnvflg(i)) then
         if(kbcon1(i).eq.kmax(i)) cnvflg(i) = .false.
     endif
             enddo
             do i=1,im
     if(cnvflg(i)) then
         tem = pfld(i,kbcon(i)) - pfld(i,kbcon1(i))
         if(tem.gt.dthk) then
                cnvflg(i) = .false.
         endif
     endif
             enddo
 !!
             totflg = .true.
             do i = 1, im
     totflg = totflg .and. (.not. cnvflg(i))
             enddo
             if(totflg) return
 !!
 !
 !  determine first guess cloud top as the level of zero buoyancy
 !
             do i = 1, im
     flg(i) = cnvflg(i)
     ktcon(i) = 1
             enddo
             do k = 2, km1
             do i = 1, im
     if (flg(i).and.k .lt. kmax(i)) then
         if(k.gt.kbcon1(i).and.dbyo(i,k).lt.0.) then
                ktcon(i) = k
                flg(i)   = .false.
         endif
     endif
             enddo
             enddo
 !
             do i = 1, im
     if(cnvflg(i)) then
         tem = pfld(i,kbcon(i))-pfld(i,ktcon(i))
         if(tem.lt.cthk) cnvflg(i) = .false.
     endif
             enddo
 !!
             totflg = .true.
             do i = 1, im
     totflg = totflg .and. (.not. cnvflg(i))
             enddo
             if(totflg) return
 !!
 !
 !  search for downdraft originating level above theta-e minimum
 !
             do i = 1, im
     if(cnvflg(i)) then
            hmin(i) = heo(i,kbcon1(i))
            lmin(i) = kbmax(i)
            jmin(i) = kbmax(i)
     endif
             enddo
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i) .and. k .le. kbmax(i)) then
             if(k.gt.kbcon1(i).and.heo(i,k).lt.hmin(i)) then
        lmin(i) = k + 1
        hmin(i) = heo(i,k)
             endif
         endif
     enddo
             enddo
 !
 !  make sure that jmin(i) is within the cloud
 !
             do i = 1, im
     if(cnvflg(i)) then
         jmin(i) = min(lmin(i),ktcon(i)-1)
         jmin(i) = max(jmin(i),kbcon1(i)+1)
         if(jmin(i).ge.ktcon(i)) cnvflg(i) = .false.
     endif
             enddo
 !
 !  specify upper limit of mass flux at cloud base
 !
             do i = 1, im
     if(cnvflg(i)) then
 !	   xmbmax(i) = .1
 !
         k = kbcon(i)
         dp = 1000. * del(i,k)
         xmbmax(i) = dp / (g * dt2)
 !
 !	   tem = dp / (g * dt2)
 !	   xmbmax(i) = min(tem, xmbmax(i))
     endif
             enddo
 !
 !  compute cloud moisture property and precipitation
 !
             do i = 1, im
     if (cnvflg(i)) then
         aa1(i) = 0.
         qcko(i,kb(i)) = qo(i,kb(i))
         qrcko(i,kb(i)) = qo(i,kb(i))
 !	   rhbar(i) = 0.
     endif
             enddo
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.lt.ktcon(i)) then
                 dz    = zi(i,k) - zi(i,k-1)
                 gamma = el2orc * qeso(i,k) / (to(i,k)**2)
                 qrch = qeso(i,k)                                       &
            & 	    + gamma * dbyo(i,k) / (hvap * (1. + gamma))
 !j
                 tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
                 tem1 = 0.5 * xlamud(i) * dz
                 factor = 1. + tem - tem1
                 qcko(i,k) = ((1.-tem1)*qcko(i,k-1)+tem*0.5*             &
            & 		    (qo(i,k)+qo(i,k-1)))/factor
                 qrcko(i,k) = qcko(i,k)
 !j
                 dq = eta(i,k) * (qcko(i,k) - qrch)
 !
 !	       rhbar(i) = rhbar(i) + qo(i,k) / qeso(i,k)
 !
 !  check if there is excess moisture to release latent heat
 !
                 if(k.ge.kbcon(i).and.dq.gt.0.) then
         etah = .5 * (eta(i,k) + eta(i,k-1))
         if(ncloud.gt.0..and.k.gt.jmin(i)) then
             dp = 1000. * del(i,k)
             qlk = dq / (eta(i,k) + etah * (c0 + c1) * dz)
             dellal(i,k) = etah * c1 * dz * qlk * g / dp
         else
             qlk = dq / (eta(i,k) + etah * c0 * dz)
         endif
         aa1(i) = aa1(i) - dz * g * qlk
         qcko(i,k) = qlk + qrch
         pwo(i,k) = etah * c0 * dz * qlk
         pwavo(i) = pwavo(i) + pwo(i,k)
 !		 cnvwt(i,k) = (etah*qlk + pwo(i,k)) * g / dp
         cnvwt(i,k) = etah * qlk * g / dp
                 endif
             endif
         endif
     enddo
             enddo
 !
 !     do i = 1, im
 !	 if(cnvflg(i)) then
 !	   indx = ktcon(i) - kb(i) - 1
 !	   rhbar(i) = rhbar(i) / float(indx)
 !	 endif
 !     enddo
 !
 !  calculate cloud work function
 !
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i)) then
             if(k.ge.kbcon(i).and.k.lt.ktcon(i)) then
                 dz1 = zo(i,k+1) - zo(i,k)
                 gamma = el2orc * qeso(i,k) / (to(i,k)**2)    
                 rfact =  1. + delta * cp * gamma            &
            & 		* to(i,k) / hvap
                 aa1(i) = aa1(i) +                           &
            & 		dz1 * (g / (cp * to(i,k)))         &
            & 		* dbyo(i,k) / (1. + gamma)         &
            & 		* rfact 
                 val = 0.
                 aa1(i)=aa1(i)+                              &
            & 		dz1 * g * delta *                  &   
            & 		max(val,(qeso(i,k) - qo(i,k)))
             endif
         endif
     enddo
             enddo
!       write(*,*)aa1

             do i = 1, im
     if(cnvflg(i).and.aa1(i).le.0.) cnvflg(i) = .false.
             enddo
 !!
             totflg = .true.
             do i=1,im
     totflg = totflg .and. (.not. cnvflg(i))
             enddo
             if(totflg) return
 !!
 !
 !  estimate the onvective overshooting as the level
 !    where the [aafac * cloud work function] becomes zero,
 !    which is the final cloud top
 !
             do i = 1, im
     if (cnvflg(i)) then
         aa2(i) = aafac * aa1(i)
     endif
             enddo
 !
             do i = 1, im
     flg(i) = cnvflg(i)
     ktcon1(i) = kmax(i) - 1
             enddo
             do k = 2, km1
     do i = 1, im
         if (flg(i)) then
             if(k.ge.ktcon(i).and.k.lt.kmax(i)) then
                 dz1 = zo(i,k+1) - zo(i,k)
                 gamma = el2orc * qeso(i,k) / (to(i,k)**2)
                 rfact =  1. + delta * cp * gamma                     &
            & 		* to(i,k) / hvap
                 aa2(i) = aa2(i) +                                    &
            & 		dz1 * (g / (cp * to(i,k)))                  & 
            & 		* dbyo(i,k) / (1. + gamma)                  &
            & 		* rfact
                 if(aa2(i).lt.0.) then
         ktcon1(i) = k
         flg(i) = .false.
                 endif
             endif
         endif
     enddo
             enddo
 !
 !  compute cloud moisture property, detraining cloud water
 !    and precipitation in overshooting layers
 !
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i)) then
             if(k.ge.ktcon(i).and.k.lt.ktcon1(i)) then
                 dz    = zi(i,k) - zi(i,k-1)
                 gamma = el2orc * qeso(i,k) / (to(i,k)**2)
                 qrch = qeso(i,k)                                            &
            & 	    + gamma * dbyo(i,k) / (hvap * (1. + gamma))
 !j
                 tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
                 tem1 = 0.5 * xlamud(i) * dz
                 factor = 1. + tem - tem1
                 qcko(i,k) = ((1.-tem1)*qcko(i,k-1)+tem*0.5*                  &
            & 		    (qo(i,k)+qo(i,k-1)))/factor
                 qrcko(i,k) = qcko(i,k)
 !j
                 dq = eta(i,k) * (qcko(i,k) - qrch)
 !
 !  check if there is excess moisture to release latent heat
 !
                 if(dq.gt.0.) then
         etah = .5 * (eta(i,k) + eta(i,k-1))
         if(ncloud.gt.0.) then
             dp = 1000. * del(i,k)
             qlk = dq / (eta(i,k) + etah * (c0 + c1) * dz)
             dellal(i,k) = etah * c1 * dz * qlk * g / dp
         else
             qlk = dq / (eta(i,k) + etah * c0 * dz)
         endif
         qcko(i,k) = qlk + qrch
         pwo(i,k) = etah * c0 * dz * qlk
         pwavo(i) = pwavo(i) + pwo(i,k)
 !		 cnvwt(i,k) = (etah*qlk + pwo(i,k)) * g / dp
         cnvwt(i,k) = etah * qlk * g / dp
                 endif
             endif
         endif
     enddo
             enddo
 !
 ! exchange ktcon with ktcon1
 !
             do i = 1, im
     if(cnvflg(i)) then
         kk = ktcon(i)
         ktcon(i) = ktcon1(i)
         ktcon1(i) = kk
     endif
             enddo
 !
 !  this section is ready for cloud water
 !
             if(ncloud.gt.0) then
 !
 !  compute liquid and vapor separation at cloud top
 !
             do i = 1, im
     if(cnvflg(i)) then
         k = ktcon(i) - 1
         gamma = el2orc * qeso(i,k) / (to(i,k)**2)
         qrch = qeso(i,k)                                                &
            & 	+ gamma * dbyo(i,k) / (hvap * (1. + gamma))
         dq = qcko(i,k) - qrch
 !
 !  check if there is excess moisture to release latent heat
 !
         if(dq.gt.0.) then
             qlko_ktcon(i) = dq
             qcko(i,k) = qrch
         endif
     endif
             enddo
             endif
 !
 !!!cc if(lat.eq.latd.and.lon.eq.lond.and.cnvflg(i)) then
 !!!cc   print *, ' aa1(i) before dwndrft =', aa1(i)
 !!!cc endif
 !
 !------- downdraft calculations
 !
 !--- compute precipitation efficiency in terms of windshear
 !
             do i = 1, im
     if(cnvflg(i)) then
         vshear(i) = 0.
     endif
             enddo
             do k = 2, km
     do i = 1, im
         if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.le.ktcon(i)) then
                 shear= sqrt((uo(i,k)-uo(i,k-1)) ** 2                         &
            & 		 + (vo(i,k)-vo(i,k-1)) ** 2)
                 vshear(i) = vshear(i) + shear
             endif
         endif
     enddo
             enddo
             do i = 1, im
     if(cnvflg(i)) then
         vshear(i) = 1.e3 * vshear(i) / (zi(i,ktcon(i))-zi(i,kb(i)))
         e1=1.591-.639*vshear(i)                                          & 
            &       +.0953*(vshear(i)**2)-.00496*(vshear(i)**3)
         edt(i)=1.-e1
         val =	 .9
         edt(i) = min(edt(i),val)
         val =	 .0
         edt(i) = max(edt(i),val)
         edto(i)=edt(i)
         edtx(i)=edt(i)
     endif
             enddo
 !
 !  determine detrainment rate between 1 and kbcon
 !
             do i = 1, im
     if(cnvflg(i)) then
         sumx(i) = 0.
     endif
             enddo
             do k = 1, km1
             do i = 1, im
     if(cnvflg(i).and.k.ge.1.and.k.lt.kbcon(i)) then
         dz = zi(i,k+1) - zi(i,k)
         sumx(i) = sumx(i) + dz
     endif
             enddo
             enddo
             do i = 1, im
     beta = betas
     if(islimsk(i) == 1) beta = betal
     if(cnvflg(i)) then
         dz  = (sumx(i)+zi(i,1))/float(kbcon(i))
         tem = 1./float(kbcon(i))
         xlamd(i) = (1.-beta**tem)/dz
     endif
             enddo
 !
 !  determine downdraft mass flux
 !
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i) .and. k .le. kmax(i)-1) then
            if(k.lt.jmin(i).and.k.ge.kbcon(i)) then
                 dz	 = zi(i,k+1) - zi(i,k)
                 ptem	 = xlamdd - xlamde
                 etad(i,k) = etad(i,k+1) * (1. - ptem * dz)
            else if(k.lt.kbcon(i)) then
                 dz	 = zi(i,k+1) - zi(i,k)
                 ptem	 = xlamd(i) + xlamdd - xlamde
                 etad(i,k) = etad(i,k+1) * (1. - ptem * dz)
            endif
         endif
     enddo
             enddo
 !
 !--- downdraft moisture properties
 !
             do i = 1, im
     if(cnvflg(i)) then
         jmn = jmin(i)
         hcdo(i,jmn) = heo(i,jmn)
         qcdo(i,jmn) = qo(i,jmn)
         qrcdo(i,jmn)= qo(i,jmn)
         ucdo(i,jmn) = uo(i,jmn)
         vcdo(i,jmn) = vo(i,jmn)
         pwevo(i) = 0.
     endif
             enddo
 !j
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i) .and. k.lt.jmin(i)) then
                 dz = zi(i,k+1) - zi(i,k)
                 if(k.ge.kbcon(i)) then
            tem  = xlamde * dz
            tem1 = 0.5 * xlamdd * dz
                 else
            tem  = xlamde * dz
            tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
                 endif
                 factor = 1. + tem - tem1
                 ptem = 0.5 * tem - pgcon
                 ptem1= 0.5 * tem + pgcon
                 hcdo(i,k) = ((1.-tem1)*hcdo(i,k+1)+tem*0.5*                       &
            & 		    (heo(i,k)+heo(i,k+1)))/factor
                 ucdo(i,k) = ((1.-tem1)*ucdo(i,k+1)+ptem*uo(i,k+1)                 &
            & 		    +ptem1*uo(i,k))/factor
                 vcdo(i,k) = ((1.-tem1)*vcdo(i,k+1)+ptem*vo(i,k+1)                 &
            & 		    +ptem1*vo(i,k))/factor
                 dbyo(i,k) = hcdo(i,k) - heso(i,k)
         endif
     enddo
             enddo
 !
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i).and.k.lt.jmin(i)) then
                 gamma	  = el2orc * qeso(i,k) / (to(i,k)**2)
                 qrcdo(i,k) = qeso(i,k)+                                            &
            & 	       (1./hvap)*(gamma/(1.+gamma))*dbyo(i,k)
 !	       detad	  = etad(i,k+1) - etad(i,k)
 !j
                 dz = zi(i,k+1) - zi(i,k)
                 if(k.ge.kbcon(i)) then
            tem  = xlamde * dz
            tem1 = 0.5 * xlamdd * dz
                 else
            tem  = xlamde * dz
            tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
                 endif
                 factor = 1. + tem - tem1
                 qcdo(i,k) = ((1.-tem1)*qrcdo(i,k+1)+tem*0.5*                      & 
            & 		    (qo(i,k)+qo(i,k+1)))/factor
 !j
 !	       pwdo(i,k)  = etad(i,k+1) * qcdo(i,k+1) -
 !    & 		    etad(i,k) * qrcdo(i,k)
 !	       pwdo(i,k)  = pwdo(i,k) - detad *
 !    & 		   .5 * (qrcdo(i,k) + qrcdo(i,k+1))
 !j
                 pwdo(i,k)  = etad(i,k) * (qcdo(i,k) - qrcdo(i,k))
                 pwevo(i)   = pwevo(i) + pwdo(i,k)
         endif
     enddo
             enddo
 !
 !--- final downdraft strength dependent on precip
 !--- efficiency(edt), normalized condensate(pwav), and
 !--- evaporate(pwev)
 !
             do i = 1, im
     edtmax = edtmaxl
     if(islimsk(i) == 0) edtmax = edtmaxs
     if(cnvflg(i)) then
         if(pwevo(i).lt.0.) then
             edto(i) = -edto(i) * pwavo(i) / pwevo(i)
             edto(i) = min(edto(i),edtmax)
         else
             edto(i) = 0.
         endif
     endif
             enddo
 !
 !--- downdraft cloudwork functions
 !
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i) .and. k .lt. jmin(i)) then
                 gamma = el2orc * qeso(i,k) / to(i,k)**2
                 dhh=hcdo(i,k)
                 dt=to(i,k)
                 dg=gamma
                 dh=heso(i,k)
                 dz=-1.*(zo(i,k+1)-zo(i,k))
                 aa1(i)=aa1(i)+edto(i)*dz*(g/(cp*dt))*((dhh-dh)/(1.+dg))                 &
            & 	      *(1.+delta*cp*dg*dt/hvap)
                 val=0.
                 aa1(i)=aa1(i)+edto(i)*                                                  &
            &        dz*g*delta*max(val,(qeso(i,k)-qo(i,k)))
         endif
     enddo
             enddo
             do i = 1, im
     if(cnvflg(i).and.aa1(i).le.0.) then
            cnvflg(i) = .false.
     endif
             enddo
 !!
             totflg = .true.
             do i=1,im
     totflg = totflg .and. (.not. cnvflg(i))
             enddo
             if(totflg) return
 !!
 !
 !--- what would the change be, that a cloud with unit mass
 !--- will do to the environment?
 !
             do k = 1, km
     do i = 1, im
         if(cnvflg(i) .and. k .le. kmax(i)) then
             dellah(i,k) = 0.
             dellaq(i,k) = 0.
             dellau(i,k) = 0.
             dellav(i,k) = 0.
         endif
     enddo
             enddo
             do i = 1, im
     if(cnvflg(i)) then
         dp = 1000. * del(i,1)
         dellah(i,1) = edto(i) * etad(i,1) * (hcdo(i,1)                 &
            & 		  - heo(i,1)) * g / dp 
         dellaq(i,1) = edto(i) * etad(i,1) * (qrcdo(i,1)                 &
            & 		  - qo(i,1)) * g / dp
         dellau(i,1) = edto(i) * etad(i,1) * (ucdo(i,1)                  &
            & 		  - uo(i,1)) * g / dp
         dellav(i,1) = edto(i) * etad(i,1) * (vcdo(i,1)                  &
            & 		  - vo(i,1)) * g / dp
     endif
             enddo
 !
 !--- changed due to subsidence and entrainment
 !
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i).and.k.lt.ktcon(i)) then
                 aup = 1.
                 if(k.le.kb(i)) aup = 0.
                 adw = 1.
                 if(k.gt.jmin(i)) adw = 0.
                 dp = 1000. * del(i,k)
                 dz = zi(i,k) - zi(i,k-1)
 !
                 dv1h = heo(i,k)
                 dv2h = .5 * (heo(i,k) + heo(i,k-1))
                 dv3h = heo(i,k-1)
                 dv1q = qo(i,k)
                 dv2q = .5 * (qo(i,k) + qo(i,k-1))
                 dv3q = qo(i,k-1)
                 dv1u = uo(i,k)
                 dv2u = .5 * (uo(i,k) + uo(i,k-1))
                 dv3u = uo(i,k-1)
                 dv1v = vo(i,k)
                 dv2v = .5 * (vo(i,k) + vo(i,k-1))
                 dv3v = vo(i,k-1)
 !
                 tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1))
                 tem1 = xlamud(i)
 !
                 if(k.le.kbcon(i)) then
         ptem  = xlamde
         ptem1 = xlamd(i)+xlamdd
                 else
         ptem  = xlamde
         ptem1 = xlamdd
                 endif
 !j
                 dellah(i,k) = dellah(i,k) +                                           &
            &     ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1h                               &
            &    - (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3h                           &
            &    - (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2h*dz                 &
            &    +  aup*tem1*eta(i,k-1)*.5*(hcko(i,k)+hcko(i,k-1))*dz                      &
            &    +  adw*edto(i)*ptem1*etad(i,k)*.5*(hcdo(i,k)+hcdo(i,k-1))*dz              &
            & 	) *g/dp
 !j
                 dellaq(i,k) = dellaq(i,k) +                                           &
            &     ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1q                               &
            &    - (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3q                           &
            &    - (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2q*dz                 &
            &    +  aup*tem1*eta(i,k-1)*.5*(qrcko(i,k)+qcko(i,k-1))*dz                     &
            &    +  adw*edto(i)*ptem1*etad(i,k)*.5*(qrcdo(i,k)+qcdo(i,k-1))*dz             &
            & 	) *g/dp
 !j
                 dellau(i,k) = dellau(i,k) +                                           &
            &     ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1u                               &
            &    - (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3u                           &
            &    - (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2u*dz                 &
            &    +  aup*tem1*eta(i,k-1)*.5*(ucko(i,k)+ucko(i,k-1))*dz                      & 
            &    +  adw*edto(i)*ptem1*etad(i,k)*.5*(ucdo(i,k)+ucdo(i,k-1))*dz 	     & 
            &    -  pgcon*(aup*eta(i,k-1)-adw*edto(i)*etad(i,k))*(dv1u-dv3u)		     & 
            & 	) *g/dp
 !j
                 dellav(i,k) = dellav(i,k) +                                            & 
            &     ((aup*eta(i,k)-adw*edto(i)*etad(i,k))*dv1v				      & 
            &    - (aup*eta(i,k-1)-adw*edto(i)*etad(i,k-1))*dv3v			      & 
            &    - (aup*tem*eta(i,k-1)+adw*edto(i)*ptem*etad(i,k))*dv2v*dz                  &  	       
            &    +  aup*tem1*eta(i,k-1)*.5*(vcko(i,k)+vcko(i,k-1))*dz 		      & 
            &    +  adw*edto(i)*ptem1*etad(i,k)*.5*(vcdo(i,k)+vcdo(i,k-1))*dz 	      & 
            &    -  pgcon*(aup*eta(i,k-1)-adw*edto(i)*etad(i,k))*(dv1v-dv3v)                &
            & 	) *g/dp
 !j
         endif
     enddo
             enddo
 !
 !------- cloud top
 !
             do i = 1, im
     if(cnvflg(i)) then
         indx = ktcon(i)
         dp = 1000. * del(i,indx)
         dv1h = heo(i,indx-1)
         dellah(i,indx) = eta(i,indx-1) *                                           &
            & 		    (hcko(i,indx-1) - dv1h) * g / dp
         dv1q = qo(i,indx-1)
         dellaq(i,indx) = eta(i,indx-1) *                                           &
            & 		    (qcko(i,indx-1) - dv1q) * g / dp
         dv1u = uo(i,indx-1)
         dellau(i,indx) = eta(i,indx-1) *                                           &
            & 		    (ucko(i,indx-1) - dv1u) * g / dp
         dv1v = vo(i,indx-1)
         dellav(i,indx) = eta(i,indx-1) *                                            &
            & 		    (vcko(i,indx-1) - dv1v) * g / dp
 !
 !  cloud water
 !
         dellal(i,indx) = eta(i,indx-1) *                                          &
            & 		    qlko_ktcon(i) * g / dp
     endif
             enddo
 !
 !------- final changed variable per unit mass flux
 !
             do k = 1, km
     do i = 1, im
         if (cnvflg(i).and.k .le. kmax(i)) then
             if(k.gt.ktcon(i)) then
                 qo(i,k) = q1(i,k)
                 to(i,k) = t1(i,k)
             endif
             if(k.le.ktcon(i)) then
                 qo(i,k) = dellaq(i,k) * mbdt + q1(i,k)
                 dellat = (dellah(i,k) - hvap * dellaq(i,k)) / cp
                 to(i,k) = dellat * mbdt + t1(i,k)
                 val   =  	 1.e-10
                 qo(i,k) = max(qo(i,k), val  )
             endif
         endif
     enddo
             enddo
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
 !--- the above changed environment is now used to calulate the
 !--- effect the arbitrary cloud(with unit mass flux)
 !--- would have on the stability,
 !--- which then is used to calculate the real mass flux,
 !--- necessary to keep this change in balance with the large-scale
 !--- destabilization.
 !
 !--- environmental conditions again, first heights
 !
             do k = 1, km
     do i = 1, im
         if(cnvflg(i) .and. k .le. kmax(i)) then
             qeso(i,k) = 0.01 * fpvsx(to(i,k))	   ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (pfld(i,k)+epsm1*qeso(i,k))
             val       =	     1.e-8
             qeso(i,k) = max(qeso(i,k), val )
 !	     tvo(i,k)  = to(i,k) + delta * to(i,k) * qo(i,k)
         endif
     enddo
             enddo
 !
 !--- moist static energy
 !
 !! - Recalculate moist static energy and saturation moist static energy.
             do k = 1, km1
     do i = 1, im
         if(cnvflg(i) .and. k .le. kmax(i)-1) then
             dz = .5 * (zo(i,k+1) - zo(i,k))
             dp = .5 * (pfld(i,k+1) - pfld(i,k))
             es = 0.01 * fpvsx(to(i,k+1))      ! fpvsx is in pa
             pprime = pfld(i,k+1) + epsm1 * es
             qs = eps * es / pprime
             dqsdp = - qs / pprime
             desdt = es * (fact1 / to(i,k+1) + fact2 / (to(i,k+1)**2))
             dqsdt = qs * pfld(i,k+1) * desdt / (es * pprime)
             gamma = el2orc * qeso(i,k+1) / (to(i,k+1)**2)
             dt = (g * dz + hvap * dqsdp * dp) / (cp * (1. + gamma))
             dq = dqsdt * dt + dqsdp * dp
             to(i,k) = to(i,k+1) + dt
             qo(i,k) = qo(i,k+1) + dq
             po(i,k) = .5 * (pfld(i,k) + pfld(i,k+1))
         endif
     enddo
             enddo
             do k = 1, km1
     do i = 1, im
         if(cnvflg(i) .and. k .le. kmax(i)-1) then
             qeso(i,k) = 0.01 * fpvsx(to(i,k))	   ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (po(i,k) + epsm1 * qeso(i,k))
             val1      =	     1.e-8
             qeso(i,k) = max(qeso(i,k), val1)
             val2      =	   1.e-10
             qo(i,k)   = max(qo(i,k), val2 )
 !	     qo(i,k)   = min(qo(i,k),qeso(i,k))
             heo(i,k)	= .5 * g * (zo(i,k) + zo(i,k+1)) +         &
            & 		   cp * to(i,k) + hvap * qo(i,k)
             heso(i,k) = .5 * g * (zo(i,k) + zo(i,k+1)) +           &
            & 		 cp * to(i,k) + hvap * qeso(i,k)
         endif
     enddo
             enddo
             do i = 1, im
     if(cnvflg(i)) then
         k = kmax(i)
         heo(i,k) = g * zo(i,k) + cp * to(i,k) + hvap * qo(i,k)
         heso(i,k) = g * zo(i,k) + cp * to(i,k) + hvap * qeso(i,k)
 !	   heo(i,k) = min(heo(i,k),heso(i,k))
     endif
             enddo
 !
 !**************************** static control
 !
 !------- moisture and cloud work functions
 !
             do i = 1, im
     if(cnvflg(i)) then
         xaa0(i) = 0.
         xpwav(i) = 0.
     endif
             enddo
 !
             do i = 1, im
     if(cnvflg(i)) then
         indx = kb(i)
         hcko(i,indx) = heo(i,indx)
         qcko(i,indx) = qo(i,indx)
     endif
             enddo
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.le.ktcon(i)) then
                 dz = zi(i,k) - zi(i,k-1)
                 tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
                 tem1 = 0.5 * xlamud(i) * dz
                 factor = 1. + tem - tem1 
                 hcko(i,k) = ((1.-tem1)*hcko(i,k-1)+tem*0.5*            &
            & 		    (heo(i,k)+heo(i,k-1)))/factor
             endif
         endif
     enddo
             enddo
             do k = 2, km1
     do i = 1, im
         if (cnvflg(i)) then
             if(k.gt.kb(i).and.k.lt.ktcon(i)) then
                 dz = zi(i,k) - zi(i,k-1)
                 gamma = el2orc * qeso(i,k) / (to(i,k)**2)
                 xdby = hcko(i,k) - heso(i,k)
                 xqrch = qeso(i,k)                                         &
            & 	     + gamma * xdby / (hvap * (1. + gamma))
 !j
                 tem  = 0.5 * (xlamue(i,k)+xlamue(i,k-1)) * dz
                 tem1 = 0.5 * xlamud(i) * dz
                 factor = 1. + tem - tem1
                 qcko(i,k) = ((1.-tem1)*qcko(i,k-1)+tem*0.5*                 &
            & 		    (qo(i,k)+qo(i,k-1)))/factor
 !j
                 dq = eta(i,k) * (qcko(i,k) - xqrch)
 !
                 if(k.ge.kbcon(i).and.dq.gt.0.) then
         etah = .5 * (eta(i,k) + eta(i,k-1))
         if(ncloud.gt.0..and.k.gt.jmin(i)) then
             qlk = dq / (eta(i,k) + etah * (c0 + c1) * dz)
         else
             qlk = dq / (eta(i,k) + etah * c0 * dz)
         endif
         if(k.lt.ktcon1(i)) then
             xaa0(i) = xaa0(i) - dz * g * qlk
         endif
         qcko(i,k) = qlk + xqrch
         xpw = etah * c0 * dz * qlk
         xpwav(i) = xpwav(i) + xpw
                 endif
             endif
             if(k.ge.kbcon(i).and.k.lt.ktcon1(i)) then
                 dz1 = zo(i,k+1) - zo(i,k)
                 gamma = el2orc * qeso(i,k) / (to(i,k)**2)
                 rfact =  1. + delta * cp * gamma                      &
            & 		* to(i,k) / hvap
                 xaa0(i) = xaa0(i)                                     &
            & 	       + dz1 * (g / (cp * to(i,k)))                  &
            & 	       * xdby / (1. + gamma)                         &
            & 	       * rfact
                 val=0.
                 xaa0(i)=xaa0(i)+                                     &
            & 		dz1 * g * delta *                           &
            & 		max(val,(qeso(i,k) - qo(i,k)))
             endif
         endif
     enddo
             enddo
 !
 !------- downdraft calculations
 !
 !--- downdraft moisture properties
 !
             do i = 1, im
     if(cnvflg(i)) then
         jmn = jmin(i)
         hcdo(i,jmn) = heo(i,jmn)
         qcdo(i,jmn) = qo(i,jmn)
         qrcd(i,jmn) = qo(i,jmn)
         xpwev(i) = 0.
     endif
             enddo
 !j
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i) .and. k.lt.jmin(i)) then
                 dz = zi(i,k+1) - zi(i,k)
                 if(k.ge.kbcon(i)) then
            tem  = xlamde * dz
            tem1 = 0.5 * xlamdd * dz
                 else
            tem  = xlamde * dz
            tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
                 endif
                 factor = 1. + tem - tem1                          
                 hcdo(i,k) = ((1.-tem1)*hcdo(i,k+1)+tem*0.5*           &
            & 		    (heo(i,k)+heo(i,k+1)))/factor
         endif
     enddo
             enddo
 !j
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i) .and. k .lt. jmin(i)) then
                 dq = qeso(i,k)
                 dt = to(i,k)
                 gamma	= el2orc * dq / dt**2
                 dh	= hcdo(i,k) - heso(i,k)
                 qrcd(i,k)=dq+(1./hvap)*(gamma/(1.+gamma))*dh
 !	       detad	= etad(i,k+1) - etad(i,k)
 !j
                 dz = zi(i,k+1) - zi(i,k)
                 if(k.ge.kbcon(i)) then
            tem  = xlamde * dz
            tem1 = 0.5 * xlamdd * dz
                 else
            tem  = xlamde * dz
            tem1 = 0.5 * (xlamd(i)+xlamdd) * dz
                 endif
                 factor = 1. + tem - tem1                         
                 qcdo(i,k) = ((1.-tem1)*qrcd(i,k+1)+tem*0.5*                  &
            & 		    (qo(i,k)+qo(i,k+1)))/factor
 !j
 !	       xpwd	= etad(i,k+1) * qcdo(i,k+1) -
 !    & 		  etad(i,k) * qrcd(i,k)
 !	       xpwd	= xpwd - detad *
 !    & 		.5 * (qrcd(i,k) + qrcd(i,k+1))
 !j
                 xpwd	= etad(i,k) * (qcdo(i,k) - qrcd(i,k))
                 xpwev(i) = xpwev(i) + xpwd
         endif
     enddo
             enddo
 !
             do i = 1, im
     edtmax = edtmaxl
     if(islimsk(i) == 0) edtmax = edtmaxs
     if(cnvflg(i)) then
         if(xpwev(i).ge.0.) then
             edtx(i) = 0.
         else
             edtx(i) = -edtx(i) * xpwav(i) / xpwev(i)
             edtx(i) = min(edtx(i),edtmax)
         endif
     endif
             enddo
 !
 !
 !--- downdraft cloudwork functions
 !
 !
             do k = km1, 1, -1
     do i = 1, im
         if (cnvflg(i) .and. k.lt.jmin(i)) then
                 gamma = el2orc * qeso(i,k) / to(i,k)**2
                 dhh=hcdo(i,k)
                 dt= to(i,k)
                 dg= gamma
                 dh= heso(i,k)
                 dz=-1.*(zo(i,k+1)-zo(i,k))
                 xaa0(i)=xaa0(i)+edtx(i)*dz*(g/(cp*dt))*((dhh-dh)/(1.+dg))    &
            & 	       *(1.+delta*cp*dg*dt/hvap)
                 val=0.
                 xaa0(i)=xaa0(i)+edtx(i)*                                     &
            &        dz*g*delta*max(val,(qeso(i,k)-qo(i,k)))
         endif
     enddo
             enddo
 !
 !  calculate critical cloud work function
 !
             do i = 1, im
     if(cnvflg(i)) then
         if(pfld(i,ktcon(i)).lt.pcrit(15))then
             acrt(i)=acrit(15)*(975.-pfld(i,ktcon(i)))                      &
            & 	     /(975.-pcrit(15))
         else if(pfld(i,ktcon(i)).gt.pcrit(1))then
             acrt(i)=acrit(1)
         else
             k =  int((850. - pfld(i,ktcon(i)))/50.) + 2
             k = min(k,15)
             k = max(k,2)
             acrt(i)=acrit(k)+(acrit(k-1)-acrit(k))*                         &
            & 	  (pfld(i,ktcon(i))-pcrit(k))/(pcrit(k-1)-pcrit(k))
         endif
     endif
             enddo
             do i = 1, im
     if(cnvflg(i)) then
         if(islimsk(i) == 1) then
             w1 = w1l
             w2 = w2l
             w3 = w3l
             w4 = w4l
         else
             w1 = w1s
             w2 = w2s
             w3 = w3s
             w4 = w4s
         endif
 !
 !  modify critical cloud workfunction by cloud base vertical velocity
 !
         if(pdot(i).le.w4) then
             acrtfct(i) = (pdot(i) - w4) / (w3 - w4)
         elseif(pdot(i).ge.-w4) then
             acrtfct(i) = - (pdot(i) + w4) / (w4 - w3)
         else
             acrtfct(i) = 0.
         endif
         val1    =		 -1.
         acrtfct(i) = max(acrtfct(i),val1)
         val2    =		 1.
         acrtfct(i) = min(acrtfct(i),val2)
         acrtfct(i) = 1. - acrtfct(i)
 !
 !  modify acrtfct(i) by colume mean rh if rhbar(i) is greater than 80 percent
 !
 !	   if(rhbar(i).ge..8) then
 !	     acrtfct(i) = acrtfct(i) * (.9 - min(rhbar(i),.9)) * 10.
 !	   endif
 !
 !  modify adjustment time scale by cloud base vertical velocity
 !
         dtconv(i) = dt2 + max((1800. - dt2),0.) *                   &
            & 	       (pdot(i) - w2) / (w1 - w2)
 !	   dtconv(i) = max(dtconv(i), dt2)
 !	   dtconv(i) = 1800. * (pdot(i) - w2) / (w1 - w2)
         dtconv(i) = max(dtconv(i),dtmin)
         dtconv(i) = min(dtconv(i),dtmax)
 !
     endif
             enddo
 !
 !--- large scale forcing
 !
             do i= 1, im
     if(cnvflg(i)) then
         fld(i)=(aa1(i)-acrt(i)* acrtfct(i))/dtconv(i)
         if(fld(i).le.0.) cnvflg(i) = .false.
     endif
     if(cnvflg(i)) then
 !	   xaa0(i) = max(xaa0(i),0.)
         xk(i) = (xaa0(i) - aa1(i)) / mbdt
         if(xk(i).ge.0.) cnvflg(i) = .false.
     endif
 !
 !--- kernel, cloud base mass flux
 !
     if(cnvflg(i)) then
         xmb(i) = -fld(i) / xk(i)
         xmb(i) = min(xmb(i),xmbmax(i))
     endif
             enddo
 !!
             totflg = .true.
             do i=1,im
     totflg = totflg .and. (.not. cnvflg(i))
             enddo
             if(totflg) return
 !!
 !
 !  restore to,qo,uo,vo to t1,q1,u1,v1 in case convection stops
 !
 
             do k = 1, km
     do i = 1, im
         if (cnvflg(i) .and. k .le. kmax(i)) then
             to(i,k) = t1(i,k)
             qo(i,k) = q1(i,k)
             uo(i,k) = u1(i,k)
             vo(i,k) = v1(i,k)
             qeso(i,k) = 0.01 * fpvsx(t1(i,k))	   ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (pfld(i,k) + epsm1*qeso(i,k))
             val     =  	   1.e-8
             qeso(i,k) = max(qeso(i,k), val )
         endif
     enddo
             enddo
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 !
 !--- feedback: simply the changes from the cloud with unit mass flux
 !---		multiplied by  the mass flux necessary to keep the
 !---		equilibrium with the larger-scale.
 !
             do i = 1, im
     delhbar(i) = 0.
     delqbar(i) = 0.
     deltbar(i) = 0.
     delubar(i) = 0.
     delvbar(i) = 0.
     qcond(i) = 0.
             enddo
             do k = 1, km
     do i = 1, im
         if (cnvflg(i) .and. k .le. kmax(i)) then
             if(k.le.ktcon(i)) then
                 dellat = (dellah(i,k) - hvap * dellaq(i,k)) / cp
                 t1(i,k) = t1(i,k) + dellat * xmb(i) * dt2
                 q1(i,k) = q1(i,k) + dellaq(i,k) * xmb(i) * dt2
 !	       tem = 1./rcs(i)
 !	       u1(i,k) = u1(i,k) + dellau(i,k) * xmb(i) * dt2 * tem
 !	       v1(i,k) = v1(i,k) + dellav(i,k) * xmb(i) * dt2 * tem
                 u1(i,k) = u1(i,k) + dellau(i,k) * xmb(i) * dt2
                 v1(i,k) = v1(i,k) + dellav(i,k) * xmb(i) * dt2
                 dp = 1000. * del(i,k)
                 delhbar(i) = delhbar(i) + dellah(i,k)*xmb(i)*dp/g
                 delqbar(i) = delqbar(i) + dellaq(i,k)*xmb(i)*dp/g
                 deltbar(i) = deltbar(i) + dellat*xmb(i)*dp/g
                 delubar(i) = delubar(i) + dellau(i,k)*xmb(i)*dp/g
                 delvbar(i) = delvbar(i) + dellav(i,k)*xmb(i)*dp/g
             endif
         endif
     enddo
             enddo
             do k = 1, km
     do i = 1, im
         if (cnvflg(i) .and. k .le. kmax(i)) then
             if(k.le.ktcon(i)) then
                 qeso(i,k) = 0.01 * fpvsx(t1(i,k))      ! fpvsx is in pa
                 qeso(i,k) = eps * qeso(i,k)/(pfld(i,k) + epsm1*qeso(i,k))
                 val     =	     1.e-8
                 qeso(i,k) = max(qeso(i,k), val )
             endif
         endif
     enddo
             enddo
 !
             do i = 1, im
     rntot(i) = 0.
     delqev(i) = 0.
     delq2(i) = 0.
     flg(i) = cnvflg(i)
             enddo
             do k = km, 1, -1
     do i = 1, im
         if (cnvflg(i) .and. k .le. kmax(i)) then
             if(k.lt.ktcon(i)) then
                 aup = 1.
                 if(k.le.kb(i)) aup = 0.
                 adw = 1.
                 if(k.ge.jmin(i)) adw = 0.
                 rain =  aup * pwo(i,k) + adw * edto(i) * pwdo(i,k)
                 rntot(i) = rntot(i) + rain * xmb(i) * .001 * dt2
             endif
         endif
     enddo
             enddo
             do k = km, 1, -1
     do i = 1, im
         if (k .le. kmax(i)) then
             deltv(i) = 0.
             delq(i) = 0.
             qevap(i) = 0.
             if(cnvflg(i).and.k.lt.ktcon(i)) then
                 aup = 1.
                 if(k.le.kb(i)) aup = 0.
                 adw = 1.
                 if(k.ge.jmin(i)) adw = 0.
                 rain =  aup * pwo(i,k) + adw * edto(i) * pwdo(i,k)
                 rn(i) = rn(i) + rain * xmb(i) * .001 * dt2
             endif
             if(flg(i).and.k.lt.ktcon(i)) then
                 evef = edt(i) * evfact
                 if(islimsk(i) == 1) evef=edt(i) * evfactl
 !	       if(islimsk(i) == 1) evef=.07
 !	       if(islimsk(i) == 1) evef = 0.
                 qcond(i) = evef * (q1(i,k) - qeso(i,k))                       &
            & 		/ (1. + el2orc * qeso(i,k) / t1(i,k)**2)
                 dp = 1000. * del(i,k)
                 if(rn(i).gt.0..and.qcond(i).lt.0.) then
         qevap(i) = -qcond(i) * (1.-exp(-.32*sqrt(dt2*rn(i))))
         qevap(i) = min(qevap(i), rn(i)*1000.*g/dp)
         delq2(i) = delqev(i) + .001 * qevap(i) * dp / g
                 endif
                 if(rn(i).gt.0..and.qcond(i).lt.0..and.                         &
            & 	  delq2(i).gt.rntot(i)) then
         qevap(i) = 1000.* g * (rntot(i) - delqev(i)) / dp
         flg(i) = .false.
                 endif
                 if(rn(i).gt.0..and.qevap(i).gt.0.) then
         q1(i,k) = q1(i,k) + qevap(i)
         t1(i,k) = t1(i,k) - elocp * qevap(i)
         rn(i) = rn(i) - .001 * qevap(i) * dp / g
         deltv(i) = - elocp*qevap(i)/dt2
         delq(i) =  + qevap(i)/dt2
         delqev(i) = delqev(i) + .001*dp*qevap(i)/g
                 endif
                 dellaq(i,k) = dellaq(i,k) + delq(i) / xmb(i)
                 delqbar(i) = delqbar(i) + delq(i)*dp/g
                 deltbar(i) = deltbar(i) + deltv(i)*dp/g
             endif
         endif
     enddo
             enddo
 !j
 !     do i = 1, im
 !     if(me.eq.31.and.cnvflg(i)) then
 !     if(cnvflg(i)) then
 !	 print *, ' deep delhbar, delqbar, deltbar = ',
 !    & 	    delhbar(i),hvap*delqbar(i),cp*deltbar(i)
 !	 print *, ' deep delubar, delvbar = ',delubar(i),delvbar(i)
 !	 print *, ' precip =', hvap*rn(i)*1000./dt2
 !	 print*,'pdif= ',pfld(i,kbcon(i))-pfld(i,ktcon(i))
 !     endif
 !     enddo
 !
 !  precipitation rate converted to actual precip
 !  in unit of m instead of kg
 !
             do i = 1, im
     if(cnvflg(i)) then
 !
 !  in the event of upper level rain evaporation and lower level downdraft
 !    moistening, rn can become negative, in this case, we back out of the
 !    heating and the moistening
 !
 
         if(rn(i).lt.0..and..not.flg(i)) rn(i) = 0.
         if(rn(i).le.0.) then
             rn(i) = 0.
         else
             ktop(i) = ktcon(i)
             kbot(i) = kbcon(i)
             kcnv(i) = 1
             cldwrk(i) = aa1(i)
         endif
     endif
             enddo
 !
 !  convective cloud water
 !
             do k = 1, km
     do i = 1, im
         if (cnvflg(i) .and. rn(i).gt.0.) then
             if (k.ge.kbcon(i).and.k.lt.ktcon(i)) then
                 cnvw(i,k) = cnvwt(i,k) * xmb(i) * dt2
             endif
         endif
     enddo
             enddo
 !
 !  convective cloud cover
 !
             do k = 1, km
     do i = 1, im
         if (cnvflg(i) .and. rn(i).gt.0.) then
             if (k.ge.kbcon(i).and.k.lt.ktcon(i)) then
                 cnvc(i,k) = 0.04 * log(1. + 675. * eta(i,k) * xmb(i))
                 cnvc(i,k) = min(cnvc(i,k), 0.6)
                 cnvc(i,k) = max(cnvc(i,k), 0.0)
             endif
         endif
     enddo
             enddo
 
 !
 !  cloud water
 !
             if (ncloud.gt.0) then
 !
             do k = 1, km
     do i = 1, im
         if (cnvflg(i) .and. rn(i).gt.0.) then
             if (k.gt.kb(i).and.k.le.ktcon(i)) then
                 tem  = dellal(i,k) * xmb(i) * dt2
                 tem1 = max(0.0, min(1.0, (tcr-t1(i,k))*tcrf))
                 if (ql(i,k,2) .gt. -999.0) then
         ql(i,k,1) = ql(i,k,1) + tem * tem1	       ! ice
         ql(i,k,2) = ql(i,k,2) + tem *(1.0-tem1)       ! water
                 else
         ql(i,k,1) = ql(i,k,1) + tem
                 endif
             endif
         endif
     enddo
             enddo
 !
             endif
 !
             do k = 1, km
     do i = 1, im
         if(cnvflg(i).and.rn(i).le.0.) then
             if (k .le. kmax(i)) then
                 t1(i,k) = to(i,k)
                 q1(i,k) = qo(i,k)
                 u1(i,k) = uo(i,k)
                 v1(i,k) = vo(i,k)
             endif
         endif
     enddo
             enddo
 !
 ! hchuang code change
 !
             do k = 1, km
     do i = 1, im
         if(cnvflg(i).and.rn(i).gt.0.) then
             if(k.ge.kb(i) .and. k.lt.ktop(i)) then
                 ud_mf(i,k) = eta(i,k) * xmb(i) * dt2
             endif
         endif
     enddo
             enddo
             do i = 1, im
     if(cnvflg(i).and.rn(i).gt.0.) then
            k = ktop(i)-1
            dt_mf(i,k) = ud_mf(i,k)
     endif
             enddo
             do k = 1, km
     do i = 1, im
         if(cnvflg(i).and.rn(i).gt.0.) then
             if(k.ge.1 .and. k.le.jmin(i)) then
                 dd_mf(i,k) = edto(i) * etad(i,k) * xmb(i) * dt2
             endif
         endif
     enddo
             enddo
 !!
             return
             end
