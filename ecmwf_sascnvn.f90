! Simplified Arakawa-Schubert Deep Convection Scheme using data supplied from netCDF files
! Detailed algorithm : http://www.dtcenter.org/GMTB/gfs_phys_doc/group___s_a_s.html#ga5ad7848a22a950d41e5bea89066876fb

! COMMANDS TO RUN THIS CODE
! --------------------------------------------------------------
! interpolate 2.5deg reanalysis grid to 0.25deg resolution using ./regrid_sascnvn_input.ipynb
! $ gfortran -fbounds-check -ffree-line-length-512 exper_machine.f90 exper_physcons.f90 exper_funcphys.f90 sascnvn_init.f90 sascnvn_static.f90 sascnvn_recalculate.f90 sascnvn_dynamic.f90 sascnvn_feedback.f90 pwl_interp_1d.f90 sascnvn.f90 -o sascnvn -I/usr/local/include -L/usr/local/lib -lnetcdff
! $ ./sascnvn
! --------------------------------------------------------------

! CAUTION
! --------------------------------------------------------------
! 1. Index variables, they should start from 1, not 0
! 2. Datatypes and dimensions of variables
! 3. Implicit ordering when lats, lons 2D pairs are reduced to 1D im, keep it consistent
! 4. Subroutine calculations should not be a part of higher level if or do loop
! 5. Reading _FillValue from netcdf file
! 6. if(totflg) return should stop calling of subsequent subroutines
! 7. If levels are reversed, the data in data arrays remains in old level order. Need to fix that.
! 8. External modules used may cause problems due to datatype size and precision assumed.

program isolated_sascnvn
    
    ! import modules
    use machine, only:kind_phys
    use physcons, grav => con_g
    use netcdf
    ! use omp_lib

    use sascnvn_init    
    use sascnvn_static
    use sascnvn_recalculate
    use sascnvn_dynamic
    use sascnvn_feedback

    implicit none

    ! files and handles
    character (len = *), parameter :: FILE_2D_VARIABLES = "../data/ECMWF_Aug2017/ecmwf_2d.nc"
    character (len = *), parameter :: FILE_3D_VARIABLES = "../data/ECMWF_Aug2017/ecmwf_3d.nc"
    character (len = *), parameter :: FILE_4D_VARIABLES = "../data/ECMWF_Aug2017/ecmwf_4d.nc"
    character (len = *), parameter :: OUTPUT_FILE_NAME = "../data/sascnvn_output/ecmwf_sascnvn_Aug2017.nc"
    integer :: ncid_2d, ncid_3d, ncid_4d, out_ncid
    
    ! name given to variables in files
    character (len = *), parameter :: LVL_NAME =        "level"
    character (len = *), parameter :: LAT_NAME =        "latitude"
    character (len = *), parameter :: LON_NAME =        "longitude"
    character (len = *), parameter :: REC_NAME =        "time"

    ! 2D - invariant
    character (len = *), parameter :: LSMASK_NAME =     "lsm"
    ! 3D
    character (len = *), parameter :: SFCPRES_NAME =    "msl"
    ! 4D
    character (len = *), parameter :: AIRTEMP_NAME =    "t"
    character (len = *), parameter :: SHUM_NAME =       "q"
    character (len = *), parameter :: HGT_NAME =        "z"
    character (len = *), parameter :: UWND_NAME =       "u"
    character (len = *), parameter :: VWND_NAME =       "v"
    character (len = *), parameter :: OMEGA_NAME =      "w"
    
    ! Declarations for file writing
    character (len = *), parameter :: UNITS_STRING =    "units"
    character (len = *), parameter :: LAT_UNITS =       "degrees_north"
    character (len = *), parameter :: LON_UNITS =       "degrees_east"
    character (len = *), parameter :: LVL_UNITS =       "millibar"
    character (len = *), parameter :: REC_UNITS =       "hours since 1900-01-01 00:00:0.0"
    character (len = *), parameter :: CALENDAR_UNITS =  "gregorian"
    character (len = *), parameter :: RAINFALL_UNITS =  "mm/day"
    character (len = *), parameter :: CLDWRK_UNITS =    "m^2/s^2"
    character (len = *), parameter :: RAINFALL_NAME =   "rn"
    character (len = *), parameter :: CLDWRK_NAME =     "cldwrk"
    integer :: rainfall_varid, cldwrk_varid, lat_varid, lon_varid, rec_varid
    
    integer, parameter :: NDIMS_SFCPRES     = 3
    integer, parameter :: NDIMS_COMMON      = 4
    
    ! total number of pressure levels after interpolation at 25mb, from 1000mb to 300mb
    integer, parameter :: NLVLS_MODEL = 20    
    real(kind = kind_phys):: scale_factor, offset
    real (kind=kind_phys) :: levels(NLVLS_MODEL)
    data levels /1000.0, 950.0, 900.0, 850.0, 800.0, 750., 700.0, 650.0,600.0, 550.0, 500.0, 450.0,400.0, 350.0, 300.0, 250.0, 200.0, 150.0 ,100.0 , 50.0/

    ! /1000., 975., 950., 925., 900., 875., 850., 825., 800., 775., 750., 725., 700., 675., 650., 625., 600., 575., 550., 525., 500., 475., 450., 425., 400., 375., 350., 325., 300./
    ! /1000., 925., 850., 700., 600., 500., 400., 300., 250., 200., 150., 100., 70., 50., 30., 20., 10./
    ! , 275., 250., 225., 200., 175., 150., 125., 100., 75., 50., 25., 0./
    integer, parameter :: NLATS_COMMON = 36, NLONS_COMMON = 71, NLVLS_COMMON = 20, NRECS_COMMON = 124
    
    ! integer, parameter :: NLATS_COMMON = 25, NLONS_COMMON = 73, NRECS_COMMON = 45
    ! integer, parameter :: NLATS_COMMON = 240, NLONS_COMMON = 720, NRECS_COMMON = 45
    ! ---------------------------------------------------------------------------------------------------

    integer :: start(NDIMS_COMMON), count(NDIMS_COMMON)
    integer :: start_sfcpres(NDIMS_SFCPRES), count_sfcpres(NDIMS_SFCPRES)
    
    ! loop iterators
    integer :: i_4D
    integer :: islimsk_i
    integer :: prsl_i, prsl_k, psp_i
    integer :: airtemp_i, shum_i, sfcpres_i, hgt_i, uwnd_i, vwnd_i, omega_i
    integer :: rn_i, cldwrk_i

    ! write these to file
    real (kind=kind_phys) :: rainfall(NLONS_COMMON, NLATS_COMMON, NRECS_COMMON)
    real (kind=kind_phys) :: cloudwork(NLONS_COMMON, NLATS_COMMON, NRECS_COMMON)
    ! ---------------------------------------------------------------------------------------------------

    ! variables which will hold the actual data
    real (kind=kind_phys) :: lats_common    (NLATS_COMMON) 
    real (kind=kind_phys) :: lons_common    (NLONS_COMMON)
    real (kind=kind_phys) :: recs_common  (NRECS_COMMON)      
    real (kind=kind_phys) :: levels_common  (NLVLS_COMMON)
    
    integer :: lon_common_varid, lat_common_varid, rec_common_varid, lvl_common_varid

    real (kind=kind_phys) :: lsmask_in   (NLONS_COMMON, NLATS_COMMON)
    real (kind=kind_phys) :: sfcpres_in  (NLONS_COMMON, NLATS_COMMON)
    real (kind=kind_phys) :: airtemp_in  (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: shum_in     (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: hgt_in      (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: uwnd_in     (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: vwnd_in     (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    real (kind=kind_phys) :: omega_in    (NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON)
    integer :: lsmask_varid, airtemp_varid, shum_varid, sfcpres_varid, hgt_varid, uwnd_varid, vwnd_varid, omega_varid
    
    integer :: dimids(3)
    integer :: lon_dimid, lat_dimid, lvl_dimid, rec_dimid    
    integer :: dimids_sfcpres(NDIMS_SFCPRES)
    integer :: dimids_common(NDIMS_COMMON)

    ! Loop indices
    integer :: lon, lat, lvl, rec

    ! Open the files 
    call check( nf90_open(FILE_2D_VARIABLES, nf90_nowrite, ncid_2d) )
    call check( nf90_open(FILE_3D_VARIABLES, nf90_nowrite, ncid_3d) )
    call check( nf90_open(FILE_4D_VARIABLES, nf90_nowrite, ncid_4d) )
    
    ! Get the varids of variables.
    call check( nf90_inq_varid(ncid_4d, LAT_NAME, lat_common_varid) )
    call check( nf90_inq_varid(ncid_4d, LON_NAME, lon_common_varid) )
    call check( nf90_inq_varid(ncid_4d, REC_NAME, rec_common_varid) )
    call check( nf90_inq_varid(ncid_4d, LVL_NAME, lvl_common_varid) )

    call check( nf90_inq_varid(ncid_2d, LSMASK_NAME, lsmask_varid) )
    call check( nf90_inq_varid(ncid_3d, SFCPRES_NAME, sfcpres_varid) )
    call check( nf90_inq_varid(ncid_4d, AIRTEMP_NAME, airtemp_varid) )
    call check( nf90_inq_varid(ncid_4d, SHUM_NAME, shum_varid) )
    call check( nf90_inq_varid(ncid_4d, HGT_NAME, hgt_varid) )
    call check( nf90_inq_varid(ncid_4d, UWND_NAME, uwnd_varid) )
    call check( nf90_inq_varid(ncid_4d, VWND_NAME, vwnd_varid) )
    call check( nf90_inq_varid(ncid_4d, OMEGA_NAME, omega_varid) )

    ! Read the dimensions
    call check( nf90_get_var(ncid_4d, lat_common_varid, lats_common) )
    call check( nf90_get_var(ncid_4d, lon_common_varid, lons_common) )
    call check( nf90_get_var(ncid_4d, rec_common_varid, recs_common) )
    call check( nf90_get_var(ncid_4d, lvl_common_varid, levels_common) )
    
    ! reverse levels for model code
    ! levels_common = levels_common(NLVLS_COMMON : 1 : -1)
    ! levels_shum = levels_shum(NLVLS_SHUM : 1 : -1)
    ! levels_omega = levels_omega(NLVLS_OMEGA : 1 : -1)

    ! -------------------------------------------------------------------
    ! Create the output file. 
    call check( nf90_create(OUTPUT_FILE_NAME, nf90_clobber, out_ncid) )
    
    ! Define the dimensions. The record dimension is defined to have unlimited length - it can grow as needed.
    call check( nf90_def_dim(out_ncid, LON_NAME, NLONS_COMMON, lon_dimid) )
    call check( nf90_def_dim(out_ncid, LAT_NAME, NLATS_COMMON, lat_dimid) )
    call check( nf90_def_dim(out_ncid, REC_NAME, NF90_UNLIMITED, rec_dimid) )
    ! call check( nf90_def_dim(out_ncid, LVL_NAME, NLVLS_MODEL, lvl_dimid) )
    
    ! Define the coordinate variables and assign attributes
    call check( nf90_def_var(out_ncid, LON_NAME, NF90_REAL, lon_dimid, lon_varid) )
    call check( nf90_put_att(out_ncid, lon_varid, UNITS_STRING, LON_UNITS) )
    
    call check( nf90_def_var(out_ncid, LAT_NAME, NF90_REAL, lat_dimid, lat_varid) )
    call check( nf90_put_att(out_ncid, lat_varid, UNITS_STRING, LAT_UNITS) )
    
    call check( nf90_def_var(out_ncid, REC_NAME, NF90_REAL, rec_dimid, rec_varid) )
    call check( nf90_put_att(out_ncid, rec_varid, UNITS_STRING, REC_UNITS) )
    call check( nf90_put_att(out_ncid, rec_varid, "calendar", CALENDAR_UNITS) )   

    ! call check( nf90_def_var(out_ncid, LVL_NAME, NF90_REAL, lvl_dimid, lvl_varid) )
    ! call check( nf90_put_att(out_ncid, lvl_varid, UNITS_STRING, LVL_UNITS) )
    
    ! dimids = (/ lon_dimid, lat_dimid, lvl_dimid /)
    dimids = (/ lon_dimid, lat_dimid, rec_dimid /)
    
    ! Define the custom variables and assign attributes
    call check( nf90_def_var(out_ncid, RAINFALL_NAME, NF90_REAL, dimids, rainfall_varid) )
    call check( nf90_put_att(out_ncid, rainfall_varid, UNITS_STRING, RAINFALL_UNITS) )
        
    call check( nf90_def_var(out_ncid, CLDWRK_NAME, NF90_REAL, dimids, cldwrk_varid) )
    call check( nf90_put_att(out_ncid, cldwrk_varid, UNITS_STRING, CLDWRK_UNITS) )

    ! End define mode.
    call check( nf90_enddef(out_ncid) )
    ! -------------------------------------------------------------------
    

    ! MAP NETCDF INPUT TO VARIABLES THE MODULE CODE EXPECTS
    ! ------------------------------------------------------------
    im = NLATS_COMMON * NLONS_COMMON
    ix = NLATS_COMMON * NLONS_COMMON
    km = NLVLS_MODEL
    delt = 600
    ncloud = 1
    jcap = 10

    ! ALLOCATE ALL THE ARRAYS IN MODULE DECLARATION: if (.not.allocated(Jam)) allocate ( Jam(4*M) )
    ! ------------------------------------------------------------
    ! ALLOCATE 1D
    ! -------------------------------------
    allocate(kbot(im))
    allocate(ktop(im))
    allocate(kcnv(im))
    allocate(kb(im))
    allocate(kbcon(im))
    allocate(kbcon1(im))
    allocate(ktcon(im))
    allocate(ktcon1(im))
    allocate(jmin(im))
    allocate(lmin(im))
    allocate(kbmax(im))
    allocate(kbm(im)) 
    allocate(kmax(im))
    allocate(islimsk(im))
    allocate(psp(im))
    allocate(cldwrk(im))
    allocate(rn(im))
    allocate(ps(im))

    allocate(qcond(im))
    allocate(qevap(im))
    allocate(rntot(im))
    allocate(vshear(im))
    allocate(xaa0(im))
    allocate(xk(im))
    allocate(xlamd(im))
    allocate(xmb(im))
    allocate(xmbmax(im))
    allocate(xpwav(im))
    allocate(xpwev(im))
    allocate(delubar(im))
    allocate(delvbar(im))
    allocate(aa1(im))
    allocate(acrt(im))
    allocate(acrtfct(im))
    allocate(delhbar(im))
    allocate(delq(im))
    allocate(delq2(im))
    allocate(delqbar(im))
    allocate(delqev(im))
    allocate(deltbar(im))
    allocate(deltv(im))
    allocate(dtconv(im))
    allocate(edt(im))
    allocate(edto(im))
    allocate(edtx(im))
    allocate(fld(im))
    allocate(hmax(im))
    allocate(hmin(im))
    allocate(aa2(im))
    allocate(pbcdif(im))
    allocate(pdot(im))
    allocate(pwavo(im))
    allocate(pwevo(im))
    allocate(xlamud(im))
    allocate(cnvflg(im))
    allocate(flg(im))
    allocate(qlko_ktcon(im))
    allocate(tx1(im))
    allocate(sumx(im))

    ! ALLOCATE 2D
    ! -------------------------------------
    allocate(delp(ix,km))
    allocate(prslp(ix,km))
    allocate(del(ix,km))    
    allocate(prsl(ix,km))
    allocate(q1(ix,km))
    allocate(t1(ix,km))
    allocate(u1(ix,km))
    allocate(v1(ix,km))
    allocate(dot(ix,km))
    allocate(phil(ix,km))
    allocate(cnvw(ix,km))
    allocate(cnvc(ix,km))

    allocate(ud_mf(im,km))
    allocate(dd_mf(im,km))
    allocate(dt_mf(im,km))
    allocate(hcdo(im,km))
    allocate(po(im,km))
    allocate(ucdo(im,km))
    allocate(vcdo(im,km))
    allocate(qcdo(im,km))
    allocate(pfld(im,km))
    allocate(to(im,km))
    allocate(qo(im,km))
    allocate(uo(im,km))
    allocate(vo(im,km))
    allocate(qeso(im,km))
    allocate(dellal(im,km))
    allocate(tvo(im,km))
    allocate(dbyo(im,km))
    allocate(zo(im,km))
    allocate(xlamue(im,km))
    allocate(fent1(im,km))
    allocate(fent2(im,km))
    allocate(frh(im,km))
    allocate(heo(im,km))
    allocate(heso(im,km))
    allocate(qrcd(im,km))
    allocate(dellah(im,km))
    allocate(dellaq(im,km))
    allocate(dellau(im,km))
    allocate(dellav(im,km))
    allocate(hcko(im,km))
    allocate(ucko(im,km))
    allocate(vcko(im,km))
    allocate(qcko(im,km))
    allocate(eta(im,km))
    allocate(etad(im,km))
    allocate(zi(im,km))
    allocate(qrcko(im,km))
    allocate(qrcdo(im,km))
    allocate(pwo(im,km))
    allocate(pwdo(im,km))
    allocate(cnvwt(im,km))

    ! start and count should be reverse of the netcdf var dimensions
    start = (/ 1, 1, 1, 1 /)
    count = (/ NLONS_COMMON, NLATS_COMMON, NLVLS_COMMON, 1 /)

    start_sfcpres = (/ 1, 1, 1 /)
    count_sfcpres = (/ NLONS_COMMON, NLATS_COMMON, 1 /)

    ! call sascnvn routine after every timestep of netcdf file
    !$OMP PARALLEL DO
    do rec = 1, NRECS_COMMON
        start(4) = rec
        start_sfcpres(3) = rec
        print *, "for time step - ", rec

        do prsl_i = 1, im
            do prsl_k = 1, km
                prslp(prsl_i, prsl_k) = levels(prsl_k) * 100.0
                delp(prsl_i, prsl_k) = 2500.
            end do
        end do

        ! load the 2D data - time invariant
        call check( nf90_get_var(ncid_2d, lsmask_varid, lsmask_in) )    
        scale_factor = 1.52594875864068e-05
        offset = 0.499992370256207
        lsmask_in = lsmask_in * scale_factor + offset 
        islimsk_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                islimsk(islimsk_i) = lsmask_in(lon, lat)
                islimsk_i = islimsk_i + 1
            end do
        end do

        ! read the 3D data
        call check( nf90_get_var(ncid_3d, sfcpres_varid, sfcpres_in, start = start_sfcpres, count = count_sfcpres) )
        scale_factor = 0.0543132849098927   
        offset = 100833.129093358
        sfcpres_in = sfcpres_in * scale_factor + offset

        psp_i = 1        
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                psp(psp_i) = sfcpres_in(lon, lat)
                psp_i = psp_i + 1
            end do
        end do

        ! read the 4D data - take scale and offset from ncdump
        call check( nf90_get_var(ncid_4d, airtemp_varid, airtemp_in, start = start, count = count) )
        scale_factor = 0.00206040779423332
        offset = 255.171920540732
        airtemp_in = airtemp_in * scale_factor + offset

        call check( nf90_get_var(ncid_4d, shum_varid, shum_in, start = start, count = count) )
        scale_factor = 4.93984081858048e-07   
        offset = 0.0161858824261608
        shum_in = shum_in * scale_factor + offset
        
        call check( nf90_get_var(ncid_4d, hgt_varid, hgt_in, start = start, count = count) )
        scale_factor = 3.15936598289459   
        offset = 102676.929838493
        hgt_in = hgt_in * scale_factor + offset
                
        call check( nf90_get_var(ncid_4d, uwnd_varid, uwnd_in, start = start, count = count) )
        scale_factor = 0.00169866880678496   
        offset = 0.303159149483326
        uwnd_in = uwnd_in * scale_factor + offset
                
        call check( nf90_get_var(ncid_4d, vwnd_varid, vwnd_in, start = start, count = count) )
        scale_factor = 0.00107316626074542   
        offset = -5.8480783922124
        vwnd_in = vwnd_in * scale_factor + offset
                
        call check( nf90_get_var(ncid_4d, omega_varid, omega_in, start = start, count = count) )
        scale_factor = 0.000105057369829008
        offset = -1.79894216567832
        omega_in = omega_in * scale_factor + offset

        i_4D = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                do lvl = 1, NLVLS_COMMON
                    t1(i_4D, lvl) = airtemp_in(lon, lat, NLVLS_COMMON-lvl+1)
                    q1(i_4D, lvl) = shum_in(lon, lat, NLVLS_COMMON-lvl+1)
                    phil(i_4D, lvl) = hgt_in(lon, lat, NLVLS_COMMON-lvl+1)
                    u1(i_4D, lvl) = uwnd_in(lon, lat, NLVLS_COMMON-lvl+1)
                    v1(i_4D, lvl) = vwnd_in(lon, lat, NLVLS_COMMON-lvl+1)
                    dot(i_4D, lvl) = omega_in(lon, lat, NLVLS_COMMON-lvl+1)
                end do
                i_4D = i_4D + 1
            end do
        end do

        ! CALL SASCNVN MODULES NOW

        ! SASCNVN: INITIALIZATION SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call convert_input_pa_to_cb()
        call init_arrays_and_tunable_params()
        call define_top_layer()
        call init_hydrostatic_height_and_entr_rate()
        call convert_from_cb_to_mb()
        call init_column_variables()
        call computeMSE()
        ! -----------------------------------------------------------

        ! SASCNVN: STATIC CONTROL SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call determine_max_MSE_level()
        call find_level_of_free_convection()
        if(totflg) goto 101

        call find_critical_convective_inhibition()
        if(totflg) goto 101
        
        call assume_entrainment_detrainment_rate()
        call functions_mimic_cloud_ensemble()
        call final_entrainment_rate()
        call find_updraft_mass_flux()
        call mass_flux_above_cloud_base()
        call updraft_cloud_properties()
        call consider_convection_inhibition()
        if(totflg) goto 101

        call first_guess_cloud_top()
        if(totflg) goto 101

        call search_downdraft_originating_layer()
        call verify_jmin_within_cloud ()
        call upper_limit_mass_flux()
        call find_cloud_moisture_and_precipitation()
        call find_cloud_work_function()
        if(totflg) goto 101

        call estimate_convective_overshooting()
        call find_properties()
        call exchange_ktcon_ktcon1()
        call find_liquid_vapor_separation()
        call compute_precipitation_efficiency()
        call determine_detrainment_rate()
        call downdraft_mass_flux()
        call downdraft_moisture_properties()
        call final_downdraft_strength ()
        call downdraft_cloudwork_functions()
        if(totflg) goto 101

        call change_due_to_cloud_unit_mass()
        call change_due_to_subsidence_entrainment()
        call cloud_top_and_water()
        call final_variable_per_unit_mass_flux()
        call environmental_conditions()
        call recalculate_moist_static_energy()
        ! -----------------------------------------------------------

        ! SASCNVN: RECALCULATION SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call recalculate_moisture()
        call recalculate_downdraft_moisture_properties()
        call recalculate_downdraft_cloudwork_functions()
        ! -----------------------------------------------------------

        ! SASCNVN: DYNAMIC CONTROL SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call critical_cloudwork_function()
        call large_scale_forcing()
        if(totflg) goto 101

        call return_without_modifying()
        ! -----------------------------------------------------------

        ! SASCNVN: FEEDBACK CONTROL SECTION OF ALGORITHM
        ! -----------------------------------------------------------
        call feedback()
        call convective_cloud_water()
        call convective_cloud_cover()
        call cloud_water()
        call final_feedback()
        ! -----------------------------------------------------------
        
        101 continue
        ! print *, "totflag", totflg
        ! print *, "q1 : ", q1
        ! print *, "t1 : ", t1
        ! print *, "u1 : ", u1
        ! print *, "v1 : ", v1
        ! print *, "cloud work function : ", cldwrk
        ! print *, "convective rain : ", rn
        ! print *, "kbot : ", kbot
        ! print *, "ktop : ", ktop
        ! print *, "deep conv flag :", kcnv
        ! print *, "ud_mf :", ud_mf
        ! print *, "dd_mf : ", dd_mf
        ! print *, "dt_mf : ", dt_mf
        ! print *, "cnvw : ", cnvw
        ! print *, "cnvc : ", cnvc
        
        ! pause

        ! units = mm/day
        rn = rn * 86400.0
        ! print *, rn

        ! unflatten im dimension into (lon,lat) for file writing
        rn_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                rainfall(lon, lat, rec) = rn(rn_i)
                rn_i = rn_i + 1
            end do
        end do
        
        cldwrk_i = 1
        do lon = 1, NLONS_COMMON
            do lat = 1, NLATS_COMMON
                cloudwork(lon, lat, rec) = cldwrk(cldwrk_i)
                cldwrk_i = cldwrk_i + 1
            end do
        end do
        
        ! print *, "--------- time step ends -------------"
        ! next time step
    end do        
    !$OMP END PARALLEL DO

    ! -----------------------------------------------------
    ! Write the variable data.
    call check( nf90_put_var(out_ncid, lat_varid, lats_common) )
    call check( nf90_put_var(out_ncid, lon_varid, lons_common) )
    call check( nf90_put_var(out_ncid, rec_varid, recs_common) )    
    call check( nf90_put_var(out_ncid, rainfall_varid, rainfall) )
    call check( nf90_put_var(out_ncid, cldwrk_varid, cloudwork) )
    ! -----------------------------------------------------

    ! ARRAY DEALLOCATION
    ! -----------------------------------------------------
    
    ! DEALLOCATE 1D
    ! -------------------------------------
    deallocate(kbot)
    deallocate(ktop)
    deallocate(kcnv)
    deallocate(kb)
    deallocate(kbcon)
    deallocate(kbcon1)
    deallocate(ktcon)
    deallocate(ktcon1)
    deallocate(jmin)
    deallocate(lmin)
    deallocate(kbmax)
    deallocate(kbm) 
    deallocate(kmax)
    deallocate(islimsk)
    deallocate(psp)
    deallocate(cldwrk)
    deallocate(rn)
    deallocate(ps)
    deallocate(qcond)
    deallocate(qevap)
    deallocate(rntot)
    deallocate(vshear)
    deallocate(xaa0)
    deallocate(xk)
    deallocate(xlamd)
    deallocate(xmb)
    deallocate(xmbmax)
    deallocate(xpwav)
    deallocate(xpwev)
    deallocate(delubar)
    deallocate(delvbar)
    deallocate(aa1)
    deallocate(acrt)
    deallocate(acrtfct)
    deallocate(delhbar)
    deallocate(delq)
    deallocate(delq2)
    deallocate(delqbar)
    deallocate(delqev)
    deallocate(deltbar)
    deallocate(deltv)
    deallocate(dtconv)
    deallocate(edt)
    deallocate(edto)
    deallocate(edtx)
    deallocate(fld)
    deallocate(hmax)
    deallocate(hmin)
    deallocate(aa2)
    deallocate(pbcdif)
    deallocate(pdot)
    deallocate(pwavo)
    deallocate(pwevo)
    deallocate(xlamud)
    deallocate(cnvflg)
    deallocate(flg)
    deallocate(qlko_ktcon)
    deallocate(tx1)
    deallocate(sumx)
    
    ! DEALLOCATE 2D
    ! -------------------------------------
    deallocate(delp)
    deallocate(prslp)
    deallocate(del)    
    deallocate(prsl)
    deallocate(q1)
    deallocate(t1)
    deallocate(u1)
    deallocate(v1)
    deallocate(dot)
    deallocate(phil)
    deallocate(cnvw)
    deallocate(cnvc)

    deallocate(ud_mf)
    deallocate(dd_mf)
    deallocate(dt_mf)
    deallocate(hcdo)
    deallocate(po)
    deallocate(ucdo)
    deallocate(vcdo)
    deallocate(qcdo)
    deallocate(pfld)
    deallocate(to)
    deallocate(qo)
    deallocate(uo)
    deallocate(vo)
    deallocate(qeso)
    deallocate(dellal)
    deallocate(tvo)
    deallocate(dbyo)
    deallocate(zo)
    deallocate(xlamue)
    deallocate(fent1)
    deallocate(fent2)
    deallocate(frh)
    deallocate(heo)
    deallocate(heso)
    deallocate(qrcd)
    deallocate(dellah)
    deallocate(dellaq)
    deallocate(dellau)
    deallocate(dellav)
    deallocate(hcko)
    deallocate(ucko)
    deallocate(vcko)
    deallocate(qcko)
    deallocate(eta)
    deallocate(etad)
    deallocate(zi)
    deallocate(qrcko)
    deallocate(qrcdo)
    deallocate(pwo)
    deallocate(pwdo)
    deallocate(cnvwt)

    ! Close the files
    call check( nf90_close(ncid_4d) )
    call check( nf90_close(ncid_3d) )
    call check( nf90_close(ncid_2d) )
    call check( nf90_close(out_ncid) )
    
    print *, "*** END OF PROGRAM! ***"

contains
    subroutine check(status)
        use netcdf
        implicit none
        integer, intent ( in) :: status
        if(status /= nf90_noerr) then 
            print *, trim(nf90_strerror(status))
            stop "Stopped"
        end if
    end subroutine check

end program isolated_sascnvn

!  \section original Original Documentation
!  Penetrative convection is simulated following Pan and Wu (1994), which is based on Arakawa and Schubert(1974) as simplified by Grell (1993) and with a saturated downdraft. Convection occurs when the cloud work function (CWF) exceeds a certain threshold. Mass flux of the cloud is determined using a quasi-equilibrium assumption based on this threshold CWF. The CWF is a function of temperature and moisture in each air column of the model gridpoint. The temperature and moisture profiles are adjusted towards the equilibrium CWF within a specified time scale using the deduced mass flux. A major simplification of the original Arakawa-Shubert scheme is to consider only the deepest cloud and not the spectrum of clouds. The cloud model incorporates a downdraft mechanism as well as the evaporation of precipitation. Entrainment of the updraft and detrainment of the downdraft in the sub-cloud layers are included. Downdraft strength is based on the vertical wind shear through the cloud. The critical CWF is a function of the cloud base vertical motion. As the large-scale rising motion becomes strong, the CWF [similar to convective available potential energy (CAPE)] is allowed to approach zero (therefore approaching neutral stability).
!
!  Mass fluxes induced in the updraft and the downdraft are allowed to transport momentum. The momentum exchange is calculated through the mass flux formulation in a manner similar to that for heat and moisture. The effect of the convection-induced pressure gradient force on cumulus momentum transport is parameterized in terms of mass flux and vertical wind shear (Han and Pan, 2006). As a result, the cumulus momentum exchange is reduced by about 55 % compared to the full exchange.
!
!  The entrainment rate in cloud layers is dependent upon environmental humidity (Han and Pan, 2010). A drier environment increases the entrainment, suppressing the convection. The entrainment rate in sub-cloud layers is given as inversely proportional to height. The detrainment rate is assumed to be a constant in all layers and equal to the entrainment rate value at cloud base, which is O(10-4). The liquid water in the updraft layer is assumed to be detrained from the layers above the level of the minimum moist static energy into the grid-scale cloud water with conversion parameter of 0.002 m-1, which is same as the rain conversion parameter.
!
!  Following Han and Pan (2010), the trigger condition is that a parcel lifted from the convection starting level without entrainment must reach its level of free convection within 120-180 hPa of ascent, proportional to the large-scale vertical velocity. This is intended to produce more convection in large-scale convergent regions but less convection in large-scale subsidence regions. Another important trigger mechanism is to include the effect of environmental humidity in the sub-cloud layer, taking into account convection inhibition due to existence of dry layers below cloud base. On the other hand, the cloud parcel might overshoot beyond the level of neutral buoyancy due to its inertia, eventually stopping its overshoot at cloud top. The CWF is used to model the overshoot. The overshoot of the cloud top is stopped at the height where a parcel lifted from the neutral buoyancy level with energy equal to 10% of the CWF would first have zero energy.
!
!  Deep convection parameterization (SAS) modifications include:
!  - Detraining cloud water from every updraft layer
!  - Starting convection from the level of maximum moist static energy within PBL
!  - Random cloud top is eliminated and only deepest cloud is considered
!  - Cloud water is detrained from every cloud layer
!  - Finite entrainment and detrainment rates for heat, moisture, and momentum are specified
!  - Similar to shallow convection scheme,
!  - entrainment rate is given to be inversely proportional to height in sub-cloud layers
!  - detrainment rate is set to be a constant as entrainment rate at the cloud base.
!  - Above cloud base, an organized entrainment is added, which is a function of environmental relative humidity
