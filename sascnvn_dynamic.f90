module sascnvn_dynamic
    use sascnvn_init
    implicit none
contains

subroutine critical_cloudwork_function()
!calculate critical cloud work function
       do i = 1, im
         if(cnvflg(i)) then
           if(pfld(i,ktcon(i)).lt.pcrit(15))then
             acrt(i)=acrit(15)*(975.-pfld(i,ktcon(i)))/(975.-pcrit(15))
           else if(pfld(i,ktcon(i)).gt.pcrit(1))then
             acrt(i)=acrit(1)
           else
             k =  int((850. - pfld(i,ktcon(i)))/50.) + 2
             k = min(k,15)
             k = max(k,2)
             acrt(i)=acrit(k)+(acrit(k-1)-acrit(k))*(pfld(i,ktcon(i))-pcrit(k))/(pcrit(k-1)-pcrit(k))
           endif
         endif
       enddo
       do i = 1, im
         if(cnvflg(i)) then
           if(islimsk(i) == 1) then
             w1 = w1l
             w2 = w2l
             w3 = w3l
             w4 = w4l
           else
             w1 = w1s
             w2 = w2s
             w3 = w3s
             w4 = w4s
           endif
!modify critical cloud workfunction by cloud base vertical velocity
           if(pdot(i).le.w4) then
             acrtfct(i) = (pdot(i) - w4) / (w3 - w4)
           elseif(pdot(i).ge.-w4) then
             acrtfct(i) = - (pdot(i) + w4) / (w4 - w3)
           else
             acrtfct(i) = 0.
           endif
           val1    =             -1.
           acrtfct(i) = max(acrtfct(i),val1)
           val2    =             1.
           acrtfct(i) = min(acrtfct(i),val2)
           acrtfct(i) = 1. - acrtfct(i)
!modify acrtfct(i) by colume mean rh if rhbar(i) is greater than 80 percent
!
!         if(rhbar(i).ge..8) then
!           acrtfct(i) = acrtfct(i) * (.9 - min(rhbar(i),.9)) * 10.
!         endif

!  modify adjustment time scale by cloud base vertical velocity

           dtconv(i) = dt2 + max((1800. - dt2),0.) * (pdot(i) - w2) / (w1 - w2)
!         dtconv(i) = max(dtconv(i), dt2)
!         dtconv(i) = 1800. * (pdot(i) - w2) / (w1 - w2)
           dtconv(i) = max(dtconv(i),dtmin)
           dtconv(i) = min(dtconv(i),dtmax)
!
         endif
       enddo
end subroutine critical_cloudwork_function

subroutine large_scale_forcing()
!--- large scale forcing

       do i= 1, im
         if(cnvflg(i)) then
           fld(i)=(aa1(i)-acrt(i)* acrtfct(i))/dtconv(i)
           if(fld(i).le.0.) cnvflg(i) = .false.
         endif
         if(cnvflg(i)) then
!         xaa0(i) = max(xaa0(i),0.)
           xk(i) = (xaa0(i) - aa1(i)) / mbdt
           if(xk(i).ge.0.) cnvflg(i) = .false.
         endif
 
 !--- kernel, cloud base mass flux
 
         if(cnvflg(i)) then
           xmb(i) = -fld(i) / xk(i)
           xmb(i) = min(xmb(i),xmbmax(i))
         endif
       enddo
       totflg = .true.
       do i=1,im
         totflg = totflg .and. (.not. cnvflg(i))
       enddo
      !  if(totflg) return
end subroutine large_scale_forcing


subroutine return_without_modifying()
 !restore to,qo,uo,vo to t1,q1,u1,v1 in case convection stops
       do k = 1, km
         do i = 1, im
           if (cnvflg(i) .and. k .le. kmax(i)) then
             to(i,k) = t1(i,k)
             qo(i,k) = q1(i,k)
             uo(i,k) = u1(i,k)
             vo(i,k) = v1(i,k)
            qeso(i,k) = 0.01 * fpvsx(t1(i,k))      ! fpvsx is in pa
             qeso(i,k) = eps * qeso(i,k) / (pfld(i,k) + epsm1*qeso(i,k))
             val     =             1.e-8
             qeso(i,k) = max(qeso(i,k), val )
           endif
         enddo
       enddo
end subroutine return_without_modifying


end module sascnvn_dynamic
 
